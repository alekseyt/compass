/* damn, dude:
 * http://stackoverflow.com/questions/7337232/android-ndk-produce-unreasonable-big-binaries-how-to-optimize-so-size
 *
 * this is basically workaround for toolchain bug which is pulling in
 * exceptions unwinding stuff even with -fno-exeptions
 */
char __aeabi_unwind_cpp_pr0[1];
char __aeabi_unwind_cpp_pr1[1];
char __aeabi_unwind_cpp_pr2[1];
