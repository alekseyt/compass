#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1sanitize_1lat(JNIEnv *env, jclass cls, jfloat lat) {
	return cmps_sanitize_lat(lat);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1sanitize_1lon(JNIEnv *env, jclass cls, jfloat lon) {
	return cmps_sanitize_lon(lon);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1normalize_1azimuth(JNIEnv *env, jclass cls, jfloat azimuth) {
	return cmps_normalize_azimuth(azimuth);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1direction(JNIEnv *env, jclass cls, jlong c_location_here, jlong c_location_there) {
	return cmps_direction(locationp(c_location_here), locationp(c_location_there));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1distance(JNIEnv *env, jclass cls, jlong c_location_here, jlong c_location_there) {
	return cmps_distance(locationp(c_location_here), locationp(c_location_there));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1azimuth_1to_1direction(JNIEnv *env, jclass cls, jfloat azimuth, jint directions) {
	return cmps_azimuth_to_direction(azimuth, directions);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1magnetic_1field_1sqr_1strength(JNIEnv *env, jclass cls, jfloat x, jfloat y, jfloat z) {
	return cmps_magnetic_field_sqr_strength(x, y, z);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1sqr_1magnetic_1field_1valid(JNIEnv *env, jclass cls, jfloat sqr_strength) {
	return cmps_sqr_magnetic_field_valid(sqr_strength);
}
