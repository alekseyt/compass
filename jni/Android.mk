LOCAL_PATH := $(call my-dir)
 
include $(CLEAR_VARS)

LOCAL_C_INCLUDES += jni/libnunicode

LOCAL_MODULE    := compass
LOCAL_SRC_FILES := compass_jni.c \
                   compass_jni_callee_lookup.c \
                   compass_jni_constants.c \
                   compass_jni_distress.c \
                   compass_jni_dynamo.c \
                   compass_jni_hrt.c \
                   compass_jni_location.c \
                   compass_jni_low_pass_filter.c \
                   compass_jni_screen_filter.c \
                   compass_jni_router.c \
                   compass_jni_tracker.c \
                   compass_jni_utils.c \
                   libcompass/callee_lookup.c \
                   libcompass/distress_detector.c \
                   libcompass/dynamo.c \
                   libcompass/hrt.c \
                   libcompass/location.c \
                   libcompass/low_pass_filter.c \
                   libcompass/router.c \
                   libcompass/tracker.c \
                   libcompass/screen_filter.c \
                   libcompass/utils.c \
                   libnunicode/libnu/cesu8.c \
   
include $(BUILD_SHARED_LIBRARY)
