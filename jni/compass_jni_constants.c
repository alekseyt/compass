#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1tracker_1angle_1alpha(JNIEnv *env, jclass cls) {
	return TRACKER_ANGLE_ALPHA;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1tracker_1speed_1alpha(JNIEnv *env, jclass cls) {
	return TRACKER_SPEED_ALPHA;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1router_1location_1ttl(JNIEnv *env, jclass cls) {
	return ROUTER_LOCATION_TTL;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1router_1speed_1ttl(JNIEnv *env, jclass cls) {
	return ROUTER_SPEED_TTL;
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1distress_1alert_1level_1armed(JNIEnv *env, jclass cls) {
	return DISTRESS_ALERT_LEVEL_ARMED;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1distress_1alert_1level_1ttl(JNIEnv *env, jclass cls) {
	return DISTRESS_ALERT_LEVEL_TTL;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1distress_1armed_1ttl(JNIEnv *env, jclass cls) {
	return DISTRESS_ARMED_TTL;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1g(JNIEnv *env, jclass cls) {
	return G;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1distress_1force_1threshold(JNIEnv *env, jclass cls) {
	return DISTRESS_FORCE_THRESHOLD;
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1config_1max_1number_1length(JNIEnv *env, jclass cls) {
	return MAX_NUMBER_LENGTH;
}
