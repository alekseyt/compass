#include <stdlib.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1distress_1alloc(JNIEnv *env, jclass cls) {
	cmps_distress_detector_t *detector = malloc(sizeof(*detector));
	return (jlong)(long)(detector);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1distress_1free(JNIEnv *env, jclass cls, jlong c_detector) {
	free(detectorp(c_detector));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1distress_1init(JNIEnv *env, jclass cls, jlong c_detector) {
	cmps_distress_init(detectorp(c_detector));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1distress_1release(JNIEnv *env, jclass cls, jlong c_detector) {
	cmps_distress_release(detectorp(c_detector));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1reset(JNIEnv *env, jclass cls, jlong c_detector) {
	cmpss_distress_reset(detectorp(c_detector));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1update_1acceleration(JNIEnv *env, jclass cls, jlong c_detector, jfloat x, jfloat y, jfloat z, jlong now) {
	cmpss_distress_update_acceleration(detectorp(c_detector), x, y, z, now);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1update_1alert_1button(JNIEnv *env, jclass cls, jlong c_detector, jint on, jlong now) {
	cmpss_distress_update_alert_button(detectorp(c_detector), on, now);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1armed(JNIEnv *env, jclass cls, jlong c_detector, jlong now) {
	return cmpss_distress_armed(detectorp(c_detector), now);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1pushed(JNIEnv *env, jclass cls, jlong c_detector, jlong now) {
	return cmpss_distress_pushed(detectorp(c_detector), now);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1distress_1panic(JNIEnv *env, jclass cls, jlong c_detector, jlong now) {
	return cmpss_distress_panic(detectorp(c_detector), now);
}
