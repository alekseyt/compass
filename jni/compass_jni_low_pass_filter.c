#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1low_1pass_1filter(JNIEnv *env, jclass cls, jfloat input, jfloat output, jfloat alpha) {
	return cmps_low_pass_filter(input, output, alpha);
}
