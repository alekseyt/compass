#include <stdlib.h>
#include <string.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1dynamo_1alloc(JNIEnv *env, jclass cls) {
	cmps_dynamo_t *dynamo = malloc(sizeof(*dynamo));
	memset(dynamo, 0, sizeof(*dynamo));
	return (jlong)(long)(dynamo);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1dynamo_1free(JNIEnv *env, jclass cls, jlong c_dynamo) {
	free(dynamop(c_dynamo));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1dynamo_1init(JNIEnv *env, jclass cls, jlong c_dynamo) {
	cmps_dynamo_init(dynamop(c_dynamo));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1dynamo_1release(JNIEnv *env, jclass cls, jlong c_dynamo) {
	cmps_dynamo_release(dynamop(c_dynamo));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1dynamo_1reset(JNIEnv *env, jclass cls, jlong c_dynamo) {
	cmpss_dynamo_reset(dynamop(c_dynamo));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1dynamo_1next(JNIEnv *env, jclass cls, jlong c_dynamo) {
	cmpss_dynamo_next(dynamop(c_dynamo));
}

jstring JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1dynamo_1get_1number(JNIEnv *env, jclass cls, jlong c_dynamo, jstring preset_number, jlong c_lookup) {
	const char *c_preset_number = (preset_number == 0 ? 0 : (*env)->GetStringUTFChars(env, preset_number, 0));

	char number[MAX_NUMBER_LENGTH + 1] = { 0 };
	int ret = cmpss_dynamo_get_number(dynamop(c_dynamo), c_preset_number, lookupp(c_lookup), number);

	if (c_preset_number != 0) {
		(*env)->ReleaseStringUTFChars(env, preset_number, c_preset_number);
	}

	return (ret == 0 ? (*env)->NewStringUTF(env, number) : 0);
}
