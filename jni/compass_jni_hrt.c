#include <stdlib.h>
#include <string.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jstring JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1marshall(JNIEnv *env, jclass cls, jstring title, jfloat lat, jfloat lon) {
	const char *c_title = (title == 0 ? 0 : (*env)->GetStringUTFChars(env, title, 0));

	int text_size = cmps_hrt_marshall(0, 0, c_title, lat, lon);
	if (text_size <= 0) {
		if (c_title != 0) {
			(*env)->ReleaseStringUTFChars(env, title, c_title);
		}

		return 0;
	}

	char *text = malloc(++text_size); /* include space for trailing 0 */
	cmps_hrt_marshall(text, text_size, c_title, lat, lon);
	jstring j_text = (*env)->NewStringUTF(env, text);
	free(text);

	if (c_title != 0) {
		(*env)->ReleaseStringUTFChars(env, title, c_title);
	}

	return j_text;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1unmarshall(JNIEnv *env, jclass cls, jstring text) {
	const char *title = 0;
	float lat = 0, lon = 0;

	const char *c_text = (*env)->GetStringUTFChars(env, text, 0);

	int title_len = cmps_hrt_unmarshall(c_text, &title, &lat, &lon);
	if (title_len < 0) { /* might be 0, title is optional */
		if (c_text != 0) {
			(*env)->ReleaseStringUTFChars(env, text, c_text);
		}

		return 0;
	}

	cmps_jni_bookmark_t *bookmark = malloc(sizeof(*bookmark));
	memset(bookmark, 0, sizeof(*bookmark));

	bookmark->lat = lat;
	bookmark->lon = lon;

	if (title_len > 0) {
		bookmark->title = malloc(title_len + 1);
		memcpy(bookmark->title, title, title_len);
		bookmark->title[title_len] = 0;
	}

	if (c_text != 0) {
		(*env)->ReleaseStringUTFChars(env, text, c_text);
	}

	return (jlong)(long)(bookmark);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1bookmark_1free(JNIEnv *env, jclass cls, jlong c_bookmark) {
	cmps_jni_bookmark_t *bookmark = bookmarkp(c_bookmark);

	if (bookmark->title != 0) {
		free(bookmark->title);
	}

	free(bookmark);
}

jstring JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1bookmark_1get_1title(JNIEnv *env, jclass cls, jlong c_bookmark) {
	const cmps_jni_bookmark_t *bookmark = bookmarkp(c_bookmark);
	return (bookmark->title != 0 ? (*env)->NewStringUTF(env, bookmark->title) : 0);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1bookmark_1get_1lat(JNIEnv *env, jclass cls, jlong c_bookmark) {
	return bookmarkp(c_bookmark)->lat;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1hrt_1bookmark_1get_1lon(JNIEnv *env, jclass cls, jlong c_bookmark) {
	return bookmarkp(c_bookmark)->lon;
}

