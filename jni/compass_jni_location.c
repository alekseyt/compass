#include <stdlib.h>
#include <string.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1location_1alloc(JNIEnv *env, jclass cls) {
	cmps_location_t *location = malloc(sizeof(*location));
	memset(location, 0, sizeof(*location));
	return (jlong)(long)(location);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1location_1free(JNIEnv *env, jclass cls, jlong c_location) {
	free(locationp(c_location));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1location_1init(JNIEnv *env, jclass cls, jlong c_location) {
	cmps_location_init(locationp(c_location));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1location_1release(JNIEnv *env, jclass cls, jlong c_location) {
	cmps_location_release(locationp(c_location));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1get_1lat(JNIEnv *env, jclass cls, jlong c_location) {
	return locationp(c_location)->lat;
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1set_1lat(JNIEnv *env, jclass cls, jlong c_location, jfloat lat) {
	locationp(c_location)->lat = lat;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1get_1lon(JNIEnv *env, jclass cls, jlong c_location) {
	return locationp(c_location)->lon;
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1set_1lon(JNIEnv *env, jclass cls, jlong c_location, jfloat lon) {
	locationp(c_location)->lon = lon;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1get_1speed(JNIEnv *env, jclass cls, jlong c_location) {
	return locationp(c_location)->speed;
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1set_1speed(JNIEnv *env, jclass cls, jlong c_location, jfloat speed) {
	cmpss_location_set_speed(locationp(c_location), speed);
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1get_1bearing(JNIEnv *env, jclass cls, jlong c_location) {
	return locationp(c_location)->bearing;
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1location_1set_1bearing(JNIEnv *env, jclass cls, jlong c_location, jfloat bearing) {
	cmpss_location_set_bearing(locationp(c_location), bearing);
}
