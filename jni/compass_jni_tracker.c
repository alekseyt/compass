#include <stdlib.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1tracker_1alloc(JNIEnv *env, jclass cls) {
	cmps_tracker_t *tracker = malloc(sizeof(*tracker));
	return (jlong)(long)(tracker);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1tracker_1free(JNIEnv *env, jclass cls, jlong c_tracker) {
	free(trackerp(c_tracker));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1tracker_1init(JNIEnv *env, jclass cls, jlong c_tracker) {
	cmps_tracker_init(trackerp(c_tracker));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1tracker_1release(JNIEnv *env, jclass cls, jlong c_tracker) {
	cmps_tracker_release(trackerp(c_tracker));
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1location(JNIEnv *env, jclass cls, jlong c_tracker, jlong c_location) {
	return cmpss_tracker_get_location(trackerp(c_tracker), locationp(c_location));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1update_1location(JNIEnv *env, jclass cls, jlong c_tracker, jlong c_location, jlong now) {
	cmpss_tracker_update_location(trackerp(c_tracker), locationp(c_location), now);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1has_1location_1heading(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_has_location_heading(trackerp(c_tracker));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1location_1heading(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_get_location_heading(trackerp(c_tracker));
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1has_1magnetic_1heading(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_has_magnetic_heading(trackerp(c_tracker));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1magnetic_1heading(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_get_magnetic_heading(trackerp(c_tracker));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1update_1magnetic_1heading(JNIEnv *env, jclass cls, jlong c_tracker, jfloat azimuth) {
	cmpss_tracker_update_magnetic_heading(trackerp(c_tracker), azimuth);
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1has_1speed(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_has_speed(trackerp(c_tracker));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1speed(JNIEnv *env, jclass cls, jlong c_tracker) {
	return cmpss_tracker_get_speed(trackerp(c_tracker));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1direction(JNIEnv *env, jclass cls, jlong c_tracker, jfloat heading, jlong c_location) {
	return cmpss_tracker_get_direction(trackerp(c_tracker), heading, locationp(c_location));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1tracker_1get_1distance(JNIEnv *env, jclass cls, jlong c_tracker, jlong c_location) {
	return cmpss_tracker_get_distance(trackerp(c_tracker), locationp(c_location));
}
