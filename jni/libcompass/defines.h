#ifndef CMPS_DEFINES_H
#define CMPS_DEFINES_H

#ifndef M_PI /* C99 stuff */
#       define M_PI 3.14159265358979323846
#endif

#define TO_RAD (M_PI / 180)
#define TO_DEG (180 / M_PI)

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

#endif /* CMPS_DEFINES_H */
