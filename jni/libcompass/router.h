#ifndef CMPS_ROUTER_H
#define CMPS_ROUTER_H

#include <pthread.h>
#include <time.h>

#include "tracker.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** @defgroup router Router
 */

/** Router state holder
 * @ingroup router
 */
typedef struct {
	cmps_tracker_t tracker;
	cmps_location_t *target;
	time_t location_update_ms;

	pthread_mutex_t lock;
} cmps_router_t;

/** Router momentum holder
 * @ingroup router
 */
typedef struct {
	float azimuth;
	float distance;
	float direction;
	int direction_is_magnetic;
	int direction_from_gps;
	float speed;
	int has_location;
	cmps_location_t location;
} cmps_router_state_t;

/** Initialize router structure
 * @ingroup router
 */
void cmps_router_init(cmps_router_t *router);

/** Release any resources allocated by router
 * @ingroup router
 */
void cmps_router_release(cmps_router_t *router);

/** Set target location
 * @ingroup router
 *
 * @param location target location
 */
void cmps_router_set_target(cmps_router_t *router, const cmps_location_t *target);

/** Update current location
 * @ingroup router
 *
 * @param location current location
 * @param now current time in unixtime
 */
void cmps_router_update_location(cmps_router_t *router, const cmps_location_t *location, time_t now);

/** Update magnetic heading
 * @ingroup router
 *
 * @param azimuth magnetic azimuth
 */
void cmps_router_update_mag_heading(cmps_router_t *router, float azimuth);

/** Get router state
 * @ingroup router
 *
 * @param state pointer to state struct
 * @param now current time in unix-time
 */
void cmps_router_get_state(cmps_router_t *router, cmps_router_state_t *state, time_t now);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_ROUTER_H */
