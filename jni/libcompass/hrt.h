#ifndef CMPS_HRT_H
#define CMPS_HRT_H

/** @defgroup hrt HRT Protocol
 */

#include <stddef.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** Write bookmark to string buffer, including trailing 0
 * @ingroup hrt
 *
 * This works like snprintf(): you can pass 0 instead of text_size
 * and in return you will get required buffer length. man snprintf
 *
 * @param text string buffer
 * @param text_size text buffer size, must include space for trailing 0
 * @param title title text with trailing 0, might be 0
 * @param lat latitude
 * @param lon longitude
 * @return number of bytes which would have been written to the final string excluding trailing 0
 */
int cmps_hrt_marshall(char *text, size_t text_size, const char *title, float lat, float lon);

/** Read bookmark from string
 * @ingroup hrt
 *
 * @param text bookmark string with trailing 0
 * @param title pointer for title, see also return value
 * @param lat value for latitude
 * @param lon value for longitude
 * @return title length or 0 if there is no title, negative value on parsing error
 */
int cmps_hrt_unmarshall(const char *text, const char **title, float *lat, float *lon);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_HRT_H */
