#ifndef CMPS_UTILS_H
#define CMPS_UTILS_H

/** @defgroup utils Utils
 */

#include "location.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/**
 * @ingroup utils
 * @return lat in range -90..90
 */
float cmps_sanitize_lat(float lat);

/**
 * @ingroup utils
 * @return lon in range -180..180
 */
float cmps_sanitize_lon(float lon);

/** Covert negative angle into positive, fix overflow, etc
 * @ingroup utils
 *
 * @return azimuth in 0..360
 */
float cmps_normalize_azimuth(float azimuth);

/** Calculate angle to target
 * @ingroup utils
 *
 * @return angle in 0..359
 */
float cmps_direction(const cmps_location_t *here, const cmps_location_t *there);

/** Calculate geodesic distance between two points
 * @ingroup utils
 *
 * @param location first points
 * @param targete second point
 */
float cmps_distance(const cmps_location_t *location, const cmps_location_t *target);

/** Transform angle to scale of directions. for instance,
 * @ingroup utils
 *
 * 4 directions:
 *
 *          0
 *          ^
 *          |
 *          |
 * 270 <----o----> 90
 *          |
 *          |
 *          v
 *         180
 *
 * 44 degrees will be transformed into 0 degrees
 * 50 degrees will be transformed into 90 degrees
 * and so
 *
 * @return basically angle rounded to most appropriate direction
 */
float cmps_azimuth_to_direction(float azimuth, int directions);

/** Calculate absolute magnetic field value
 * @ingroup utils
 *
 * @param values magnetic field strength in x, y, z axis
 * @return absolute magnetic field strength
 */
float cmps_magnetic_field_sqr_strength(float x, float y, float z);

/** Check if (square) magnetic field strength is in reasonable range
 * @ingroup utils
 *
 * http://en.wikipedia.org/wiki/Earth's_magnetic_field
 *
 * @param sqr_strength square magnetic field strength, uT
 * @return true if it's ok
 */
int cmps_sqr_magnetic_field_valid(float sqr_strength);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_UTILS_H */
