#include "low_pass_filter.h"

float cmps_low_pass_filter(float input, float output, float alpha) {
	return output + alpha * (input - output);
}
