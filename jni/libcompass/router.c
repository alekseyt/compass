#include <stdlib.h>

#include "config.h"
#include "router.h"
#include "utils.h"

void cmps_router_init(cmps_router_t *router) {
	cmps_tracker_init(&(router->tracker));
	router->target = 0;
	router->location_update_ms = 0;

	pthread_mutex_init(&(router->lock), 0);
}

void cmps_router_release(cmps_router_t *router) {
	if (router->target != 0) {
		free(router->target);
		router->target = 0;
	}
	cmps_tracker_release(&(router->tracker));

	pthread_mutex_destroy(&(router->lock));
}

void cmps_router_set_target(cmps_router_t *router, const cmps_location_t *target) {
	if (target == 0) {
		if (router->target != 0) {
			free(router->target);
		}

		router->target = 0;
		return;
	}

	if (router->target == 0) {
		router->target = malloc(sizeof(*(router->target)));
	}

	*(router->target) = *target;
}

void cmps_router_update_location(cmps_router_t *router, const cmps_location_t *location, time_t now) {
	cmps_tracker_update_location(&(router->tracker), location, now);
	router->location_update_ms = (location == 0 ? 0 : now);
}

void cmps_router_update_mag_heading(cmps_router_t *router, float azimuth) {
	cmps_tracker_update_magnetic_heading(&(router->tracker), azimuth);
}

void cmps_router_get_state(cmps_router_t *router, cmps_router_state_t *state, time_t now) {
	/* select speed */
	state->speed = 0;

	if (cmps_tracker_has_speed(&(router->tracker))
	&& now - router->location_update_ms < ROUTER_SPEED_TTL) {
		state->speed = cmps_tracker_get_speed(&(router->tracker));
	}

	/* select heading */
	state->azimuth = 0;
	state->direction_is_magnetic = 0;
	state->direction_from_gps = 0;

	/* select location */
	state->has_location = cmps_tracker_get_location(&(router->tracker), &(state->location));

	if (cmps_tracker_has_location_heading(&(router->tracker))
	&& now - router->location_update_ms < ROUTER_LOCATION_TTL) {
		state->azimuth = cmps_tracker_get_location_heading(&(router->tracker));
		state->direction_from_gps = 1;
	}
	else if (cmps_tracker_has_magnetic_heading(&(router->tracker))) {
		/* don't set direction to magnetic if target is set, but no
		 * location is available */
		if (router->target == 0 || state->has_location) {
			state->azimuth = cmps_tracker_get_magnetic_heading(&(router->tracker));
			state->direction_is_magnetic = 1;
		}
	}

	/* select direction and distance
	 * order matters: azimuth should be set before direction */
	if (router->target != 0) {
		state->direction = cmps_tracker_get_direction(&(router->tracker), state->azimuth, router->target);
		state->distance = cmps_tracker_get_distance(&(router->tracker), router->target);
	}
	else {
		state->direction = cmps_normalize_azimuth(-state->azimuth);
	}
}
