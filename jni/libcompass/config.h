#ifndef CMPS_CONFIG_H
#define CMPS_CONFIG_H

#define TRACKER_ANGLE_ALPHA 0.15f /* 15%, used for low-pass filter in tracker */
#define TRACKER_SPEED_ALPHA 0.15f /* 15%, used for low-pass filter in tracker */

#define ROUTER_LOCATION_TTL 10 * 1000 /* msecs */
#define ROUTER_SPEED_TTL 2 * 1000 /* msecs */

#define DISTRESS_ALERT_LEVEL_ARMED 5
#define DISTRESS_ALERT_LEVEL_TTL 5000 /* msecs */
#define DISTRESS_ARMED_TTL 5000 /* msecs */
#define G 9.8
#define DISTRESS_FORCE_THRESHOLD 16 * G /* square value, 4G really */

#define MAX_NUMBER_LENGTH 32

#endif /* CMPS_CONFIG_H */
