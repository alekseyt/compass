#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <libnu/libnu.h>

#include "hrt.h"

#define SIGN_FORMAT "%s"
#define INTEGRAL_PART_FORMAT "%d"
#define DECIMAL_PART_FORMAT "%06d"
#define COORD_FORMAT SIGN_FORMAT INTEGRAL_PART_FORMAT "." DECIMAL_PART_FORMAT
#define DECIMAL_SCALE 1000000 /* 6 decimal digits */

#define SIGN(x) ((x) < 0 ? "-" : "")

/* TODO: cover snprintf contract with tests */
int cmps_hrt_marshall(char *text, size_t text_size, const char *title, float lat, float lon) {

	/* split coords into integral and decimal parts, then join with dot
	 * this is to avoid locale-dependent radix sign in %.6f */

	float lat_integral = 0;
	float lat_decimal = fabs(modff(lat, &lat_integral)) * DECIMAL_SCALE;
	lat_integral = fabs(lat_integral);

	float lon_integral = 0;
	float lon_decimal = fabs(modff(lon, &lon_integral)) * DECIMAL_SCALE;
	lon_integral = fabs(lon_integral);

	if (text_size == 0) {
		return (title != 0 ? strlen(title) + 1 /* whitespace */ : 0)
		+ snprintf(0, 0, COORD_FORMAT, SIGN(lat), (int)(lat_integral), (int)(lat_decimal))
		+ 1 /* comma */
		+ snprintf(0, 0, COORD_FORMAT, SIGN(lon), (int)(lon_integral), (int)(lon_decimal));
	}

	if (title != 0 && strlen(title) > 0) {
		return snprintf(text, text_size,
		"%s " COORD_FORMAT "," COORD_FORMAT,
		title,
		SIGN(lat), (int)(lat_integral), (int)(lat_decimal),
		SIGN(lon), (int)(lon_integral), (int)(lon_decimal));
	}
	else {
		return snprintf(text, text_size,
		COORD_FORMAT "," COORD_FORMAT,
		SIGN(lat), (int)(lat_integral), (int)(lat_decimal),
		SIGN(lon), (int)(lon_integral), (int)(lon_decimal));
	}
}

/**
 * @return pointer to title end
 */
static const char* probe(const char *text, const char *comma, float *lat, float *lon) {
	int done = sscanf(comma + 1, "%f", lon);
	if (done != 1) {
		return 0;
	}

	const char *plat = 0; /* pointer to lat */

	const char *whitespace = comma;
	uint32_t w = 0;

	while (whitespace > text) {
		whitespace = nu_cesu8_revread(&w, whitespace);
		if (w == 0x0020) { /* ascii whitespace */
			plat = whitespace + 1;
			break;
		}
		else if (w == 0x00A0) { /* unicode whitespace */
			plat = whitespace + 2;
			break;
		}
		else if (w == 0x000A || w == 0x000D) { /* \n or \r */
			plat = whitespace + 1;
			break;
		}
	}

	if (whitespace == text) {
		plat = text; /* try from the beginning */
	}

	/* validate content between whitespace and comma
	 * w/o this pass this probe won't be entirely correct: 1.456wubwub,2.345 */

	const char *p = plat;
	while (++p != comma) {
		char c = *p;
		if (c < '0' && c > '9'
		&& c != '+' && c != '-'
		&& c != '.') {
			return 0;
		}
	}

	done = sscanf(plat, "%f", lat);
	if (done != 1) {
		return 0;
	}

	return (plat == text ? text : whitespace);
}

int cmps_hrt_unmarshall(const char *text, const char **title, float *lat, float *lon) {

	/* it's simple: lat and lon are delimited by comma
	 * so parser need to find floating point value after the comma
	 * then floating point value before the comma
	 * evering ahead of this group is a title */

	const char *comma = text, *whitespace = 0;
	float tlat = 0, tlon = 0;
	do {
		comma = strchr(comma + 1, ','); /* safe to do comma + 1 even if there is no title */
		if (comma != 0) {
			whitespace = probe(text, comma, &tlat, &tlon);
			if (whitespace != 0) {
				if (lat != 0) {
					*lat = tlat;
				}

				if (lon != 0) {
					*lon = tlon;
				}

				break;
			}
		}
	} while (comma != 0);

	if (whitespace == 0) {
		return -EINVAL;
	}

	const char *begin = whitespace;
	while (begin > text && *begin != '\n' && *begin != '\r') {
		--begin;
	}

	/* trim front */
	while ((*begin == ' ' || *begin == '\n' || *begin == '\r')
		&& begin < whitespace) {
		++begin;
	}

	/* trim back */
	while (whitespace > begin && *(whitespace - 1) == ' ') {
		--whitespace;
	}

	if (begin != whitespace && title != 0) {
		*title = begin;
	}

	return (whitespace - begin);
}
