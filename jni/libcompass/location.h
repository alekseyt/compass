#ifndef CMPS_LOCATION_H
#define CMPS_LOCATION_H

#include <pthread.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** @defgroup location Locations
 */

/** Location with possibly embedded speed and bearing
 * @ingroup location
 */
typedef struct {
	float lat;
	float lon;
	float bearing; /* it will be set to 0 if not available */
	float speed; /* will also be set to 0 when not available */
	int has_bearing;
	int has_speed;

	pthread_mutex_t lock;
} cmps_location_t;

/** Initialize location holder
 * @ingroup location
 */
void cmps_location_init(cmps_location_t *location);

/** Release any resources allocated by holder
 * @ingroup location
 */
void cmps_location_release(cmps_location_t *location);

/** Set speed value
 * @ingroup location
 */
void cmps_location_set_speed(cmps_location_t *location, float speed);

/** Set bearing value
 * @ingroup location
 */
void cmps_location_set_bearing(cmps_location_t *location, float bearing);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_LOCATION_H */
