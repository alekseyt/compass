#ifndef CMPS_LOW_PASS_FILTER_H
#define CMPS_LOW_PASS_FILTER_H

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** @defgroup filters Filters
 */

/** Low-pass filter for two floats
 * @ingroup filters
 *
 * http://en.wikipedia.org/wiki/Low-pass_filter
 *
 * @param input (previous) value
 * @param output new value
 * @return new value filtered
 */
float cmps_low_pass_filter(float input, float output, float alpha);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_LOW_PASS_FILTER_H */
