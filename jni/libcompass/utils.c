#include <math.h>

#include "defines.h"
#include "utils.h"

float cmps_sanitize_lat(float lat) {
	return (lat < -90 ? -90 : (
	lat > 90 ? 90 : lat));
}

float cmps_sanitize_lon(float lon) {
	return (lon < -180 ? -180 : (
	lon > 180 ? 180 : lon));
}

float cmps_direction(const cmps_location_t *here, const cmps_location_t *there) {
	return (float)atan2(there->lon - here->lon,
	there->lat - here->lat) * (float)(180.0f / M_PI);
}

float cmps_distance(const cmps_location_t *location, const cmps_location_t *target) {
	/* http://www.gcmap.com/faq/gccalc
	 */
	const float R = 6371.0f;

	const float lat1 = location->lat * TO_RAD;
	const float lat2 = target->lat * TO_RAD;
	const float lon1 = location->lon * TO_RAD;
	const float lon2 = target->lon * TO_RAD;
	const float dlon = lon1 - lon2;

	const float d = acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon));

	return (d < 0 ? d + M_PI : d) * R * 1000; /* return distance in meters */
}

float cmps_normalize_azimuth(float azimuth) {
	/* bring value to 0..-359 */
	if (azimuth < 0) {
		azimuth = fmod(azimuth, -360);
	}

	/* bring value to 0..359 */
	if (azimuth >= 360) {
		azimuth = fmod(azimuth, 360);
	}

	/* convert value to positive */
	if (azimuth < 0) {
		azimuth += 360;
	}

	return azimuth;
}

float cmps_azimuth_to_direction(float azimuth, int directions) {
	return ((int)(azimuth / (360.0f / directions))) * (360.0f / directions);
}

float cmps_magnetic_field_sqr_strength(float x, float y, float z) {
	return x * x + y * y + z * z;
}

int cmps_sqr_magnetic_field_valid(float sqr_strength) {
	return (20 * 20 <= sqr_strength && sqr_strength <= 70 * 70)
	? 1 : 0;
}
