#ifndef CMPS_CALLEE_LOOKUP_H
#define CMPS_CALLEE_LOOKUP_H

#include <pthread.h>
#include <time.h>

#include "config.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** Callee record representation
 * @ingroup distress
 */
typedef struct { /* this need to be copyable with operator= */
	char number[MAX_NUMBER_LENGTH + 1]; /* + trailing 0 */
	size_t name_len;
	time_t timestamp; /* last call timestamp */
	long unsigned calls_number;
	long unsigned duration; /* seconds */
} cmps_callee_lookup_rec_t;

/** Callee lookup holder
 * @ingroup distress
 */
typedef struct {
	cmps_callee_lookup_rec_t *array;
	unsigned array_size;
	unsigned array_used;
	int need_sorting;

	pthread_mutex_t lock;
} cmps_callee_lookup_t;

/** Initialize lookup struct
 * @ingroup distress
 */
void cmps_callee_lookup_init(cmps_callee_lookup_t *lookup);

/** Release any resources allocated for callee lookup
 * @ingroup distress
 */
void cmps_callee_lookup_release(cmps_callee_lookup_t *lookup);

/** Cleanup callee lookup without reallocation
 * @ingroup distress
 */
void cmps_callee_lookup_reset(cmps_callee_lookup_t *lookup);

/** Add call to lookup
 * @ingroup disress
 *
 * @param number phone number
 * @param name contact name or 0
 * @param timestamp call timestamp
 * @param duration call duration
 * @return 0 on success or negative error
 */
int cmps_callee_lookup_push_call(cmps_callee_lookup_t *lookup, const char *number, const char *name, time_t timestamp, unsigned long duration);

/** Bestness is basically index in callees scoreboard
 * @ingroup distress
 *
 * If you need to select best one - pass 0
 * If you want to select second best one - pass 1
 *
 * @param bestness index in best callees list
 * @param best output callee
 * @return 0 on success negative value on negative error
 */
int cmps_callee_lookup_best(cmps_callee_lookup_t *lookup, unsigned bestness, cmps_callee_lookup_rec_t *best);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_CALLEE_LOOKUP_H */
