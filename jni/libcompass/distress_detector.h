#ifndef CMPS_DISTRESS_DETECTOR_H
#define CMPS_DISTRESS_DETECTOR_H

#include <pthread.h>
#include <time.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** @defgroup distress Distress detection
 */

/** Ditress state holder
 * @ingroup distress
 */
typedef struct {
	int alert_level;
	float force;
	time_t alert_timestamp;
	time_t armed_timestamp;

	pthread_mutex_t lock;
} cmps_distress_detector_t;

/** Initialized detector holder
 * @ingroup distress
 */
void cmps_distress_init(cmps_distress_detector_t *detector);

/** Release any resources allocated by detector holder
 * @ingroup distress
 */
void cmps_distress_release(cmps_distress_detector_t *detector);

/** Reset distress call status (to unarmed)
 * @ingroup distress
 *
 * @param now current time in unixtime
 */
void cmps_distress_reset(cmps_distress_detector_t *detector);

/** Feed acceleration vector
 *
 * @ingroup distress
 * @param values acceleration force in x-y-z planes
 * @param now current time in unixtime
 */
void cmps_distress_update_acceleration(cmps_distress_detector_t *detector, float x, float y, float z, time_t now);

/** Feed alert button status
 * @ingroup distress
 *
 * @param on alert button on or off
 * @param now current time in unixtime
 */
void cmps_distress_update_alert_button(cmps_distress_detector_t *detector, int on, time_t now);

/**
 * @ingroup distress
 *
 * @param now current time in unixtime
 * @return true if distress detector is armed (stage 1 complete)
 */
int cmps_distress_armed(cmps_distress_detector_t *detector, time_t now);

/**
 * @ingroup distress
 *
 * @param now current time in unixtime
 * @return true if requred strength was applied to complete stage 2 of distress call
 */
int cmps_distress_pushed(cmps_distress_detector_t *detector, time_t now);

/** 
 * @ingroup distress
 *
 * @param now current time in unixtime
 * @return true on overall distress status (armed + pushed), i.e. true in panic mode
 */
int cmps_distress_panic(cmps_distress_detector_t *detector, time_t now);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_DISTRESS_DETECTOR_H */
