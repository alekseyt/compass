#ifndef CMPS_SYNCHRONIZED_H
#define CMPS_SYNCHRONIZED_H

#include "config.h"

#include "callee_lookup.h"
#include "distress_detector.h"
#include "dynamo.h"
#include "hrt.h"
#include "location.h"
#include "low_pass_filter.h"
#include "router.h"
#include "screen_filter.h"
#include "tracker.h"
#include "utils.h"

/** @defgroup synchronized Synchronized calls
 *
 * All calls in cmpss_ are automatically locked on corresponding object
 * and therefore thread-safe. In all other regard they are complete equivalents
 * of corresponding cmps_ calls.
 *
 * Calls missing complementary cmpss are intentionally left that way
 * indicate that they don't need thread-safety or should never be called by 
 * multiple threads (as init()/release() calls)
 */

#define LOCK(x) (pthread_mutex_lock(&((x)->lock)))
#define UNLOCK(x) (pthread_mutex_unlock(&((x)->lock)))

#define SYNC_VOID(func, obj, ...) \
	LOCK((obj)); \
	(func)((obj), ## __VA_ARGS__); \
	UNLOCK((obj));

#define SYNC_RET(func, obj, ...) \
	(ret); \
	LOCK((obj)); \
	(ret) = (func)((obj), ## __VA_ARGS__); \
	UNLOCK((obj)); \
	return ret;

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/* location */

static inline void cmpss_location_set_speed(cmps_location_t *location, float speed) {
	SYNC_VOID(cmps_location_set_speed, location, speed);
}

static inline void cmpss_location_set_bearing(cmps_location_t *location, float bearing) {
	SYNC_VOID(cmps_location_set_bearing, location, bearing);
}

/* tracker */

static inline int cmpss_tracker_get_location(cmps_tracker_t *tracker, cmps_location_t *location) {
	int SYNC_RET(cmps_tracker_get_location, tracker, location);
}

static inline void cmpss_tracker_update_location(cmps_tracker_t *tracker, const cmps_location_t *location, long now) {
	SYNC_VOID(cmps_tracker_update_location, tracker, location, now);
}

static inline int cmpss_tracker_has_location_heading(cmps_tracker_t *tracker) {
	int SYNC_RET(cmps_tracker_has_location_heading, tracker);
}

static inline float cmpss_tracker_get_location_heading(cmps_tracker_t *tracker) {
	float SYNC_RET(cmps_tracker_get_location_heading, tracker);
}

static inline int cmpss_tracker_has_magnetic_heading(cmps_tracker_t *tracker) {
	int SYNC_RET(cmps_tracker_has_magnetic_heading, tracker);
}

static inline float cmpss_tracker_get_magnetic_heading(cmps_tracker_t *tracker) {
	float SYNC_RET(cmps_tracker_get_magnetic_heading, tracker);
}

static inline void cmpss_tracker_update_magnetic_heading(cmps_tracker_t *tracker, float azimuth) {
	SYNC_VOID(cmps_tracker_update_magnetic_heading, tracker, azimuth);
}

static inline int cmpss_tracker_has_speed(cmps_tracker_t *tracker) {
	int SYNC_RET(cmps_tracker_has_speed, tracker);
}

static inline float cmpss_tracker_get_speed(cmps_tracker_t *tracker) {
	float SYNC_RET(cmps_tracker_get_speed, tracker);
}

static inline float cmpss_tracker_get_direction(cmps_tracker_t *tracker, float heading, const cmps_location_t *target) {
	float SYNC_RET(cmps_tracker_get_direction, tracker, heading, target);
}

static inline float cmpss_tracker_get_distance(cmps_tracker_t *tracker, const cmps_location_t *target) {
	float SYNC_RET(cmps_tracker_get_distance, tracker, target);
}

/* router */

static inline void cmpss_router_set_target(cmps_router_t *router, const cmps_location_t *target) {
	SYNC_VOID(cmps_router_set_target, router, target);
}

static inline void cmpss_router_update_location(cmps_router_t *router, const cmps_location_t *location, time_t now) {
	SYNC_VOID(cmps_router_update_location, router, location, now);
}

static inline void cmpss_router_update_mag_heading(cmps_router_t *router, float azimuth) {
	SYNC_VOID(cmps_router_update_mag_heading, router, azimuth);
}

static inline void cmpss_router_get_state(cmps_router_t *router, cmps_router_state_t *state, time_t now) {
	SYNC_VOID(cmps_router_get_state, router, state, now);
}

/* distress detector */

static inline void cmpss_distress_reset(cmps_distress_detector_t *detector) {
	SYNC_VOID(cmps_distress_reset, detector);
}

static inline void cmpss_distress_update_acceleration(cmps_distress_detector_t *detector, float x, float y, float z, time_t now) {
	SYNC_VOID(cmps_distress_update_acceleration, detector, x, y, z, now);
}

static inline void cmpss_distress_update_alert_button(cmps_distress_detector_t *detector, int on, time_t now) {
	SYNC_VOID(cmps_distress_update_alert_button, detector, on, now);
}

static inline int cmpss_distress_armed(cmps_distress_detector_t *detector, time_t now) {
	int SYNC_RET(cmps_distress_armed, detector, now);
}

static inline int cmpss_distress_pushed(cmps_distress_detector_t *detector, time_t now) {
	int SYNC_RET(cmps_distress_pushed, detector, now);
}

static inline int cmpss_distress_panic(cmps_distress_detector_t *detector, time_t now) {
	int SYNC_RET(cmps_distress_panic, detector, now);
}

/* callee lookup */

static inline void cmpss_callee_lookup_reset(cmps_callee_lookup_t *lookup) {
	SYNC_VOID(cmps_callee_lookup_reset, lookup);
}

static inline int cmpss_callee_lookup_push_call(cmps_callee_lookup_t *lookup, const char *number, const char *name, time_t timestamp, unsigned long duration) {
	int SYNC_RET(cmps_callee_lookup_push_call, lookup, number, name, timestamp, duration);
}

static inline int cmpss_callee_lookup_best(cmps_callee_lookup_t *lookup, unsigned bestness, cmps_callee_lookup_rec_t *best) {
	int SYNC_RET(cmps_callee_lookup_best, lookup, bestness, best);
}

/* dynamo */

static inline void cmpss_dynamo_reset(cmps_dynamo_t *dynamo) {
	SYNC_VOID(cmps_dynamo_reset, dynamo);
}

static inline void cmpss_dynamo_next(cmps_dynamo_t *dynamo) {
	SYNC_VOID(cmps_dynamo_next, dynamo);
}

static inline int cmpss_dynamo_get_number(cmps_dynamo_t *dynamo, const char *preset_number, cmps_callee_lookup_t *lookup,
	char best_number[MAX_NUMBER_LENGTH + 1]) {
	int SYNC_RET(cmps_dynamo_get_number, dynamo, preset_number, lookup, best_number);
}

/* screen */

static inline void cmpss_screen_filter_reset(cmps_screen_filter_t *filter) {
	SYNC_VOID(cmps_screen_filter_reset, filter);
}

static inline float cmpss_screen_filter_get_current(cmps_screen_filter_t *filter) {
	float SYNC_RET(cmps_screen_filter_get_current, filter);
}

static inline float cmpss_screen_filter_pass(cmps_screen_filter_t *filter, float current) {
	float SYNC_RET(cmps_screen_filter_pass, filter, current);
}

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#undef LOCK
#undef UNLOCK
#undef SYNC_VOID
#undef SYNC_RET

#endif /* CMPS_SYNCHRONIZED_H */
