#ifndef CMPS_DYNAMO_H
#define CMPS_DYNAMO_H

#include <pthread.h>

#include "config.h"
#include "callee_lookup.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** Dynamo holder
 * @ingroup distress
 */
typedef struct {
	unsigned calls_counter;

	pthread_mutex_t lock;
} cmps_dynamo_t;

/** Initialize dynamo
 * @ingroup distress
 */
void cmps_dynamo_init(cmps_dynamo_t *dynamo);

/** Release any resources allocated for dynamo
 * @ingroup distress
 */
void cmps_dynamo_release(cmps_dynamo_t *dynamo);

/** Reset dynamo (start over)
 * @ingroup distress
 */
void cmps_dynamo_reset(cmps_dynamo_t *dynamo);

/** Rotate dynamo once
 *
 * Note: do not expect dynamo to rotate sequentially
 * call this when you need to switch to another number,
 * but do not expect that dynamo will rotate twice
 * if you call this twice. This is not an array iterator
 *
 * @ingroup distress
 * @see cmps_dynamo_get_number
 */
void cmps_dynamo_next(cmps_dynamo_t *dynamo);

/** Get best number either preset one of from callee lookup
 * This number will be the same until dynamo is rotated
 *
 * @ingroup dynamo
 * @param preset_number present number or 0
 * @param lookup callee lookup to operate on
 * @return 0 if number was written or negative error
 *
 * @see cmps_dynamo_next
 */
int cmps_dynamo_get_number(cmps_dynamo_t *dynamo, const char *preset_number, cmps_callee_lookup_t *lookup, 
	char best_number[MAX_NUMBER_LENGTH + 1]);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_DYNAMO_H */
