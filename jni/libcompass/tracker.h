#ifndef CMPS_TRACKER_H
#define CMPS_TRACKER_H

/** @defgroup tracker Tracker
 */

#include <pthread.h>

#include "location.h"

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** Tracker state holder
 * @ingroup tracker
 */
typedef struct {
	cmps_location_t *last_location;

	int has_magnetic_heading;
	float magnetic_azimuth;

	int has_location_heading;
	float location_azimuth;

	int has_speed;
	float speed;

	pthread_mutex_t lock;
} cmps_tracker_t;

/** Initialize tracker structure
 * @ingroup tracker
 */
void cmps_tracker_init(cmps_tracker_t *tracker);

/** Release any memory possibly allocated by tracker
 * @ingroup tracker
 */
void cmps_tracker_release(cmps_tracker_t *tracker);

/**
 * @ingroup tracker
 * @param location location struct to fill
 * @return false if there no location available, true otherwise
 */
int cmps_tracker_get_location(cmps_tracker_t *tracker, cmps_location_t *location);

/** Update current location
 * This call will also update speed and GPS heading if available
 * @ingroup tracker
 *
 * @param location current location with embedded speed and heading
 */
void cmps_tracker_update_location(cmps_tracker_t *tracker, const cmps_location_t *location, long now);

/**
 * @ingroup tracker
 *
 * @return true if GPS heading was set previously
 */
int cmps_tracker_has_location_heading(cmps_tracker_t *tracker);

/** Get azimuth as calculated by GPS
 * @ingroup tracker
 *
 * @return azimuth in range 0..359
 */
float cmps_tracker_get_location_heading(cmps_tracker_t *tracker);

/**
 * @ingroup tracker
 *
 * @return true if magnetic heading was set previously
 */
int cmps_tracker_has_magnetic_heading(cmps_tracker_t *tracker);

/** Get magnetic azimuth
 * @ingroup tracker
 *
 * @return azimuth in range 0..359
 */
float cmps_tracker_get_magnetic_heading(cmps_tracker_t *tracker);

/**
 * @ingroup tracker
 *
 * @param azimuth angle between north and target (phone head)
 * @param now current time in unixtime
 */
void cmps_tracker_update_magnetic_heading(cmps_tracker_t *tracker, float azimuth);

/**
 * @ingroup tracker
 *
 * @param now current time in unixtime
 * @return true if speed was updated recently enough
 */
int cmps_tracker_has_speed(cmps_tracker_t *tracker);

/**
 * @ingroup tracker
 *
 * @return speed in m/s
 */
float cmps_tracker_get_speed(cmps_tracker_t *tracker);

/** Get direction to target
 * @ingroup tracker
 *
 * @param heading magnetic of gps heading
 * @param target location to get direction to
 * @return direction angle in range 0..359
 */
float cmps_tracker_get_direction(cmps_tracker_t *tracker, float heading, const cmps_location_t *target);

/** Calculate geodesic distance
 * @ingroup tracker
 *
 * @param target location to calcultate distance to
 * @return distance distance to target in meters
 */
float cmps_tracker_get_distance(cmps_tracker_t *tracker, const cmps_location_t *target);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_TRACKER_H */
