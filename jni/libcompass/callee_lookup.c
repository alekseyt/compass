#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "callee_lookup.h"
#include "defines.h"

#define BASE_ARRAY_LENGTH 32 /* assuming there are at least 32 people in call history */
#define CALLEE_LOOKUP_TIMESTAMP_SCALE 1000000000.0f /* scale of miliseconds */

#define NO_NAME ((size_t)(-1))

void cmps_callee_lookup_init(cmps_callee_lookup_t *lookup) {
	lookup->array = malloc(sizeof(*(lookup->array)) * BASE_ARRAY_LENGTH);
	lookup->array_size = BASE_ARRAY_LENGTH;
	lookup->array_used = 0;
	lookup->need_sorting = 0;

	pthread_mutex_init(&(lookup->lock), 0);
}

void cmps_callee_lookup_release(cmps_callee_lookup_t *lookup) {
	free(lookup->array);
	lookup->array_size = 0;
	lookup->array_used = 0;
	lookup->need_sorting = 1;

	pthread_mutex_destroy(&(lookup->lock));
}

void cmps_callee_lookup_reset(cmps_callee_lookup_t *lookup) {
	lookup->array_used = 0;
}

static ssize_t find_number(const cmps_callee_lookup_t *lookup, const char *number) {
	size_t i = 0; for (; i < lookup->array_used; ++i) {
		if (strcmp(lookup->array[i].number, number) == 0) {
			return i;
		}
	}

	return -1;
}

int cmps_callee_lookup_push_call(cmps_callee_lookup_t *lookup, const char *number, const char *name, time_t timestamp, unsigned long duration) {
	if (strlen(number) > MAX_NUMBER_LENGTH) {
		return -E2BIG;
	}

	const size_t name_len = ((name == 0 || strlen(name) == 0) ? NO_NAME : strlen(name));

	lookup->need_sorting = 1;

	ssize_t index = find_number(lookup, number);
	if (index != -1) {
		cmps_callee_lookup_rec_t *rec = lookup->array + index;
		rec->timestamp = MAX(timestamp, rec->timestamp);
		rec->duration += duration;
		rec->calls_number += 1;
		rec->name_len = MIN(rec->name_len, name_len); /* prefer shorter name if it's different for whatever reason */

		return 0;
	}

	/* number not found */

	if (lookup->array_used + 1 > lookup->array_size) {
		size_t new_size = lookup->array_size * 2;

		/* allocate twice more memory */
		lookup->array = realloc(lookup->array, sizeof(*(lookup->array)) * new_size);
		lookup->array_size = new_size;
	}

	cmps_callee_lookup_rec_t rec = { { 0 } }; /* no need to set trailing 0 for the number */
	memcpy(rec.number, number, MIN(MAX_NUMBER_LENGTH, strlen(number) + 1));
	rec.name_len = name_len;
	rec.timestamp = timestamp;
	rec.duration = duration;
	rec.calls_number = 1;

	lookup->array[lookup->array_used] = rec;
	lookup->array_used += 1;

	return 0;
}

static int bad_number(const cmps_callee_lookup_rec_t *callee) {
	/* ignore callees with a single record or without name
	 */
	return ((callee->calls_number < 2 || callee->name_len == NO_NAME) ? 1 : 0);
}

static int ignore_number(const cmps_callee_lookup_rec_t *callee) {
	/* ignore short numbers
	 *
	 * <s>5-digit numbers are still rolling in Russia and other
	 * countries of the Soviet Block, but those are land-line
	 * number with no SMS support</s>
	 *
	 * 5-digit numbers in Russia with SMS support :(
	 *
	 * assuming it's safe to ignore numbers shorter than <s>7</s> 5 digits
	 */
	return ((strlen(callee->number) < 5) ? 1 : 0);
}

/** Callee base score in measureless comparable unit
 */
static float base_score(const cmps_callee_lookup_rec_t *callee) {
	/* skip some numbers completely */
	if (ignore_number(callee) != 0) {
		return -1;
	}

	/* put bad numbers to the end of the list as a measure of last resort */
	if (bad_number(callee) != 0) {
		return 0;
	}

	/* normally timestamp is an unixtime and unixtime is
	 * always increasing. that means that latest calls has higher
	 * timestamp value and that value can be used for base score:
	 * higher timestamp -> recent call -> higher score
	 *
	 * divide timestamp by 10^9 for normalization
	 * this will produce base score > 1.0
	 */
	return callee->timestamp / CALLEE_LOOKUP_TIMESTAMP_SCALE;
}

static float duration_score(const cmps_callee_lookup_rec_t *callee, float duration_scale) {
	return (callee->duration / duration_scale);
}

/** Produce derivative duration score based on number of calls
 */
static float calls_score(const cmps_callee_lookup_rec_t *callee, float calls_scale) {
	return 1 / (callee->calls_number / calls_scale);
}

static float name_len_score(const cmps_callee_lookup_rec_t *callee, float name_len_scale) {
	return 1 / (callee->name_len / name_len_scale);
}

static int callee_comp(const void *v1, const void *v2) {
	const cmps_callee_lookup_rec_t *lhs = (const cmps_callee_lookup_rec_t *)(v1);
	const cmps_callee_lookup_rec_t *rhs = (const cmps_callee_lookup_rec_t *)(v2);

	/* base score from timestamp, higher timestamp -> higher score */
	float lhs_base_score = base_score(lhs);
	float rhs_base_score = base_score(rhs);

	/* if one of the callees is not comparable */
	if (lhs_base_score <= 0 || rhs_base_score <= 0) {
		if (lhs_base_score < rhs_base_score) {
			return 1;
		}
		else if (lhs_base_score > rhs_base_score) {
			return -1;
		}

		return 0;
	}

	assert(lhs_base_score > 0);
	assert(rhs_base_score > 0);

	/* weight based on contact's name length
	 * applied as penalty to base score: 0.85 and so
	 * shorter name will have weight == 1.0
	 *
	 * will always be greater than zero since NO_NAME
	 * is underflowed (size_t)(-1) */
	float name_len_scale = MIN(lhs->name_len, rhs->name_len);
	float lhs_name_len_score = name_len_score(lhs, name_len_scale);
	float rhs_name_len_score = name_len_score(rhs, name_len_scale);

	/* this is basically a weight based on calls duration
	 * longer duration -> higher score */
	float duration_scale = MAX(lhs->duration, rhs->duration);
	float lhs_duration_score = duration_score(lhs, duration_scale);
	float rhs_duration_score = duration_score(rhs, duration_scale);

	/* reduce weight by calls number, more calls -> lower weight
	 * i.e. longer calls -> higher score */
	float calls_scale = MIN(lhs->calls_number, rhs->calls_number);
	lhs_duration_score *= calls_score(lhs, calls_scale);
	rhs_duration_score *= calls_score(rhs, calls_scale);

	float lhs_score = lhs_base_score * lhs_name_len_score + lhs_duration_score;
	float rhs_score = rhs_base_score * rhs_name_len_score + rhs_duration_score;

	/* for debugging
	__android_log_print(ANDROID_LOG_ERROR, "", "lhs number: %s", lhs->number);
	__android_log_print(ANDROID_LOG_ERROR, "", "lhs base score: %f", lhs_base_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "lhs name score: %f", lhs_name_len_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "lhs duration score: %f", lhs_duration_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "lhs score: %f", lhs_score);

	__android_log_print(ANDROID_LOG_ERROR, "", "rhs number: %s", rhs->number);
	__android_log_print(ANDROID_LOG_ERROR, "", "rhs base score: %f", rhs_base_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "rhs name score: %f", rhs_name_len_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "rhs duration score: %f", rhs_duration_score);
	__android_log_print(ANDROID_LOG_ERROR, "", "rhs score: %f", rhs_score);
	*/

	if (lhs_score > rhs_score) {
		return -1;
	}

	if (lhs_score < rhs_score) {
		return 1;
	}

	return 0;
}

static int has_score(const cmps_callee_lookup_rec_t *callee) {
	return (ignore_number(callee) != 0 ? 0 : 1);
}

int cmps_callee_lookup_best(cmps_callee_lookup_t *lookup, unsigned bestness, cmps_callee_lookup_rec_t *best) {
	if (lookup->array_used <= bestness) {
		return -ERANGE;
	}

	if (lookup->need_sorting != 0) {
		qsort(lookup->array, lookup->array_used, sizeof(*(lookup->array)), callee_comp);
		lookup->need_sorting = 0;
	}

	*best = lookup->array[bestness];

	return (has_score(best) ? 0 : -ERANGE);
}
