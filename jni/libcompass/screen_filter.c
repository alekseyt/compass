#include <math.h>

#include "screen_filter.h"

void cmps_screen_filter_init(cmps_screen_filter_t *filter, float threshold) {
	filter->threshold = threshold;
	filter->current = 0;

	pthread_mutex_init(&(filter->lock), 0);
}

void cmps_screen_filter_release(cmps_screen_filter_t *filter) {
	pthread_mutex_destroy(&(filter->lock));
}

void cmps_screen_filter_reset(cmps_screen_filter_t *filter) {
	filter->current = 0;
}

float cmps_screen_filter_get_current(cmps_screen_filter_t *filter) {
	return filter->current;
}

float cmps_screen_filter_pass(cmps_screen_filter_t *filter, float current) {
	float cw_difference = fabs(current - filter->current);
	float ccw_difference = 360 - fabs(current - filter->current);

	if (cw_difference < filter->threshold || ccw_difference < filter->threshold) {
		return filter->current;
	}

	filter->current = current;
	return filter->current;
}
