#include "location.h"

void cmps_location_init(cmps_location_t *location) {
	pthread_mutex_init(&(location->lock), 0);
}

void cmps_location_release(cmps_location_t *location) {
	pthread_mutex_destroy(&(location->lock));
}

void cmps_location_set_speed(cmps_location_t *location, float speed) {
	location->speed = speed;
	location->has_speed = 1;
}

void cmps_location_set_bearing(cmps_location_t *location, float bearing) {
	location->bearing = bearing;
	location->has_bearing = 1;
}
