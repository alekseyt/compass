#include "config.h"
#include "defines.h"
#include "distress_detector.h"

/* forward declarations */

void cmps_distress_reset(cmps_distress_detector_t *detector);

static int update_alert_level_ttl(cmps_distress_detector_t *detector, time_t now) {
	if (detector->alert_timestamp != (time_t)(-1)
	&& now - detector->alert_timestamp > DISTRESS_ALERT_LEVEL_TTL) {
		cmps_distress_reset(detector);
		return 0;
	}

	if (detector->armed_timestamp != (time_t)(-1)
	&& now - detector->armed_timestamp > DISTRESS_ARMED_TTL) {
		cmps_distress_reset(detector);
		return 0;
	}

	return 1;
}

void cmps_distress_init(cmps_distress_detector_t *detector) {
	cmps_distress_reset(detector);
	pthread_mutex_init(&(detector->lock), 0);
}

void cmps_distress_release(cmps_distress_detector_t *detector) {
	pthread_mutex_destroy(&(detector->lock));
}

void cmps_distress_reset(cmps_distress_detector_t *detector) {
	detector->alert_level = 0;
	detector->force = 0;
	detector->alert_timestamp = (time_t)(-1);
	detector->armed_timestamp = (time_t)(-1);
}

void cmps_distress_update_acceleration(cmps_distress_detector_t *detector, float x, float y, float z, time_t now) {
	if (update_alert_level_ttl(detector, now) == 0 
	|| cmps_distress_armed(detector, now) == 0) {
		return;
	}

	/* calc squares but don't take square root */
	detector->force = (x * x + y * y + z * z);
}

void cmps_distress_update_alert_button(cmps_distress_detector_t *detector, int on, time_t now) {
	update_alert_level_ttl(detector, now);
	/* fall-through */

	detector->alert_level = MIN(detector->alert_level + 1, DISTRESS_ALERT_LEVEL_ARMED);
	detector->alert_timestamp = now;

	if (detector->alert_level >= DISTRESS_ALERT_LEVEL_ARMED) {
		detector->armed_timestamp = now;
	}
}

int cmps_distress_armed(cmps_distress_detector_t *detector, time_t now) {
	if (detector->alert_level < DISTRESS_ALERT_LEVEL_ARMED) {
		return 0;
	}

	return ((now - detector->armed_timestamp < DISTRESS_ARMED_TTL) ? 1 : 0);
}

int cmps_distress_pushed(cmps_distress_detector_t *detector, time_t now) {
	return ((detector->force > DISTRESS_FORCE_THRESHOLD) ? 1 : 0);
}

int cmps_distress_panic(cmps_distress_detector_t *detector, time_t now) {
	return ((cmps_distress_armed(detector, now) != 0
	&& cmps_distress_pushed(detector, now) != 0) ? 1 : 0);
}
