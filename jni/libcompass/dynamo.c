#include <errno.h>
#include <string.h>

#include "defines.h"
#include "dynamo.h"

void cmps_dynamo_init(cmps_dynamo_t *dynamo) {
	dynamo->calls_counter = 0;

	pthread_mutex_init(&(dynamo->lock), 0);
}

void cmps_dynamo_release(cmps_dynamo_t *dynamo) {
	pthread_mutex_destroy(&(dynamo->lock));
}

void cmps_dynamo_reset(cmps_dynamo_t *dynamo) {
	dynamo->calls_counter = 0;
}

void cmps_dynamo_next(cmps_dynamo_t *dynamo) {
	dynamo->calls_counter += 1;
}

int cmps_dynamo_get_number(cmps_dynamo_t *dynamo, const char *preset_number, cmps_callee_lookup_t *lookup, char best_number[MAX_NUMBER_LENGTH + 1]) {
	const char *best = 0;

	size_t i = 0; for (; i < 2; ++i) {
		/* select preset number first (if any) */
		if (preset_number != 0 && dynamo->calls_counter == 0) {
			best = preset_number;
			break;
		}
		/* if preset number is already processed or not set
		 * continue with best numbers from phone book */
		else {
			/* check if preset number is already used
			 * if so, calls_counter will be set to 1 already and
			 * bestness index is need to fixed by -1 */
			int index_fix = ((preset_number != 0) ? -1 : 0);

			cmps_callee_lookup_rec_t callee = { { 0 } };
			int lookup_ret = -EINVAL;
			if (lookup != 0) {
				lookup_ret = cmps_callee_lookup_best(lookup, 
					dynamo->calls_counter + index_fix, &callee);	
			}

			/* if candidate is found - return it
			 * (and skip next iteration) */
			if (lookup_ret == 0) {
				best = callee.number;
				break;
			}

			/* if there are no (more) candidates - try to reset
			 * (and start from the beginning of the list again) */
			if (best == 0) {
				cmps_dynamo_reset(dynamo);
				continue;
			}
		}
	}

	if (best != 0) {
		memcpy(best_number, best, MIN(MAX_NUMBER_LENGTH + 1, strlen(best) + 1));
	}

	return (best != 0 ? 0 : -ERANGE);
}
