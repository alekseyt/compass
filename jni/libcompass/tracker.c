#include "config.h"
#include "defines.h"
#include "low_pass_filter.h"
#include "tracker.h"
#include "utils.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

/* forward declarations
*/

/** Update speed value
 * @ingroup tracker
 *
 * @param speed speed reading
 * @param now current time in unixtime
 */
static void cmps_tracker_update_speed(cmps_tracker_t *tracker, float speed, long now);

/**
 * @ingroup tracker
 * @param azimuth angle between north and target (phone head)
 * @param now current time in unixtime
 */
static void cmps_tracker_update_location_heading(cmps_tracker_t *tracker, float azimuth, long now);

void cmps_tracker_init(cmps_tracker_t *tracker) {
	tracker->last_location = 0;

	tracker->has_magnetic_heading = 0;
	tracker->magnetic_azimuth = 0;

	tracker->has_location_heading = 0;
	tracker->location_azimuth = 0;

	tracker->has_speed = 0;
	tracker->speed = 0;

	pthread_mutex_init(&(tracker->lock), 0);
}

void cmps_tracker_release(cmps_tracker_t *tracker) {
	if (tracker->last_location != 0) {
		free(tracker->last_location);
		tracker->last_location = 0;
	}

	pthread_mutex_destroy(&(tracker->lock));
}

int cmps_tracker_get_location(cmps_tracker_t *tracker, cmps_location_t *location) {
	if (tracker->last_location != 0) {
		*location = *(tracker->last_location);
		return 1;
	}

	return 0;
}

void cmps_tracker_update_location(cmps_tracker_t *tracker, const cmps_location_t *location, long now) {
	if (location == 0 && tracker->last_location != 0) {
		free(tracker->last_location);
		tracker->last_location = 0;
	}

	if (location != 0 && tracker->last_location == 0) {
		tracker->last_location = malloc(sizeof(*tracker->last_location));
		memset(tracker->last_location, 0, sizeof(*(tracker->last_location)));
	}

	if (tracker->last_location != 0 && location != 0) {
		*(tracker->last_location) = *location;
	}

	tracker->has_speed = (location == 0 ? 0 : location->has_speed);
	if (tracker->has_speed) {
		cmps_tracker_update_speed(tracker, location->speed, now);
	}

	int has_location_heading = (location == 0 ? 0 : location->has_bearing);
	if (has_location_heading != 0) {
		tracker->has_location_heading = 1;
		cmps_tracker_update_location_heading(tracker, location->bearing, now);
	}
}

int cmps_tracker_has_location_heading(cmps_tracker_t *tracker) {
	return tracker->has_location_heading;
}

float cmps_tracker_get_location_heading(cmps_tracker_t *tracker) {
	return (float)(int)tracker->location_azimuth;
}

static void cmps_tracker_update_location_heading(cmps_tracker_t *tracker, float azimuth, long now) {
	float fixed_azimuth = cmps_normalize_azimuth(azimuth);
	tracker->location_azimuth =
		cmps_low_pass_filter(tracker->location_azimuth, fixed_azimuth, TRACKER_ANGLE_ALPHA);
}

int cmps_tracker_has_magnetic_heading(cmps_tracker_t *tracker) {
	return tracker->has_magnetic_heading;
}

float cmps_tracker_get_magnetic_heading(cmps_tracker_t *tracker) {
	return (float)(int)tracker->magnetic_azimuth;
}

void cmps_tracker_update_magnetic_heading(cmps_tracker_t *tracker, float azimuth) {
	float fixed_azimuth = cmps_normalize_azimuth(azimuth);
	tracker->magnetic_azimuth =
		cmps_low_pass_filter(tracker->magnetic_azimuth, fixed_azimuth, TRACKER_ANGLE_ALPHA);

	tracker->has_magnetic_heading = 1;
}

int cmps_tracker_has_speed(cmps_tracker_t *tracker) {
	return tracker->has_speed;
}

float cmps_tracker_get_speed(cmps_tracker_t *tracker) {
	return tracker->speed;
}

static void cmps_tracker_update_speed(cmps_tracker_t *tracker, float speed, long now) {
	tracker->speed = cmps_low_pass_filter(tracker->speed, speed, TRACKER_SPEED_ALPHA);
}

float cmps_tracker_get_direction(cmps_tracker_t *tracker, float heading, const cmps_location_t *target) {
	cmps_location_t location = { 0 };
	int has_location = cmps_tracker_get_location(tracker, &location);
	if (has_location == 0 || target == 0) {
		return 0;
	}

	float target_direction = cmps_normalize_azimuth(cmps_direction(&location, target));
	return (float)(int)cmps_normalize_azimuth(target_direction - heading);
}

float cmps_tracker_get_distance(cmps_tracker_t *tracker, const cmps_location_t *target) {
	cmps_location_t location = { 0 };
	int has_location = cmps_tracker_get_location(tracker, &location);

	if (has_location == 0 || target == 0) {
		return 0;
	}

	return cmps_distance(&location, target);
}
