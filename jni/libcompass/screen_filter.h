#ifndef CMPS_SCREEN_FILTER_H
#define CMPS_SCREEN_FILTER_H

#include <pthread.h>
 
#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** Circular-range screening filter
 * @ingroup filters
 */
typedef struct {
	float threshold;
	float current;

	pthread_mutex_t lock;
} cmps_screen_filter_t;

/**
 * @ingroup filters
 */
void cmps_screen_filter_init(cmps_screen_filter_t *filter, float threshold);

/**
 * @ingroup filters
 */
void cmps_screen_filter_release(cmps_screen_filter_t *filter);

/** Reset filter
 * @ingroup filters
 */
void cmps_screen_filter_reset(cmps_screen_filter_t *filter);

/** Get current filter value
 * @ingroup filters
 */
float cmps_screen_filter_get_current(cmps_screen_filter_t *filter);

/**
 * This filter is supposed to screen changes below threshold value
 * note that it operates on circular range of 0..359
 * therefore distance between 0 and 359 is  equal to 1
 * @ingroup filters
 *
 *
 * @param current current value
 * @return previous value if change is below the threshold
 */
float cmps_screen_filter_pass(cmps_screen_filter_t *filter, float current);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* CMPS_SCREEN_FILTER_H */
