#include <stdlib.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1alloc(JNIEnv *env, jclass cls) {
	cmps_callee_lookup_t *lookup = malloc(sizeof(*lookup));
	return (jlong)(long)(lookup);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1free(JNIEnv *env, jclass cls, jlong c_lookup) {
	free(lookupp(c_lookup));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1init(JNIEnv *env, jclass cls, jlong c_lookup) {
	cmps_callee_lookup_init(lookupp(c_lookup));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1release(JNIEnv *env, jclass cls, jlong c_lookup) {
	cmps_callee_lookup_release(lookupp(c_lookup));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1callee_1lookup_1reset(JNIEnv *env, jclass cls, jlong c_lookup) {
	cmpss_callee_lookup_reset(lookupp(c_lookup));
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1callee_1lookup_1push_1call(JNIEnv *env, jclass cls, jlong c_lookup, jstring number, jstring name, jlong timestamp, jlong duration) {
	const char *c_number = (number == 0 ? 0 : (*env)->GetStringUTFChars(env, number, 0));
	const char *c_name = (name == 0 ? 0 : (*env)->GetStringUTFChars(env, name, 0));
	int ret = cmpss_callee_lookup_push_call(lookupp(c_lookup), c_number, c_name, timestamp, duration);

	if (c_name != 0) {
		(*env)->ReleaseStringUTFChars(env, name, c_name);
	}

	if (c_number != 0) {
		(*env)->ReleaseStringUTFChars(env, number, c_number);
	}

	return ret;
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1callee_1lookup_1best(JNIEnv *env, jclass cls, jlong c_lookup, jint bestness) {
	cmps_callee_lookup_rec_t *callee = malloc(sizeof(*callee));
	int ret = cmpss_callee_lookup_best(lookupp(c_lookup), bestness, callee);

	if (ret != 0) {
		free(callee);
		callee = 0;
	}

	return (jlong)(long)(callee);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1callee_1free(JNIEnv *env, jclass cls, jlong c_callee) {
	free(calleep(c_callee));
}

jstring JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1callee_1lookup_1callee_1get_1number(JNIEnv *env, jclass cls, jlong c_callee) {
	const cmps_callee_lookup_rec_t *callee = calleep(c_callee);

	return (*env)->NewStringUTF(env, callee->number);
}
