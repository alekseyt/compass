#ifndef CMPS_JNI_UTILS_H
#define CMPS_JNI_UTILS_H

#include <jni.h>
#include <libcompass/libcompass.h>

typedef struct {
	char *title;
	float lat;
	float lon;
} cmps_jni_bookmark_t;

static inline cmps_location_t* locationp(jlong location) {
	return (cmps_location_t *)(long)(location);
}

static inline cmps_tracker_t* trackerp(jlong tracker) {
	return (cmps_tracker_t *)(long)(tracker);
}

static inline cmps_screen_filter_t* screenfilterp(jlong filter) {
	return (cmps_screen_filter_t *)(long)(filter);
}

static inline cmps_router_t* routerp(jlong router) {
	return (cmps_router_t *)(long)(router);
}

static inline cmps_router_state_t* statep(jlong state) {
	return (cmps_router_state_t *)(long)(state);
}

static inline cmps_jni_bookmark_t* bookmarkp(jlong bookmark) {
	return (cmps_jni_bookmark_t *)(long)(bookmark);
}

static inline cmps_distress_detector_t* detectorp(jlong detector) {
	return (cmps_distress_detector_t *)(long)(detector);
}

static inline cmps_callee_lookup_t* lookupp(jlong lookup) {
	return (cmps_callee_lookup_t *)(long)(lookup);
}

static inline cmps_callee_lookup_rec_t* calleep(jlong callee) {
	return (cmps_callee_lookup_rec_t *)(long)(callee);
}

static inline cmps_dynamo_t* dynamop(jlong dynamo) {
	return (cmps_dynamo_t *)(long)(dynamo);
}

#endif /* CMPS_JNI_UTILS_H */
