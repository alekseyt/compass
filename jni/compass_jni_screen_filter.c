#include <stdlib.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1screen_1filter_1alloc(JNIEnv *env, jclass cls) {
	cmps_screen_filter_t *filter = malloc(sizeof(*filter));
	return (jlong)(long)(filter);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1screen_1filter_1free(JNIEnv *env, jclass cls, jlong c_filter) {
	free(screenfilterp(c_filter));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1screen_1filter_1init(JNIEnv *env, jclass cls, jlong c_filter, jfloat threshold) {
	cmps_screen_filter_init(screenfilterp(c_filter), threshold);

}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1screen_1filter_1release(JNIEnv *env, jclass cls, jlong c_filter) {
	cmps_screen_filter_release(screenfilterp(c_filter));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1screen_1filter_1reset(JNIEnv *env, jclass cls, jlong c_filter) {
	cmpss_screen_filter_reset(screenfilterp(c_filter));
}

float JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1screen_1filter_1get_1current(JNIEnv *env, jclass cls, jlong c_filter) {
	return cmpss_screen_filter_get_current(screenfilterp(c_filter));
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1screen_1filter_1pass(JNIEnv *env, jclass cls, jlong c_filter, jfloat current) {
	return cmpss_screen_filter_pass(screenfilterp(c_filter), current);
}
