#include <stdlib.h>
#include <string.h>

#include "com_sgr_b2_compass_jni_CCJNI.h"
#include "jni_utils.h"

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1alloc(JNIEnv *env, jclass cls) {
	cmps_router_t *router = malloc(sizeof(*router));
	memset(router, 0, sizeof(*router));
	return (jlong)(long)(router);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1free(JNIEnv *env, jclass cls, jlong c_router) {
	free(routerp(c_router));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1init(JNIEnv *env, jclass cls, jlong c_router) {
	cmps_router_init(routerp(c_router));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1release(JNIEnv *env, jclass cls, jlong c_router) {
	cmps_router_release(routerp(c_router));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1router_1set_1target(JNIEnv *env, jclass cls, jlong c_router, jlong c_target_location) {
	cmpss_router_set_target(routerp(c_router), locationp(c_target_location));
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1router_1update_1location(JNIEnv *env, jclass cls, jlong c_router, jlong c_location, jlong now) {
	cmpss_router_update_location(routerp(c_router), locationp(c_location), now);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmpss_1router_1update_1mag_1heading(JNIEnv *env, jclass cls, jlong c_router, jfloat azimuth) {
	cmpss_router_update_mag_heading(routerp(c_router), azimuth);
}

jlong JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1get_1state(JNIEnv *env, jclass cls, jlong c_router, jlong now) {
	cmps_router_state_t *state = malloc(sizeof(*state));
	memset(state, 0, sizeof(*state));
	cmpss_router_get_state(routerp(c_router), state, now);
	return (jlong)(long)(state);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1free(JNIEnv *env, jclass cls, jlong c_state) {
	free(statep(c_state));
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1has_1location(JNIEnv *env, jclass cls, jlong c_state) {
	return (statep(c_state)->has_location);
}

void JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1get_1location(JNIEnv *env, jclass cls, jlong c_state, jlong c_location) {
	*locationp(c_location) = statep(c_state)->location;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1get_1azimuth(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->azimuth;
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1direction_1is_1magnetic(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->direction_is_magnetic;
}

jint JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1direction_1from_1gps(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->direction_from_gps;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1get_1direction(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->direction;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1get_1distance(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->distance;
}

jfloat JNICALL Java_com_sgr_1b2_compass_jni_CCJNI_cmps_1router_1state_1get_1speed(JNIEnv *env, jclass cls, jlong c_state) {
	return statep(c_state)->speed;
}
