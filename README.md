[TOC]

## WHATS THIS

This is open-source outdoors application which can help you to find
your way though the forest or desert, or whatever. It is providing
inter-device location exchange by SMS and can make distress call even
if device screen is broken by sending SMS with your location on your
behalf.

Due to pointer-navigation nature, it can also be used as either
magnetic or GPS compass reading direction to north from your device
magnetic sensors or from GPS chip.

## WHY ITS GOOD

* Suitable for use on motorbike or in a car
* You don't need Internet connection to exchange locations - any mobile 
  network will do
* Small size - you can download it on any connection

## BASIC USAGE

* Install from [F-Droid][]
* Or install from [Google Play][]
* Or take APK from [Downloads][]
* Or build it yourself: see [BUILD][]

[F-Droid]: https://f-droid.org/repository/browse/?fdid=com.sgr_b2.compass
[Google Play]: https://play.google.com/store/apps/details?id=com.sgr_b2.compass
[Downloads]: https://bitbucket.org/alekseyt/compass/downloads
[BUILD]: https://bitbucket.org/alekseyt/compass/src/master/BUILD

### managing bookmarks

Open menu -> Bookmarks

Here you can add bookmark (from current location, if available, or
arbitrary location), edit or delete bookmarks. You can also manually
import bookmarks from SMS messages in your inbox, or to send location
(by SMS, your default SMS application will appear to select recipient).

Note that if application is running and someone would send you SMS
with location, CC will automatically open import activity for you
to choose to import this bookmark or not.

### distress call

This is software implementation of hardware SOS button on your phone. If
you press power button several time, when shake your phone, app will
acquire location from GPS and send it to preset distress call number,
if any, or will select best candidate from your phone book and will use
it as a distress call number.

If you perform those actions several times, app will use different
numbers from your phone book, from the best suitable to worst suitable.

See [Wiki][] for more details.

Compass will switch to distress call mode in which navigation isn't
available, follow on-screen instructions to cancel distress call and
return to navigation mode.

## SEE ALSO

* [Wiki][]
* [Issues][]

[Wiki]: https://bitbucket.org/alekseyt/compass/wiki/Home
[Issues]: https://bitbucket.org/alekseyt/compass/issues?status=new&status=open

## KNOWN ISSUES

* Coordinates in SMS message and in URL embedded into SMS message
  might be different in 6-th decimal digit. This is due to different
  rounding methods used for coords fomatting. However error in 6-th
  decimal digit is on scale of 10 centimeters, hence error of 0.000005
  is 50 centimeters mistake which is not significant for positioning
  in Community compass.

## QUESTIONS?

[aleksey.tulinov@gmail.com][]

[aleksey.tulinov@gmail.com]: mailto:aleksey.tulinov@gmail.com
