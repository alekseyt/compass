1.4.4 1.4.5 enhancements

[+] added distress call service icon to the system tray (no option to hide
    it, sorry, this is the only way to prevent memory cleaner applications
    to kill this service. it's a neat icon though)

[*] fixed URL to maps.google.com to work better in some SMS readers
[*] some modifications to prevent distress call service being killed by
    memory cleaning apps
[*] hopefully faster GPS fix
[*] version bumped to 1.4.5 because of G. Play issues

1.4.3 bugfix release

[*] fixed bug when distance wasn't shown on screen right after acquiring
    GPS location
[*] fixed bug when location in SMS message couldn't be imported
    if preceded by line-break
[*] removed GPS location updates threshold (5 location updates from GPS
    before location accepted by compass) because it was confusing people

1.4.2 enhancements release

[+] added option to show speed with distance

[*] added coordinates format description (eng) into distress message
[*] moved "show location alert" under "display options" in settings
[*] fixed HRT marshalling implementation to be in accordance to doc
[*] minor under-the-hood changes

1.4.1 more alien urls

[+] compass is now able to import locations from supported URLs
    to online maps

[*] under-the-hood changes to gracefully handle devices
    without SMS messaging capabilities
[*] minor changes to UI for consistent app behavior along all
    activities

1.4 alien urls release

[+] introduced option to embed URL to online maps into SMS messages
    sent by compass (/issue/2/additionally-format-sms-location-as-uri)

[*] fixed bug when "cancel distress call" button might not appear on screen
[*] fixed bug when caption wasn't visible on "cancel distress call" button
[*] fixed bug when compass arrow might got stuck on target location set

1.3.5 bugfixing release

[*] Android 2.0 or later is now required to run CC
[*] fixed issue when only local contacts were available for distress call
    (/issue/1/can-only-choose-local-contacts)
[*] reduced battery usage
[*] alerts (close to magnet, etc) won't longer blink

1.3.4 bugfixing release

[+] unicode SMS messages support

[*] fixed tab order in add/edit bookmark mode
[*] removed debug info
[*] under-the-hood changes

[-] fixed bug when application might crash if distress call is being canceled

1.3.3 enhancements release

[+] removed backup/restore functions
[+] improved distress number selection

[-] fixed conflicts resolution during bookmarks editing
[-] fixed bug when compass might ignore bookmark title when importing
    from SMS

[*] more descriptive settings UX
[*] under-the-hood changes

1.3.2 portable implementation release

[+] cancelable distress call if UI available
[+] confirmation for bookmark deletion

[-] fixed "use preset number only" UI

[*] minor changes in distress number selection
[*] (under the hood) migration to portable C-implementation

1.3.1 enhancement release

[*] introduced "use only preset number" option in distress call
    settings

1.3 distress call release

[+] distress call support with and without preset number
[+] backup/restore of bookmarks on external drive
[+] "don't show again" in all messages and messages visibility settings

[*] fixed issue with editing bookmark title
[*] "waiting for location" alert is displayed by default

1.2.2 enhancements release

[*] fixed graphics for xlarge screens
[*] meters/feet distance won't show decimal part anymore

1.2.1 enhancements release

[*] changed resources a little bit because people are complaining
    that they can't figure it out

1.2 enhancements release

[+] phone direction arrow in GPS mode
[+] indication of compass mode in app title

[*] under the hood changes for GPS heading processing
    (hopefully more reliable GPS compass)
[*] fully editable bookmarks
[*] minor fixes for alerts

1.1.2 bugfixing release

[-] fixed unavailable magnetic sensors alert

[*] main screen won't longer react on long touch

1.1.1 bugfixing release

[+] introduced magnetic alerts blinking on the screen
[+] introduced (optional) waiting for location alert

[-] fixed issue with compass arrow jumping around 0

[*] reduce expected magnetic field range

1.1 gps compass release

[+] GPS compass
[+] weak/strong magnetic field detection

1.0.2 bugfixing release

[-] fixed bookmarks editing
[-] temporary fix for devices without magnetic sensor

1.0.1 bugfixing release

[-] fixed xhdpi screens support
[-] added title to main activity to enable menu on android 4
[-] fixed handling of negative coordinates
[-] fixed bug when you could add several bookmarks with the same name

[*] some under the hood changes
[*] reduced resources disk usage

1.0 initial release

[+] magnetic compass
[+] basic gps direction tracking
[+] metric/imperial units
[+] bookmarks
[+] sms bookmarks import
[+] themes
[+] known issues
