package com.sgr_b2.compass.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "pois.db";
	private static final int DB_VERSION = 6;

	public static final String TABLE_POIS = "pois";
	public static final String TABLE_LOG = "log";

	private static final String POIS_CREATE = "CREATE TABLE " + TABLE_POIS + " (lat REAL NOT NULL, lon REAL NOT NULL, title)";
	private static final String LOGS_CREATE = "CREATE TABLE " + TABLE_LOG + " (key NOT NULL UNIQUE, value)";

	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(POIS_CREATE);
		database.execSQL(LOGS_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldv, int newv) {
		database.execSQL("DROP TABLE " + TABLE_LOG);
		database.execSQL(LOGS_CREATE);
	}
}
