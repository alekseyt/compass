package com.sgr_b2.compass.db;

import java.io.Serializable;

import com.sgr_b2.compass.jni.Bookmark;

import android.location.Location;


public class POI implements Serializable {

	private static final long serialVersionUID = -7560795199573367237L;

	public int id;
	public String title;
	public float lat;
	public float lon;

	public POI(float lat, float lon, String title) {
		this.id = -1;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}

	public POI(float lat, float lon, String title, int id) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}

	public POI(Location location, String title) {
		this.id = -1;
		this.lat = (float)location.getLatitude();
		this.lon = (float)location.getLongitude();
		this.title = title;
	}

	public static POI fromJNIBookmark(final Bookmark bookmark) {
		if (bookmark == null) {
			return null;
		}

		return new POI(bookmark.getLatitude(), bookmark.getLongitude(), bookmark.getTitle());
	}

	public Location getLocation() {
		Location loc = new Location("noone");
		loc.setLatitude(this.lat);
		loc.setLongitude(this.lon);
		return loc;
	}
}
