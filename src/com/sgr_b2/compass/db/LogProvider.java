package com.sgr_b2.compass.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class LogProvider {
	private DBHelper helper = null;
	private SQLiteDatabase db = null;

	private final String SMSID_KEY = "smsid";

	public LogProvider(Context context) {
		this.helper = new DBHelper(context);
		this.db = this.helper.getWritableDatabase();
	}

	public void dispose() {
		this.db.close();
	}

	private String getValue(final String key) {
		Cursor cursor = this.db.query(DBHelper.TABLE_LOG,
			new String[] { "value" },
			"key='" + key + "'",
			null, null, null, null);

		try {
			if (cursor.getCount() < 1) {
				return null;
			}

			cursor.moveToFirst();
			String id = cursor.getString(0);
			return id;
		}
		finally {
			cursor.close();
		}
	}

	public synchronized String getLastSMSId() {
		return getValue(SMSID_KEY);
	}

	private void insertOrReplace(final String key, final String value) {
		if (value == null) {
			return;
		}

		ContentValues values = new ContentValues();
		values.put("key", key);
		values.put("value", value);

		if (getValue(key) == null) {
			this.db.insert(DBHelper.TABLE_LOG, null, values);
		}
		else{
			this.db.update(DBHelper.TABLE_LOG, values, "key='" + key + "'", null);
		}
	}

	public synchronized void setLastSMSId(String id) {
		insertOrReplace(SMSID_KEY, id);
	}
}
