package com.sgr_b2.compass.themes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

import com.sgr_b2.compass.R;


public class VanillaTheme extends Theme {

	Context context;

	public VanillaTheme(Context context) {
		this.context = context;
	}

	@Override
	public BitmapDrawable getGPSCompassBG(int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(this.context.getResources(),
			R.drawable.compass_bg_gps);

		return GraphicUtils.scaleBitmap(this.context.getResources(),
			bmp, width, height);
	}

	@Override
	public BitmapDrawable getGPSCompassArrow(int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(this.context.getResources(),
			R.drawable.compass_arrow);

		return GraphicUtils.scaleBitmap(this.context.getResources(),
			bmp, width, height);
	}

	@Override
	public BitmapDrawable getMagneticCompassBG(int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(this.context.getResources(),
			R.drawable.compass_bg);

		return GraphicUtils.scaleBitmap(this.context.getResources(),
			bmp, width, height);
	}

	@Override
	public BitmapDrawable getMagneticCompassArrow(int width, int height) {
		return getGPSCompassArrow(width, height);
	}
}
