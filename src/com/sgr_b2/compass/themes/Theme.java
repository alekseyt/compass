package com.sgr_b2.compass.themes;

import android.graphics.drawable.BitmapDrawable;


public abstract class Theme {

	public abstract BitmapDrawable getGPSCompassBG(int width, int height);
	public abstract BitmapDrawable getMagneticCompassBG(int width, int height);
	public abstract BitmapDrawable getMagneticCompassArrow(int width, int height);
	public abstract BitmapDrawable getGPSCompassArrow(int width, int height);
}
