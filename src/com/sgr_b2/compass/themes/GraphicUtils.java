package com.sgr_b2.compass.themes;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


public class GraphicUtils {

	public static BitmapDrawable scaleBitmap(Resources res, Bitmap source, int width, int height) {
		if (source == null) {
			return null;
		}

		int size = Math.min(width, height);

		Bitmap bitmap = Bitmap.createScaledBitmap(source, size, size, false);
		BitmapDrawable drawable = new BitmapDrawable(res, bitmap);

		return drawable;
	}
}
