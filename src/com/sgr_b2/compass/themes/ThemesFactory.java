package com.sgr_b2.compass.themes;

import android.content.Context;


public class ThemesFactory {

	public final static String VANILLA = "vanilla";
	public final static String MINIMAL = "minimal";

	public static Theme getTheme(Context context, String name) {
		if (name.equals(MINIMAL)) {
			return new MinimalTheme(context);
		}

		return new VanillaTheme(context);
	}
}
