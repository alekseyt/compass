package com.sgr_b2.compass.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.activities.MainActivity;


/**
 * listener for "don't show again" alerts
 *
 * will set preferences according to checkbox in alert
 *
 * @note it will invert selection in preferences
 * i.e. if "don't show again" is checked, it will write false into preference
 * that means if "don't show" is enabled, target option is supposed to be disabled
 */
public class DontShowAgainOKListener implements OnClickListener {

	private MainActivity context = null;
	private View alert_view = null;
	private String settings_key = null;

	public DontShowAgainOKListener(MainActivity context, final View alert_view, final String settings_key) {
		assert(alert_view != null);
		assert(settings_key != null);

		this.context = context;
		this.alert_view = alert_view;
		this.settings_key = settings_key;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this.context);

		CheckBox checkbox = (CheckBox)alert_view.findViewById(R.id.dont_show);
		assert(checkbox != null);

		boolean dont_show = checkbox.isChecked();
		SharedPreferences.Editor editor = shared_pref.edit();
		editor.putBoolean(this.settings_key, !dont_show); // *** INVERT SELECTION ***
		editor.commit();

		this.context.onOK(dialog);
	}
}
