package com.sgr_b2.compass.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sgr_b2.compass.R;


public class ImportView extends RelativeLayout implements Checkable {

	TextView text1 = null;
	TextView text2 = null;
	CheckBox checkbox = null;

	public ImportView(Context context, AttributeSet attrs) {
		super(context, attrs, 0);
	}

	@Override
	protected void onFinishInflate() {
        	super.onFinishInflate();

	        this.text1 = (TextView)findViewById(R.id.text1);
        	this.text2 = (TextView)findViewById(R.id.text2);
	        this.checkbox = (CheckBox)findViewById(R.id.checkbox);

        	this.checkbox.setClickable(false);
	        this.checkbox.setFocusable(false);
        	this.checkbox.setFocusableInTouchMode(false);
    	}

	public CheckBox getCheckBox() {
		return this.checkbox;
	}

	public TextView getText1() {
		return this.text1;
	}

	public TextView getText2() {
		return this.text2;
	}

	@Override
	public boolean isChecked() {
		return this.checkbox.isChecked();
	}

	@Override
	public void setChecked(boolean checked) {
		this.checkbox.setChecked(checked);
	}

	@Override
	public void toggle() {
		this.checkbox.toggle();
	}

	@Override
	public void setEnabled (boolean enabled) {
		super.setEnabled(enabled);

		for (int i = 0; i < getChildCount();  ++i) {
			getChildAt(i).setEnabled(enabled);
		}

		if (!enabled) {
			setFocusable(false);
			setFocusableInTouchMode(false);
		}
	}

}
