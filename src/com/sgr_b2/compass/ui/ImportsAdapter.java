package com.sgr_b2.compass.ui;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.db.BookmarksProvider;
import com.sgr_b2.compass.phone.SMSPOI;
import com.sgr_b2.compass.phone.SMSReader;


public class ImportsAdapter extends BaseAdapter {

	private Context context = null;
	private List<SMSPOI> candidates = null;
	private BookmarksProvider pois_provider = null;

	public ImportsAdapter(Context context, BookmarksProvider pois_provider) {
		this.context = context;
		this.pois_provider = pois_provider;

		SMSReader sms_reader = new SMSReader(this.context);

		this.candidates = new LinkedList<SMSPOI>();

		String sms_id = null;
		do {
			SMSPOI poi = sms_reader.findLocation(sms_id, SMSReader.HISTORY_INFINITE);
			sms_id = (poi != null ? poi.sms_id : null);

			if (poi != null) {
				this.candidates.add(0, poi);
			}
		} while (sms_id != null);
	}

	@Override
	public int getCount() {
		return this.candidates.size();
	}

	@Override
	public Object getItem(int i) {
		return this.candidates.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	public static String getSMSPOITitle(final SMSPOI poi) {
		return (poi.title != null ? poi.title : poi.contact);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImportView layout = null;

		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(this.context);
			layout = (ImportView)inflater.inflate(R.layout.layout_import_item, parent, false);
		}
		else {
			layout = (ImportView)convertView;
		}

		TextView poi_title = layout.getText1();
		TextView poi_subtext = layout.getText2();
		CheckBox checkbox = layout.getCheckBox();

		SMSPOI poi = (SMSPOI)getItem(position);
		String title = ImportsAdapter.getSMSPOITitle(poi);

		poi_title.setText(title);
		poi_subtext.setText(UnitsFormatting.formatLocation(this.context, poi.lat, poi.lon));

		POI existing = this.pois_provider.hasPOI(title);
		boolean update_required = (existing == null
			|| (existing.lat != poi.lat || existing.lon != poi.lon));

		checkbox.setChecked(!update_required);
		layout.setEnabled(update_required);
		layout.setTag(poi);

		return layout;
	}

}
