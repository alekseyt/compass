package com.sgr_b2.compass.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgr_b2.compass.db.BookmarksProvider;
import com.sgr_b2.compass.db.POI;


public class BookmarksAdapter extends BaseAdapter {

	private Context context = null;
	private BookmarksProvider provider = null;
	private int count = 0;

	public BookmarksAdapter(Context context, BookmarksProvider provider) {
		this.context = context;
		this.provider = provider;
		this.count = (int)this.provider.getCount();
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public Object getItem(int position) {
		return this.provider.getPOI(count - position - 1);
	}

	@Override
	public long getItemId(int position) {
		return count - position - 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View layout = null;

		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(this.context);
			layout = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
		}
		else {
			layout = convertView;
		}

		POI poi = (POI)getItem(position);

		TextView poi_title = (TextView)layout.findViewById(android.R.id.text1);
		TextView poi_coords = (TextView)layout.findViewById(android.R.id.text2);

		poi_title.setText(poi.title);
		poi_coords.setText(UnitsFormatting.formatLocation(this.context, poi.lat, poi.lon));

		layout.setTag(poi);
		return layout;
	}

	public void update() {
		this.count = (int)this.provider.getCount();

		notifyDataSetChanged();
	}
}
