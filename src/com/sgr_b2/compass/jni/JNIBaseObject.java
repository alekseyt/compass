package com.sgr_b2.compass.jni;


public abstract class JNIBaseObject {
	protected long c_ptr = 0;

	public long getCPtr() {
		return this.c_ptr;
	}

	public synchronized void dispose() {
		if (this.c_ptr != 0) {
			_dispose();
			this.c_ptr = 0;
		}
	}

	@Override
	protected void finalize() {
		dispose();
	}

	protected abstract void _dispose();
}
