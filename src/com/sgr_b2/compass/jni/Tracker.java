package com.sgr_b2.compass.jni;

import com.sgr_b2.compass.jni.android.Adopt;


public class Tracker extends JNIBaseObject {

	public Tracker() {
		this.c_ptr = CCJNI.cmps_tracker_alloc();
		CCJNI.cmps_tracker_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_tracker_release(this.c_ptr);
		CCJNI.cmps_tracker_free(this.c_ptr);
	}

	public Location getLocation() {
		Location loc = new Location();

		int has_location = CCJNI.cmpss_tracker_get_location(this.c_ptr, loc.getCPtr());

		if (has_location == 0) {
			return null;
		}

		return loc;
	}

	public void updateLocation(android.location.Location target, long now) {
		Location location = Adopt.location(target);
		long c_location = (location == null ? 0 : location.getCPtr());

		CCJNI.cmpss_tracker_update_location(this.c_ptr, c_location, now);

		if (location != null) {
			location.dispose();
		}
	}

	public boolean hasLocationHeading() {
		return (CCJNI.cmpss_tracker_has_location_heading(this.c_ptr) != 0
			? true : false);
	}

	public float getLocationHeading() {
		return CCJNI.cmpss_tracker_get_location_heading(this.c_ptr);
	}

	public boolean hasMagneticHeading() {
		return (CCJNI.cmpss_tracker_has_magnetic_heading(this.c_ptr) != 0
			? true : false);
	}

	public float getMagneticHeading() {
		return CCJNI.cmpss_tracker_get_magnetic_heading(this.c_ptr);
	}

	public void updateMagneticHeading(float azimuth) {
		CCJNI.cmpss_tracker_update_magnetic_heading(this.c_ptr, azimuth);
	}

	public boolean hasSpeed() {
		return (CCJNI.cmpss_tracker_has_speed(this.c_ptr) != 0
			? true : false);
	}

	public float getSpeed() {
		return CCJNI.cmpss_tracker_get_speed(this.c_ptr);
	}

	public float getDistance(android.location.Location target) {
		Location location = Adopt.location(target);
		long c_location = (location == null ? 0 : location.getCPtr());

		float distance = CCJNI.cmpss_tracker_get_distance(c_ptr, c_location);

		if (location != null) {
			location.dispose();
		}

		return distance;
	}

	public float getDirection(float heading, android.location.Location target) {
		Location location = Adopt.location(target);
		long c_location = (location == null ? 0 : location.getCPtr());

		float direction = CCJNI.cmpss_tracker_get_direction(this.c_ptr, heading, c_location);

		if (location != null) {
			location.dispose();
		}

		return direction;
	}
}
