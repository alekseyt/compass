package com.sgr_b2.compass.jni.android;

import com.sgr_b2.compass.jni.Location;


public class Adopt {

	public static Location location(final android.location.Location loc) {
		if (loc == null) {
			return null;
		}

		Location ret = new Location();
		ret.setLatitude((float)loc.getLatitude());
		ret.setLongitude((float)loc.getLongitude());

		if (loc.hasBearing()) {
			ret.setBearing(loc.getBearing());
		}

		if (loc.hasSpeed()) {
			ret.setSpeed(loc.getSpeed());
		}

		return ret;
	}

	public static android.location.Location location(final Location loc) {
		if (loc == null) {
			return null;
		}

		android.location.Location ret = new android.location.Location(Location.PROVIDER);
		ret.setLatitude(loc.getLatitude());
		ret.setLongitude(loc.getLongitude());

		return ret;
	}
}
