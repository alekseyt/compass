package com.sgr_b2.compass.jni;

public class Screen extends JNIBaseObject {

	public Screen(float threshold) {
		this.c_ptr = CCJNI.cmps_screen_filter_alloc();
		CCJNI.cmps_screen_filter_init(this.c_ptr, threshold);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_screen_filter_release(this.c_ptr);
		CCJNI.cmps_screen_filter_free(this.c_ptr);
	}

	public void reset() {
		CCJNI.cmpss_screen_filter_reset(this.c_ptr);
	}

	public float getCurrent() {
		return CCJNI.cmpss_screen_filter_get_current(this.c_ptr);
	}

	public float filter(float current) {
		return CCJNI.cmpss_screen_filter_pass(this.c_ptr, current);
	}
}
