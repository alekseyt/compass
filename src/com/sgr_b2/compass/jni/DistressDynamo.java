package com.sgr_b2.compass.jni;


public class DistressDynamo extends JNIBaseObject {

	public DistressDynamo() {
		this.c_ptr = CCJNI.cmps_dynamo_alloc();
		CCJNI.cmps_dynamo_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_dynamo_release(this.c_ptr);
		CCJNI.cmps_dynamo_free(this.c_ptr);
	}

	public void reset() {
		CCJNI.cmpss_dynamo_reset(this.c_ptr);
	}

	public void next() {
		CCJNI.cmpss_dynamo_next(this.c_ptr);
	}

	public String getNumber(final String preset_number, CalleeLookup lookup) {
		long c_lookup = (lookup == null ? 0 : lookup.getCPtr());
		return CCJNI.cmpss_dynamo_get_number(this.c_ptr, preset_number, c_lookup);
	}
}
