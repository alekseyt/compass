package com.sgr_b2.compass.jni;


public class Bookmark extends JNIBaseObject {

	private String title = null;
	private float lat = 0;
	private float lon = 0;

	private Bookmark(long c_ptr) {
		this.c_ptr = c_ptr;

		readBookmark();
	}

	private void readBookmark() {
		this.title = CCJNI.cmps_hrt_bookmark_get_title(this.c_ptr);
		this.lat = CCJNI.cmps_hrt_bookmark_get_lat(this.c_ptr);
		this.lon = CCJNI.cmps_hrt_bookmark_get_lon(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_hrt_bookmark_free(this.c_ptr);
	}

	public static Bookmark fromString(final String string) {
		if (string == null) {
			return null;
		}

		long bookmark_ptr = CCJNI.cmps_hrt_unmarshall(string);
		if (bookmark_ptr == 0) {
			return null;
		}

		return new Bookmark(bookmark_ptr);
	}

	public static Bookmark fromCPtr(long c_ptr) {
		if (c_ptr == 0) {
			return null;
		}

		return new Bookmark(c_ptr);
	}

	public synchronized String getTitle() {
		return this.title;
	}

	public synchronized float getLatitude() {
		return this.lat;
	}

	public synchronized float getLongitude() {
		return this.lon;
	}
}
