package com.sgr_b2.compass.jni;


public class CalleeLookup extends JNIBaseObject {

	public static class Callee extends JNIBaseObject {

		private String number = null;

		private Callee(long c_ptr) {
			this.c_ptr = c_ptr;

			readCallee();
		}

		private void readCallee() {
			this.number = CCJNI.cmps_callee_lookup_callee_get_number(this.c_ptr);
		}

		@Override
		protected void _dispose() {
			CCJNI.cmps_callee_lookup_callee_free(this.c_ptr);
		}

		public static Callee fromCPtr(long c_ptr) {
			if (c_ptr == 0) {
				return null;
			}

			return new Callee(c_ptr);
		}

		public synchronized String getNumber() {
			return this.number;
		}
	}

	public CalleeLookup() {
		this.c_ptr = CCJNI.cmps_callee_lookup_alloc();
		CCJNI.cmps_callee_lookup_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_callee_lookup_release(this.c_ptr);
		CCJNI.cmps_callee_lookup_free(this.c_ptr);
	}

	public void reset() {
		CCJNI.cmpss_callee_lookup_reset(this.c_ptr);
	}

	public boolean pushCall(final String number, final String name, long timestamp, long duration) {
		return (CCJNI.cmpss_callee_lookup_push_call(this.c_ptr, number, name, timestamp, duration) == 0 ? true : false);
	}

	public Callee getBestNumber(int bestness) {
		long c_callee = CCJNI.cmpss_callee_lookup_best(this.c_ptr, bestness);
		return Callee.fromCPtr(c_callee);
	}
}
