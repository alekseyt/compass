package com.sgr_b2.compass.jni;


public class DistressDetector extends JNIBaseObject {

	public DistressDetector() {
		this.c_ptr = CCJNI.cmps_distress_alloc();
		CCJNI.cmps_distress_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_distress_release(this.c_ptr);
		CCJNI.cmps_distress_free(this.c_ptr);
	}

	public void reset() {
		CCJNI.cmpss_distress_reset(this.c_ptr);
	}

	public void updateAcceleration(float[] values, long now) {
		assert(values.length == 3);

		CCJNI.cmpss_distress_update_acceleration(this.c_ptr,
			values[0], values[1], values[2], now);
	}

	public void updateAlertButton(boolean on, long now) {
		CCJNI.cmpss_distress_update_alert_button(this.c_ptr, (on ? 1 : 0), now);
	}

	public boolean armed(long now) {
		return (CCJNI.cmpss_distress_armed(this.c_ptr, now) != 0);
	}

	public boolean pushed(long now) {
		return (CCJNI.cmpss_distress_pushed(this.c_ptr, now) != 0);
	}

	public boolean panic(long now) {
		return (CCJNI.cmpss_distress_panic(this.c_ptr, now) != 0);
	}
}
