package com.sgr_b2.compass.jni;


public class Location extends JNIBaseObject {

	public final static String PROVIDER = "CC";

	public Location() {
		this.c_ptr = CCJNI.cmps_location_alloc();
		CCJNI.cmps_location_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_location_release(this.c_ptr);
		CCJNI.cmps_location_free(this.c_ptr);
	}

	public float getLatitude() {
		return CCJNI.cmpss_location_get_lat(this.c_ptr);
	}

	public void setLatitude(float lat) {
		CCJNI.cmpss_location_set_lat(this.c_ptr, lat);
	}

	public float getLongitude() {
		return CCJNI.cmpss_location_get_lon(this.c_ptr);
	}

	public void setLongitude(float lon) {
		CCJNI.cmpss_location_set_lon(this.c_ptr, lon);
	}

	public void setBearing(float bearing) {
		CCJNI.cmpss_location_set_bearing(this.c_ptr, bearing);
	}

	public void setSpeed(float speed) {
		CCJNI.cmpss_location_set_speed(this.c_ptr, speed);
	}
}
