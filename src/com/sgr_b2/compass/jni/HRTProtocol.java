package com.sgr_b2.compass.jni;


public class HRTProtocol {

	public static String marshall(final String title, float lat, float lon) {
		return CCJNI.cmps_hrt_marshall(title, lat, lon);
	}

	public static final Bookmark unmarshall(final String string) {
		return Bookmark.fromString(string);
	}
}
