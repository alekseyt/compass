package com.sgr_b2.compass.jni;


public class Router extends JNIBaseObject {

	public static class State extends JNIBaseObject {

		private boolean has_location = false;
		private Location location = new Location();
		private float azimuth = 0;
		private boolean direction_is_magnetic = false;
		private boolean direction_from_gps = false;
		private float distance = 0;
		private float direction = 0;
		private float speed = 0;

		public State(final Router router, long now) {
			this.c_ptr = CCJNI.cmps_router_get_state(router.getCPtr(), now);

			readState();
		}

		private void readState() {
			this.has_location = (CCJNI.cmps_router_state_has_location(this.c_ptr) == 0 ? false : true);
			if (this.has_location) {
				CCJNI.cmps_router_state_get_location(this.c_ptr, this.location.getCPtr());
			}
			this.azimuth = CCJNI.cmps_router_state_get_azimuth(this.c_ptr);
			this.direction_is_magnetic = (CCJNI.cmps_router_state_direction_is_magnetic(this.c_ptr) == 0 ? false : true);
			this.direction_from_gps = (CCJNI.cmps_router_state_direction_from_gps(this.c_ptr) == 0 ? false : true);
			this.direction = CCJNI.cmps_router_state_get_direction(this.c_ptr);
			this.distance = CCJNI.cmps_router_state_get_distance(this.c_ptr);
			this.speed = CCJNI.cmps_router_state_get_speed(this.c_ptr);
		}

		@Override
		protected void _dispose() {
			this.location.dispose();
			CCJNI.cmps_router_state_free(this.c_ptr);
		}

		public synchronized boolean hasLocation() {
			return this.has_location;
		}

		public synchronized final Location getLocation() {
			return this.location;
		}

		public synchronized float getAzimuth() {
			return this.azimuth;
		}

		public synchronized boolean directionIsMagnetic() {
			return this.direction_is_magnetic;
		}

		public synchronized boolean directionFromGPS() {
			return this.direction_from_gps;
		}

		public synchronized float getDirection() {
			return this.direction;
		}

		public synchronized float getDistance() {
			return this.distance;
		}

		public synchronized float getSpeed() {
			return this.speed;
		}
	}

	public Router() {
		this.c_ptr = CCJNI.cmps_router_alloc();
		CCJNI.cmps_router_init(this.c_ptr);
	}

	@Override
	protected void _dispose() {
		CCJNI.cmps_router_release(this.c_ptr);
		CCJNI.cmps_router_free(this.c_ptr);
	}

	public void setTarget(Location target) {
		long c_target = (target == null ? 0 : target.getCPtr());
		CCJNI.cmpss_router_set_target(this.c_ptr, c_target);
	}

	public void updateLocation(Location location, long now) {
		long c_location = (location == null ? 0 : location.getCPtr());
		CCJNI.cmpss_router_update_location(this.c_ptr, c_location, now);
	}

	public void updateMagneticHeading(float azimuth) {
		CCJNI.cmpss_router_update_mag_heading(this.c_ptr, azimuth);
	}

	public final State getState(long now) {
		return new State(this, now);
	}
}
