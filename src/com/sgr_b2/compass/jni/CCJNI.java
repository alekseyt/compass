package com.sgr_b2.compass.jni;


public class CCJNI {

	final static String library = "compass";

	public static native float cmps_sanitize_lat(float lat);
	public static native float cmps_sanitize_lon(float lon);
	public static native float cmps_normalize_azimuth(float azimuth);
	public static native float cmps_direction(long c_location_here, long c_location_there);
	public static native float cmps_distance(long c_location, long c_location_target);
	public static native float cmps_azimuth_to_direction(float azimuth, int directions);
	public static native float cmps_magnetic_field_sqr_strength(float x, float y, float z);
	public static native int cmps_sqr_magnetic_field_valid(float sqr_strength);

	public static native long cmps_location_alloc();
	public static native void cmps_location_free(long c_location);
	public static native void cmps_location_init(long c_location);
	public static native void cmps_location_release(long c_location);
	public static native float cmpss_location_get_lat(long c_location);
	public static native void cmpss_location_set_lat(long c_location, float lat);
	public static native float cmpss_location_get_lon(long c_location);
	public static native void cmpss_location_set_lon(long c_location, float lon);
	public static native float cmpss_location_get_speed(long c_location);
	public static native void cmpss_location_set_speed(long c_location, float speed);
	public static native float cmpss_location_get_bearing(long c_location);
	public static native void cmpss_location_set_bearing(long c_location, float bearing);

	public static native long cmps_tracker_alloc();
	public static native void cmps_tracker_free(long c_tracker);
	public static native void cmps_tracker_init(long c_tracker);
	public static native void cmps_tracker_release(long c_tracker);
	public static native int cmpss_tracker_get_location(long c_tracker, long c_location);
	public static native void cmpss_tracker_update_location(long c_tracker, long c_location, long now);
	public static native int cmpss_tracker_has_location_heading(long c_tracker);
	public static native float cmpss_tracker_get_location_heading(long c_tracker);
	public static native int cmpss_tracker_has_magnetic_heading(long c_tracker);
	public static native float cmpss_tracker_get_magnetic_heading(long c_tracker);
	public static native void cmpss_tracker_update_magnetic_heading(long c_tracker, float azimuth);
	public static native int cmpss_tracker_has_speed(long c_tracker);
	public static native float cmpss_tracker_get_speed(long c_tracker);
	public static native float cmpss_tracker_get_direction(long c_tracker, float heading, long c_location);
	public static native float cmpss_tracker_get_distance(long c_tracker, long c_location);

	public static native float cmps_low_pass_filter(float input, float output, float alpha);

	public static native long cmps_screen_filter_alloc();
	public static native void cmps_screen_filter_free(long c_filter);
	public static native void cmps_screen_filter_init(long c_filter, float threshold);
	public static native void cmps_screen_filter_release(long c_filter);
	public static native void cmpss_screen_filter_reset(long c_filter);
	public static native float cmpss_screen_filter_get_current(long c_filter);
	public static native float cmpss_screen_filter_pass(long c_filter, float current);

	public static native long cmps_router_alloc();
	public static native void cmps_router_free(long c_router);
	public static native void cmps_router_init(long c_router);
	public static native void cmps_router_release(long c_router);
	public static native void cmpss_router_set_target(long c_router, long c_location);
	public static native void cmpss_router_update_location(long c_router, long c_location, long now);
	public static native void cmpss_router_update_mag_heading(long c_router, float azimuth);

	public static native long cmps_router_get_state(long c_router, long now);
	public static native void cmps_router_state_free(long c_state);
	public static native int cmps_router_state_has_location(long c_state);
	public static native void cmps_router_state_get_location(long c_state, long c_location);
	public static native float cmps_router_state_get_azimuth(long c_state);
	public static native int cmps_router_state_direction_is_magnetic(long c_state);
	public static native int cmps_router_state_direction_from_gps(long c_state);
	public static native float cmps_router_state_get_direction(long c_state);
	public static native float cmps_router_state_get_distance(long c_state);
	public static native float cmps_router_state_get_speed(long c_state);

	public static native String cmps_hrt_marshall(String title, float lat, float lon);
	public static native long cmps_hrt_unmarshall(String text);
	public static native void cmps_hrt_bookmark_free(long c_bookmark);
	public static native String cmps_hrt_bookmark_get_title(long c_bookmark);
	public static native float cmps_hrt_bookmark_get_lat(long c_bookmark);
	public static native float cmps_hrt_bookmark_get_lon(long c_bookmark);

	public static native long cmps_distress_alloc();
	public static native void cmps_distress_free(long c_detector);
	public static native void cmps_distress_init(long c_detector);
	public static native void cmps_distress_release(long c_detector);
	public static native void cmpss_distress_reset(long c_detector);
	public static native void cmpss_distress_update_acceleration(long c_detector, float x, float y, float z, long now);
	public static native void cmpss_distress_update_alert_button(long c_detector, int on, long now);
	public static native int cmpss_distress_armed(long c_detector, long now);
	public static native int cmpss_distress_pushed(long c_detector, long now);
	public static native int cmpss_distress_panic(long c_detector, long now);

	public static native long cmps_callee_lookup_alloc();
	public static native void cmps_callee_lookup_free(long c_lookup);
	public static native void cmps_callee_lookup_init(long c_lookup);
	public static native void cmps_callee_lookup_release(long c_lookup);
	public static native void cmpss_callee_lookup_reset(long c_lookup);
	public static native int cmpss_callee_lookup_push_call(long c_lookup, String number, String name, long timestamp, long duration);
	public static native long cmpss_callee_lookup_best(long c_lookup, int bestness);

	public static native void cmps_callee_lookup_callee_free(long c_callee);
	public static native String cmps_callee_lookup_callee_get_number(long c_callee);

	public static native long cmps_dynamo_alloc();
	public static native void cmps_dynamo_free(long c_dynamo);
	public static native void cmps_dynamo_init(long c_dynamo);
	public static native void cmps_dynamo_release(long c_dynamo);
	public static native void cmpss_dynamo_reset(long c_dynamo);
	public static native void cmpss_dynamo_next(long c_dynamo);
	public static native String cmpss_dynamo_get_number(long c_dynamo, String preset_number, long c_lookup);

	public static native float cmps_config_tracker_angle_alpha();
	public static native float cmps_config_tracker_speed_alpha();
	public static native long cmps_config_router_location_ttl();
	public static native long cmps_config_router_speed_ttl();
	public static native int cmps_config_distress_alert_level_armed();
	public static native long cmps_config_distress_alert_level_ttl();
	public static native long cmps_config_distress_armed_ttl();
	public static native float cmps_config_g();
	public static native float cmps_config_distress_force_threshold();
	public static native int cmps_config_max_number_length();

	static {
		System.loadLibrary(CCJNI.library);
	}
}
