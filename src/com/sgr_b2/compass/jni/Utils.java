package com.sgr_b2.compass.jni;


public class Utils {

	public static float direction(Location here, Location there) {
		return CCJNI.cmps_direction(here.getCPtr(), there.getCPtr());
	}

	public static float sanitizeLat(float lat) {
		return CCJNI.cmps_sanitize_lat(lat);
	}

	public static float sanitizeLon(float lon) {
		return CCJNI.cmps_sanitize_lon(lon);
	}

	public static float normalizeAzimuth(float azimuth) {
		return CCJNI.cmps_normalize_azimuth(azimuth);
	}

	public static float azimuthToDirection(float azimuth, int directions) {
		return CCJNI.cmps_azimuth_to_direction(azimuth, directions);
	}

	public static float magneticFieldSqrStrength(float[] values) {
		return CCJNI.cmps_magnetic_field_sqr_strength(values[0], values[1], values[2]);
	}

	public static boolean sqrMagneticFieldValid(float sqr_strength) {
		return (CCJNI.cmps_sqr_magnetic_field_valid(sqr_strength) != 0)
			? true : false;
	}
}
