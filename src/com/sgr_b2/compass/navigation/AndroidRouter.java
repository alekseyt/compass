package com.sgr_b2.compass.navigation;

import com.sgr_b2.compass.jni.Router;
import com.sgr_b2.compass.jni.android.Adopt;


public class AndroidRouter extends Router {

	public static final int EXTRA_SATELLITES = 0;
	public static final int EXTRA_MAG_ACCURACY = 1;
	public static final int EXTRA_MAG_IN_RANGE = 2;
	public static final int EXTRA_GPS_ACCURACY = 3;

	public static final int MAG_ACCURACY_NONE = 0;
	public static final int MAG_ACCURACY_LOW = 1;
	public static final int MAG_ACCURACY_NORMAL = 2;
	public static final int MAG_ACCURACY_HIGH = 3;

	private int magnetic_accuracy = MAG_ACCURACY_HIGH;
	private boolean magnetic_in_range = true;
	private int gps_satellites = 0;
	private int gps_accuracy = 0;

	public synchronized void setTarget(android.location.Location target) {
		super.setTarget(Adopt.location(target));
	}

	public synchronized void setExtra(int extra, int value) {
		switch (extra) {
		case EXTRA_SATELLITES:
			this.gps_satellites = value;
			break;
		case EXTRA_MAG_ACCURACY:
			this.magnetic_accuracy = value;
			break;
		case EXTRA_MAG_IN_RANGE:
			this.magnetic_in_range = (value != 0 ? true : false);
			break;
		case EXTRA_GPS_ACCURACY:
			this.gps_accuracy = value;
			break;
		default:
			assert(false);
		}
	}

	public synchronized void readState(Monitor.Entry entry, long now) {
		final Router.State state = getState(now);

		if (state.hasLocation()) {
			entry.location = Adopt.location(state.getLocation());
		}

		entry.azimuth = state.getAzimuth();
		entry.direction_is_magnetic = state.directionIsMagnetic();
		entry.direction_from_gps = state.directionFromGPS();
		entry.speed = state.getSpeed();
		entry.direction = state.getDirection();
		entry.distance = state.getDistance();

		state.dispose();
	}

	public synchronized void selectMagneticExtras(Monitor.Entry entry, long now) {
		switch (this.magnetic_accuracy) {
		case MAG_ACCURACY_HIGH:
		case MAG_ACCURACY_NORMAL:
			entry.magnetic_accuracy_low = false;
			break;

		default:
			entry.magnetic_accuracy_low = true;
		}

		entry.magnetic_in_range = this.magnetic_in_range;
	}

	public synchronized void selectLocationExtras(Monitor.Entry entry, long now) {
		entry.gps_satellites = this.gps_satellites;
		entry.gps_accuracy = this.gps_accuracy;
	}

	public synchronized void updateLocation(android.location.Location location, long now) {
		super.updateLocation(Adopt.location(location), now);

		if (location == null) {
			setExtra(EXTRA_SATELLITES, 0);
		}
	}
}
