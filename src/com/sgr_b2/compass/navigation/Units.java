package com.sgr_b2.compass.navigation;


public class Units {
	/** Meters to kilometers.
	 *
	 * Ok, this is stupid, but it better to have this along with @see feet2miles.
	 */
	public static float m2km(float m) {
		return m / 1000;
	}

	/** Meters to feet
	 */
	public static float m2ft(float m) {
		return m * 3.28084f;
	}

	public static float ft2mi(float feet) {
		return feet / 5280;
	}

	/** Meters per second to kilometers per hour
	 *
	 * @return speed in km/h
	 */
	public static float mps2kmph(float mps) {
		return (mps / (1000.0f / 60 / 60));
	}

	/** Meters per second to miles per hour
	 *
	 * @return speed in m/s
	 */
	public static float mps2miph(float mps) {
		return mps * 3600 / 1609.344f;
	}
}
