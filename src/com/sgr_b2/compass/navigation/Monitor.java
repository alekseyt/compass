package com.sgr_b2.compass.navigation;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.activities.DistressCallService;
import com.sgr_b2.compass.activities.ImportActivity;
import com.sgr_b2.compass.activities.MainActivity;
import com.sgr_b2.compass.db.LogProvider;
import com.sgr_b2.compass.jni.Utils;
import com.sgr_b2.compass.phone.SMSPOI;
import com.sgr_b2.compass.phone.SMSReader;


public class Monitor extends AsyncTask<Object, Monitor.Entry, Object>
	implements SensorEventListener, LocationListener {

	public static class Entry {
		// tracker stats
		public float azimuth;
		public float distance;
		public float direction;
		public boolean direction_is_magnetic;
		public boolean direction_from_gps;
		public float speed;
		public Location location;

		// sensor stats
		public boolean magnetic_accuracy_low;
		public boolean magnetic_in_range;
		public int gps_satellites;
		public int gps_accuracy;

		public boolean indistress;

		public Entry() {
			azimuth = 0;
			distance = 0;
			direction = 0;
			direction_is_magnetic = false;
			direction_from_gps = false;
			speed = 0;
			location = null;

			magnetic_accuracy_low = false;
			magnetic_in_range = true;
			gps_satellites = 0;
			gps_accuracy = 0;

			indistress = false;
		}

		public boolean equals(final Entry other) {
			/* what matters for UI are:
			 *
			 * - compass direction
			 * - distance
			 * - speed
			 * - flags
			 */

			return Math.abs(Utils.azimuthToDirection(direction, Config.COMPASS_DIRECTIONS)
				- Utils.azimuthToDirection(other.direction, Config.COMPASS_DIRECTIONS)) < 0.01
				&& Math.abs(distance - other.distance) < 0.01
				&& Math.abs(speed - other.speed) < 0.01
				&& direction_is_magnetic == other.direction_is_magnetic
				&& direction_from_gps == other.direction_from_gps
				&& magnetic_accuracy_low == other.magnetic_accuracy_low
				&& magnetic_in_range == other.magnetic_in_range
				&& indistress == other.indistress
				;
		}
	}

	private MainActivity main;
	private boolean die = false;
	private long sensors_update_timestamp = -1;
	private AndroidRouter router = new AndroidRouter();

	private float[] gravity = null;
	private float[] geomagnetic = null;
	private float[] r_matrix = new float[16];
	private float[] i_matrix = new float[16];

	private SMSReader sms_reader = null;
	private LogProvider log_provider = null;

	public Monitor(MainActivity main) {
		this.main = main;
		this.die = false;
		this.sms_reader = new SMSReader(main);
		this.log_provider = new LogProvider(main);
	}

	public void die() {
		this.die = true;
	}

	public void setTarget(Location target) {
		this.router.setTarget(target);
	}

	/**
	 * check SMS messages, running in BG thread
	 */
	private boolean checkTheTweets() {
		if (this.sms_reader == null || this.log_provider == null) {
			return false;
		}

		String last_recorded_sms_id = this.log_provider.getLastSMSId();
		String last_actual_sms_id = this.sms_reader.getLastSMSId();

		if (last_recorded_sms_id != null
			&& last_recorded_sms_id.equals(last_actual_sms_id)) {
			return false;
		}

		final SMSPOI poi = this.sms_reader.findLocation(last_recorded_sms_id,
			SMSReader.HISTORY_DEFAULT);

		this.log_provider.setLastSMSId(last_actual_sms_id);

		return (poi != null);
	}

	@Override
	protected Object doInBackground(Object... params) {
		long tick = 0;

		Entry old_entry = new Entry();
		Entry current_entry = new Entry();

		boolean first_run = true;

		while (!this.die) {
			try {
				Thread.sleep(Config.MONITOR_READ_STATE_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			long now = System.currentTimeMillis();

			current_entry.indistress = DistressCallService.indistress;
			this.router.readState(current_entry, now);
			this.router.selectMagneticExtras(current_entry, now);
			this.router.selectLocationExtras(current_entry, now);

			if (!current_entry.equals(old_entry) || first_run) {
				publishProgress(current_entry);

				Entry tmp = old_entry;
				old_entry = current_entry;
				current_entry = tmp;

				first_run = false;
			}

			boolean tweets = false;

			if (tick % Config.MONITOR_SMS_CHECK_INTERVAL == 0) {
				tweets = checkTheTweets(); // und emails
			}

			if (tweets) {
				Intent intent = new Intent(this.main, ImportActivity.class);
				this.main.startActivity(intent);
			}

			++tick; // no worries about overflow, it will still work
		}

		this.log_provider.dispose();
		this.router.dispose();

		return null;
	}

	protected void onProgressUpdate(Entry... progress) {
		for (Entry entry : progress) {
			this.main.updateStatus(entry);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	private int androidToRouterAccuracy(int accuracy) {
		switch (accuracy) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			return AndroidRouter.MAG_ACCURACY_HIGH;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			return AndroidRouter.MAG_ACCURACY_NORMAL;
		case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
			return AndroidRouter.MAG_ACCURACY_LOW;
		default:
			return AndroidRouter.MAG_ACCURACY_NONE;
		}
	}

	@Override
	public synchronized void onSensorChanged(SensorEvent event) {
		if (this.sensors_update_timestamp > 0
			&& event.timestamp - this.sensors_update_timestamp < Config.MONITOR_SENSORS_UPDATE_INTERVAL) {
			return;
		}

		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			this.gravity = event.values.clone();
		}

		if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			this.geomagnetic = event.values.clone();
			this.router.setExtra(AndroidRouter.EXTRA_MAG_ACCURACY,
				androidToRouterAccuracy(event.accuracy));
			this.router.setExtra(AndroidRouter.EXTRA_MAG_IN_RANGE,
				(Utils.sqrMagneticFieldValid(
					Utils.magneticFieldSqrStrength(this.geomagnetic)) ? 1 : 0));
		}

		if (this.gravity == null || this.geomagnetic == null) {
			return;
		}

		SensorManager.getRotationMatrix(this.r_matrix, this.i_matrix,
			this.gravity, this.geomagnetic);

		float[] orientation = new float[3];
		SensorManager.getOrientation(this.r_matrix, orientation);

		float azimuth = orientation[0] * (float)(180.0f / Math.PI);

		this.router.updateMagneticHeading(azimuth);

		this.gravity = this.geomagnetic = null;
		this.sensors_update_timestamp = event.timestamp;
	}

	@Override
	public synchronized void onLocationChanged(Location location) {
		if (location != null) {
			if (!location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
				return;
			}

			if (!location.hasAccuracy()) {
				return;
			}

			this.router.setExtra(AndroidRouter.EXTRA_GPS_ACCURACY, (int)location.getAccuracy());

			if (location.getAccuracy() > Config.MONITOR_ACCURACY_THRESHOLD) {
				return;
			}
		}

		this.router.updateLocation(location, System.currentTimeMillis());
	}

	@Override
	public synchronized void onProviderDisabled(String provider) {
		this.router.updateLocation((Location)null, System.currentTimeMillis());
	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public synchronized void onStatusChanged(String provider, int status, Bundle extras) {
		if (!provider.equals(LocationManager.GPS_PROVIDER)) {
			return;
		}

		switch (status) {
		case LocationProvider.OUT_OF_SERVICE:
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
			this.router.updateLocation((Location)null, System.currentTimeMillis());
			this.router.setExtra(AndroidRouter.EXTRA_GPS_ACCURACY, 0);
			break;
		default:
			if (extras != null) {
				this.router.setExtra(AndroidRouter.EXTRA_SATELLITES,
					extras.getInt("satellites", 0));
			}
		}
	}
}
