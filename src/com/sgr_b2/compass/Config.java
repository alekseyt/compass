package com.sgr_b2.compass;

import com.sgr_b2.compass.jni.CCJNI;


public class Config {

	/* Monitor config
	 */
	public static final float MONITOR_ACCURACY_THRESHOLD = 50; // meters with 68% probability
	public static final long  MONITOR_SMS_CHECK_INTERVAL = 10; // seconds
	public static final long  MONITOR_READ_STATE_INTERVAL = 1000; // msecs
	public static final long  MONITOR_SENSORS_UPDATE_INTERVAL = MONITOR_READ_STATE_INTERVAL * 1000000; // nanosecs

	/* Tracker config
	 */
	public static final float TRACKER_ANGLE_ALPHA = CCJNI.cmps_config_tracker_angle_alpha();
	public static final float TRACKER_SPEED_ALPHA = CCJNI.cmps_config_tracker_speed_alpha();

	/* Router config
	 */
	public static final long  ROUTER_LOCATION_TTL = CCJNI.cmps_config_router_location_ttl();
	public static final long  ROUTER_SPEED_TTL = CCJNI.cmps_config_router_speed_ttl();

	/* Distress detector config
	 */
	public static float       D_DETECTOR_FORCE_THRESHOLD = CCJNI.cmps_config_distress_force_threshold();
	public static long        D_DETECTOR_ALERT_LEVEL_TTL = CCJNI.cmps_config_distress_alert_level_ttl();
	public static int         D_DETECTOR_ALERT_LEVEL_ARMED = CCJNI.cmps_config_distress_alert_level_armed();
	public static long        D_DETECTOR_ARMED_TTL = CCJNI.cmps_config_distress_armed_ttl();

	/* DeviceWrapper config
	 */
	public static final long  DEV_WRAPPER_MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // meters
	public static final long  DEV_WRAPPER_MIN_TIME_BW_UPDATES = 1 * 1000; // msecs

	/* MainActivity config
	 */
	public static final long  COMPASS_ANIMATION_DURATION = 1 * 250; // msecs
	public static final int   COMPASS_DIRECTIONS = 16; // number of compass directions
	public static final float COMPASS_ANIMATION_THRESHOLD = 360f / COMPASS_DIRECTIONS / 2;
}
