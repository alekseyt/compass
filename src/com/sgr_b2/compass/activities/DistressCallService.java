package com.sgr_b2.compass.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.io.SMSText;
import com.sgr_b2.compass.jni.CalleeLookup;
import com.sgr_b2.compass.jni.DistressDetector;
import com.sgr_b2.compass.jni.DistressDynamo;
import com.sgr_b2.compass.phone.CallsReader;
import com.sgr_b2.compass.phone.CancelDistressReceiver;
import com.sgr_b2.compass.phone.DeviceWrapper;
import com.sgr_b2.compass.phone.PowerButtonBroadcastReceiver;
import com.sgr_b2.compass.phone.UpdateDistressReceiver;


public class DistressCallService
	extends Service
	implements SensorEventListener, LocationListener,
	SharedPreferences.OnSharedPreferenceChangeListener {

	public static String ACTION_CANCEL = "com.sgr_b2.compass.activities.DistressCallService.CANCEL_DISTRESS";
	public static String ACTION_UPDATE = "com.sgr_b2.compass.activities.DistressCallService.UPDATE";

	public static long[] VIBRATE_ARMED = { 0, 200, 0 }; // msecs
	public static long[] VIBRATE_DISTRESS = { 0, 1000, 0 }; // msecs
	public static long[] VIBRATE_SENT = { 0, 200, 500, 200, 500, 200, 0 }; // msecs

	private static final int STATE_DISARMED = 0;
	private static final int STATE_ARMED = 1;
	private static final int STATE_CALLING = 2;

	private static final int NOTIFICATION_ID = 1;

	private int state = STATE_DISARMED;
	private DistressDynamo dynamo = new DistressDynamo(); // no need to dispose() since service is always running

	private DeviceWrapper device_wrapper = null;
	private final DistressDetector detector = new DistressDetector();
	private SharedPreferences shared_pref = null;
	private final PowerButtonBroadcastReceiver power_button_listener = new PowerButtonBroadcastReceiver(this, this.detector);
	private final CancelDistressReceiver cancel_listener = new CancelDistressReceiver(this);
	private final UpdateDistressReceiver update_listener = new UpdateDistressReceiver(this);
	private NotificationManager notification_man = null;

	public static boolean indistress = false; // dammit, i don't need BROADCAST_STICKY permission for my app

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		startForeground(NOTIFICATION_ID, getNotification());

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		this.notification_man = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

		this.shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		this.shared_pref.registerOnSharedPreferenceChangeListener(this);

		this.device_wrapper = new DeviceWrapper(getBaseContext());
		DistressCallService.indistress = false;

		setupPowerButton();

		setupIcon();
		update();
	}

	@Override
	public void onDestroy() {
		this.shared_pref.unregisterOnSharedPreferenceChangeListener(this);

		releaseIcon();
		releasePowerButton();
		releaseSensors();
		releaseGPS();

		this.notification_man.cancel(NOTIFICATION_ID);

		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private Notification getNotification() {
		Intent i = new Intent(this, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent pi = PendingIntent.getActivity(this, 0,
			i, Notification.FLAG_ONGOING_EVENT);

		final String title = getResources().getString(R.string.app_name);
		String message = getResources().getString(R.string.alert_distress_call_ready);
		int icon = R.drawable.distress_icon_unarmed;

		switch (this.state) {
		case STATE_ARMED:
			icon = R.drawable.distress_icon_armed;
			break;

		case STATE_CALLING:
			icon = R.drawable.distress_icon_calling;
			message = getResources().getString(R.string.alert_distress_call);
			break;
		}

		Notification n = new Notification(icon, message, System.currentTimeMillis());
		n.flags = Notification.FLAG_ONGOING_EVENT;
		n.setLatestEventInfo(this, title, message, pi);

		return n;
	}

	private void updateTrayIcon() {
		this.notification_man.notify(NOTIFICATION_ID, getNotification());
	}

	public synchronized boolean arm() {
		if (this.state != STATE_DISARMED) {
			return false;
		}

		if (!setupSensors()) {
			return false;
		}

		this.state = STATE_ARMED;
		indicateArmedStatus();

		try {
			updateTrayIcon();
		}
		catch (Exception e) {}

		return true;
	}

	public synchronized boolean disarm() {
		if (this.state != STATE_ARMED) {
			return false;
		}

		this.state = STATE_DISARMED;
		releaseSensors();

		try {
			updateTrayIcon();
		}
		catch (Exception e) {}

		return true;
	}

	public synchronized boolean call() {
		if (!setupGPS()) {
			disarm();
			return false;
		}

		releaseSensors();

		this.state = STATE_CALLING;
		indicateDistressStatus();

		DistressCallService.indistress = true;

		try {
			updateTrayIcon();
		}
		catch (Exception e) {}

		return true;
	}

	public synchronized boolean hangup() {
		if (this.state != STATE_CALLING) {
			return false;
		}

		this.state = STATE_DISARMED;
		releaseGPS();

		this.dynamo.reset();

		DistressCallService.indistress = false;

		try {
			updateTrayIcon();
		}
		catch (Exception e) {}

		return true;
	}

	public synchronized void update() {
		try {
			updateTrayIcon();
		}
		catch (Exception e) {}
	}

	private void setupPowerButton() {
		getBaseContext().registerReceiver(this.power_button_listener, new IntentFilter(Intent.ACTION_SCREEN_ON));
		getBaseContext().registerReceiver(this.power_button_listener, new IntentFilter(Intent.ACTION_SCREEN_OFF));
		getBaseContext().registerReceiver(this.cancel_listener, new IntentFilter(DistressCallService.ACTION_CANCEL));
	}

	private void releasePowerButton() {
		getBaseContext().unregisterReceiver(this.power_button_listener);
		getBaseContext().unregisterReceiver(this.cancel_listener);
	}

	private void setupIcon() {
		getBaseContext().registerReceiver(this.update_listener, new IntentFilter(DistressCallService.ACTION_UPDATE));
	}

	private void releaseIcon() {
		getBaseContext().unregisterReceiver(this.update_listener);
	}

	private boolean setupSensors() {
		return this.device_wrapper.setupAccelerometer(this);
	}

	private void releaseSensors() {
		this.device_wrapper.releaseAccelerometer(this);
	}

	private boolean setupGPS() {
		return this.device_wrapper.setupGPS(this);
	}

	private void releaseGPS() {
		this.device_wrapper.releaseGPS(this);
	}

	private synchronized void indicateArmedStatus() {
		Vibrator vibrator = (Vibrator)getBaseContext().getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(VIBRATE_ARMED, -1);
	}

	private synchronized void indicateDistressStatus() {
		Vibrator vibrator = (Vibrator)getBaseContext().getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(VIBRATE_DISTRESS, -1);
	}

	private synchronized void indicateLocationSent() {
		Vibrator vibrator = (Vibrator)getBaseContext().getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(VIBRATE_SENT, -1);
	}

	private String sosText(final Location location) {
		POI poi = new POI(location, getResources().getString(R.string.sos_sms_text));

		return SMSText.format(this, poi,
			getResources().getString(R.string.sos_sms_append));
	}

	/**
	 * @return preset distress number if it's enabled, set and valid. return null otherwise
	 */
	private String distressNumberValid() {
		final String distress_number = this.shared_pref.getString(SettingsActivity.KEY_PREF_DISTRESS_NUMBER, "");
		final boolean use_distress_number = this.shared_pref.getBoolean(SettingsActivity.KEY_PREF_DISTRESS_USE_NUMBER, false);

		return (use_distress_number
			&& distress_number != null
			&& distress_number.length() > 0) ? distress_number : null;
	}

	private void sendSMS(final String number, final String text) {
		// send sms
		SmsManager sms_manager = SmsManager.getDefault();
		sms_manager.sendTextMessage(number, null, text, null, null);

		// make a record in sent
		ContentValues values = new ContentValues();
		values.put("address", number);
		values.put("body", text);
		getContentResolver().insert(Uri.parse("content://sms/sent"), values);
	}

	private synchronized boolean sendLocation(final Location location) {
		final String distress_number = distressNumberValid();
		final boolean use_preset_number_only = this.shared_pref.getBoolean(SettingsActivity.KEY_PREF_DISTRESS_USE_NUMBER_ONLY, false);

		if (distress_number == null && use_preset_number_only) {
			return false;
		}

		CalleeLookup lookup = null;

		if (!use_preset_number_only) {
			lookup = new CalleeLookup();
			CallsReader reader = new CallsReader(getBaseContext());

			for (CallsReader.Call call : reader.getCalls()) {
				lookup.pushCall(call.number, call.cached_name, call.date, call.duration);
			}
		}

		final String best_number = this.dynamo.getNumber(distress_number, lookup);

		if (best_number == null) {
			return false;
		}

		sendSMS(best_number, sosText(location));

		// rotate numbers, next distress call will be to another number if possible
		this.dynamo.next();

		if (lookup != null) {
			lookup.dispose();
		}

		return true;
	}

	@Override
	public synchronized void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	@Override
	public synchronized void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
			return;
		}

		long now = System.currentTimeMillis();

		this.detector.updateAcceleration(event.values, now);

		if (this.detector.panic(now)
			&& state != STATE_CALLING) {

			this.detector.reset();
			call();
		}
		else if (!this.detector.armed(now)) {
			disarm();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		synchronized (this) {
			if (location != null) {
				if (!location.hasAccuracy()) {
					return;
				}

				if (location.getAccuracy() > Config.MONITOR_ACCURACY_THRESHOLD) {
					return;
				}

				if (sendLocation(location)) {
					indicateLocationSent();
					hangup();
				}
			}
		}
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(SettingsActivity.KEY_PREF_DISTRESS_NUMBER)
		|| key.equals(SettingsActivity.KEY_PREF_DISTRESS_USE_NUMBER)) {
			// reset dynamo if preset distress number has changed
			this.dynamo.reset();
		}
	}
}
