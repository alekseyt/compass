package com.sgr_b2.compass.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.BookmarksProvider;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.ui.BookmarksAdapter;


public class BookmarksActivity
	extends TemporaryBaseActivity
	implements OnItemClickListener {

	private BookmarksAdapter adapter = null;
	private BookmarksProvider provider = null;

	private Location location = null;
	private POI selected_poi = null;

	public class Extras {
		public static final String CURRENT_LOCATION = "current_location";
		public static final String POI = "poi";
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_bookmarks);
		setTitle(getResources().getString(R.string.title_bookmarks));

		Intent intent = getIntent();
		this.location = (Location)intent.getParcelableExtra(Extras.CURRENT_LOCATION);

		this.provider = new BookmarksProvider(this);
		this.adapter = new BookmarksAdapter(this, provider);

		ListView pois = (ListView)findViewById(R.id.pois_layout);
		pois.setAdapter(this.adapter);
		pois.setOnItemClickListener(this);

		registerForContextMenu(pois);
	}

	@Override
	public void onDestroy() {
		if (this.provider != null) {
			this.provider.dispose();
		}

		super.onDestroy();
	}

	private void updateHintVisibility() {
		int visibility = (this.adapter.getCount() > 0
			? View.INVISIBLE
			: View.VISIBLE);

		findViewById(R.id.bookmarks_nothing_here).setVisibility(visibility);
	}

	@Override
	public void onResume() {
		super.onResume();

		this.adapter.update();
		updateHintVisibility();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		Intent intent = new Intent();
		intent.putExtra(Extras.POI, (POI)this.adapter.getItem(position));

		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menu_info) {
		super.onCreateContextMenu(menu, v, menu_info);

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menu_info;

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bookmarks_context, menu);

		POI poi = (POI)info.targetView.getTag();
		this.selected_poi = poi;

		menu.setHeaderTitle(poi.title);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_send_bookmark:
			if (!ActivityUtils.sendPOI(this, this.selected_poi)) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.menu_send_text);
				builder.setMessage(R.string.message_cant_send_sms);
				builder.setPositiveButton(android.R.string.ok, null);
				builder.show();
			}
			break;

		case R.id.menu_edit_bookmark:
			Intent intent = new Intent(this, AddEditActivity.class);
			intent.putExtra(AddEditActivity.Extras.MODE, AddEditActivity.MODE_EDIT);
			intent.putExtra(AddEditActivity.Extras.POI, this.selected_poi);

			startActivity(intent);
			break;

		case R.id.menu_delete_bookmark:
			{
				final BookmarksProvider provider = this.provider;
				final BookmarksAdapter adapter = this.adapter;
				final String poi_title = this.selected_poi.title;

				String message_text = String.format(
						getResources().getString(R.string.message_confirm_delete_template),
						poi_title);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getTitle());
				builder.setMessage(message_text);
				builder.setNegativeButton(android.R.string.cancel, null);
				builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						provider.deletePOI(poi_title);
						adapter.update();
					}
				});
				builder.show();
			}
			break;
		}

		return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bookmarks, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_add_bookmark:
		{
			Intent intent = new Intent(this, AddEditActivity.class);
			intent.putExtra(AddEditActivity.Extras.MODE, AddEditActivity.MODE_ADD);
			if (this.location != null) {
				intent.putExtra(AddEditActivity.Extras.LOCATION, this.location);
			}

			startActivity(intent);
			break;
		}

		case R.id.menu_import_bookmarks:
		{
			Intent i = new Intent(BookmarksActivity.this, ImportActivity.class);
			startActivity(i);
			break;
		}
		}

		return false;
	}
}
