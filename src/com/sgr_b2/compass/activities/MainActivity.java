package com.sgr_b2.compass.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.jni.Screen;
import com.sgr_b2.compass.jni.Utils;
import com.sgr_b2.compass.navigation.Monitor;
import com.sgr_b2.compass.phone.DeviceWrapper;
import com.sgr_b2.compass.themes.Theme;
import com.sgr_b2.compass.themes.ThemesFactory;
import com.sgr_b2.compass.ui.DontShowAgainOKListener;
import com.sgr_b2.compass.ui.UnitsFormatting;


public class MainActivity
	extends TemporaryBaseActivity
	implements OnGlobalLayoutListener, AnimationListener,
	DialogInterface.OnCancelListener {

	static class UI {
		public static ImageView compass_arrow = null;
		public static ImageView compass_bg = null;
		public static TextView alert_text = null;
		public static TextView distance_text = null;
		public static TextView target_text = null;
		public static Button distress_cancel_button = null;
		public static AlertDialog alert_dialog = null;
	}

	private static final int REQUEST_BOOKMARK_RESULT = 1001;
	private static final float NO_DIRECTION = -1; // invalid direction or no direction available

	private Location location = null;
	private POI target = null;

	private int graphics_state = -1; // used for graphics update

	private Monitor monitor = null;
	private Screen screen = new Screen(Config.COMPASS_ANIMATION_THRESHOLD);
	private DeviceWrapper device_wrapper = null;
	private Menu menu = null; // this is here to manually update menu items on Android 1.x-2.x

	private Theme theme = null;

	private float compass_wants = NO_DIRECTION;
	private boolean show_speed = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.device_wrapper = new DeviceWrapper(this);

		if (!this.device_wrapper.isMagneticSensorAvailable()) {
			maybeShowCompassSensorsAlert();
		}

		setContentView(R.layout.layout_main);

		initUIHolder();
	}

	private void initUIHolder() {
		UI.compass_arrow = (ImageView)findViewById(R.id.compass_arrow);
		UI.compass_bg = (ImageView)findViewById(R.id.compass_bg);
		UI.alert_text = (TextView)findViewById(R.id.alert_text);
		UI.distance_text = (TextView)findViewById(R.id.distance_text);
		UI.target_text = (TextView)findViewById(R.id.target_text);
		UI.distress_cancel_button = (Button)findViewById(R.id.distress_cancel_button);
		UI.alert_dialog = null;
	}

	private void maybeStartDistressCallService() {
		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean run_service = shared_pref.getBoolean(SettingsActivity.KEY_PREF_DISTRESS_RUN_SERVICE, true);

		if (run_service) {
			ActivityUtils.startDistressCallService(this);
		}
		else {
			ActivityUtils.stopDistressCallService(this);
		}
	}

	private void maybeShowCompassSensorsAlert() {
		if (UI.alert_dialog != null) {
			return;
		}

		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean show_sensors_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_SENSORS_ALERT, true);

		if (!show_sensors_alert) {
			return;
		}

		LayoutInflater layout_inflater = LayoutInflater.from(this);
		final View alert_view = layout_inflater.inflate(R.layout.layout_showonce_alert, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setPositiveButton(android.R.string.ok,
			new DontShowAgainOKListener(this, alert_view,
				SettingsActivity.KEY_PREF_SHOW_SENSORS_ALERT));
		builder.setMessage(R.string.message_no_magnetic_sensors);
		builder.setView(alert_view);
		builder.setOnCancelListener(this);
		UI.alert_dialog = builder.show();
	}

	@Override
	public void onResume() {
		super.onResume();

		this.monitor = new Monitor(this);
		this.monitor.setTarget(this.target != null ? this.target.getLocation() : null);
		this.monitor.execute((Object[])null);

		ViewTreeObserver observer = UI.compass_arrow.getViewTreeObserver();
		if (observer.isAlive()) {
			observer.addOnGlobalLayoutListener(this);
		}

		this.device_wrapper.setupAccelerometer(this.monitor);
		this.device_wrapper.setupMagneticSensor(this.monitor);
		this.device_wrapper.setupGPS(this.monitor);

		final SharedPreferences shared_pref =
			PreferenceManager.getDefaultSharedPreferences(this);

		this.show_speed = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_SPEED, false);

		this.theme = ThemesFactory.getTheme(this,
			shared_pref.getString(SettingsActivity.KEY_PREF_THEME,
				ThemesFactory.VANILLA));

		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		maybeStartDistressCallService();

		// works when MainActivity is exiting settings
		sendBroadcast(new Intent(DistressCallService.ACTION_UPDATE));
	}

	@Override
	public void onPause() {
		super.onPause();

		this.device_wrapper.releaseGPS(this.monitor);
		this.device_wrapper.releaseMagneticSensor(this.monitor);
		this.device_wrapper.releaseAccelerometer(this.monitor);

		if (this.monitor != null) {
			this.monitor.die();
			this.monitor = null;
		}

		this.screen.reset();
	}

	protected void onActivityResult(int request_code, int result_code, Intent data) {
		if (result_code != RESULT_OK) {
			return;
		}

		switch (request_code) {
		case REQUEST_BOOKMARK_RESULT:
			POI poi = (POI)data.getSerializableExtra(BookmarksActivity.Extras.POI);
			assert(poi != null);

			updateTarget(poi);
			break;
		}
	}

	private void rotateCompass(float from, float to, long duration, int repeat_count) {
		float distance_ccw = Math.abs(to - from);
		float distance_cw = 360 - Math.abs(to - from);

		// rotate compass using the shortest distance if animation
		// is not repeatable
		if (repeat_count >= 0 && distance_cw < distance_ccw) {
			to = from + (to < from ? +distance_cw : -distance_cw);
		}

		float around_x = UI.compass_arrow.getWidth() / 2;
		float around_y = UI.compass_arrow.getHeight() / 2;

		RotateAnimation rotation = new RotateAnimation(from, to, around_x, around_y);
		rotation.setDuration(duration);
		rotation.setRepeatCount(repeat_count);
		rotation.setInterpolator(new LinearInterpolator());
		rotation.setFillAfter(true);
		rotation.setAnimationListener(this);

		UI.compass_arrow.setAnimation(rotation);
		UI.compass_arrow.startAnimation(rotation);
	}

	private void updateDirection(boolean magnetic, boolean from_gps, float azimuth) {
		Animation animation = UI.compass_arrow.getAnimation();

		// round azimuth to direction
		// otherwise constant compass rotation will be bad for a battery
		float direction =  Utils.azimuthToDirection(this.screen.getCurrent(), Config.COMPASS_DIRECTIONS);
		float new_direction = Utils.azimuthToDirection(this.screen.filter(azimuth), Config.COMPASS_DIRECTIONS);

		if (!magnetic && !from_gps) {
			new_direction = 0;
		}

		if (Math.abs(direction - new_direction) < 0.01) {
			return;
		}

		if (animation != null && !animation.hasEnded()) {
			this.compass_wants = new_direction;
		}
		else {
			rotateCompass(direction, new_direction, Config.COMPASS_ANIMATION_DURATION, 0);
		}
	}

	private void updateDistanceAndSpeed(float meters, float mps, boolean readings_from_gps) {
		String distance_text = "";

		// compass target set - join with distance
		if (this.target != null && this.location != null) {
			distance_text = UnitsFormatting.formatDistance(this, meters);

			if (this.show_speed) {
				distance_text = UnitsFormatting.joinDistanceAndSpeed(
					distance_text,
					(readings_from_gps
						? UnitsFormatting.formatSpeed(this, mps)
						: "-")
				);
			}
		}
		// target is not set, but speed is enabled in options
		else if (this.show_speed && readings_from_gps) {
			distance_text = UnitsFormatting.formatSpeed(this, mps);
		}

		if (!UI.distance_text.getText().equals(distance_text)) {
			UI.distance_text.setText(distance_text);
		}
	}

	private void updateLocation(final Location location) {
		setLocation(location);
		updateSendLocationMenuItem(this.menu);
	}

	private void updateTarget(final POI target) {
		// trigger this message only on target update
		// since app can work as compass w/o gps
		if (!device_wrapper.isGPSAvailable()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.message_gps_not_available);
			builder.setTitle(R.string.app_name);
			builder.setPositiveButton(android.R.string.ok, null);
			builder.create().show();

			return;
		}

		setTarget(target);

		if (this.target != null && this.target.title != null) {
			UI.target_text.setText(target.title);
		}
	}

	private void setLocation(Location location) {
		this.location = location;
	}

	private void setTarget(POI target) {
		this.target = target;
	}

	private void maybeShowCalibrationAlert() {
		if (UI.alert_dialog != null) {
			return;
		}

		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean show_calibration_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_CALIBRATION_ALERT, true);

		if (!show_calibration_alert) {
			return;
		}

		LayoutInflater layout_inflater = LayoutInflater.from(this);
		final View alert_view = layout_inflater.inflate(R.layout.layout_showonce_alert, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setPositiveButton(android.R.string.ok,
			new DontShowAgainOKListener(this, alert_view,
				SettingsActivity.KEY_PREF_SHOW_CALIBRATION_ALERT));
		builder.setMessage(R.string.message_magnetic_calibration);
		builder.setView(alert_view);
		UI.alert_dialog = builder.show();
	}

	private void updateMagneticAccuracy(boolean unreliable, boolean direction_magnetic) {
		// only show alert if direction is magnetic
		if (unreliable && direction_magnetic) {
			maybeShowCalibrationAlert();
		}
	}

	private void maybeShowCloseToMagnetAlert() {
		if (UI.alert_dialog != null) {
			return;
		}

		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean show_range_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_MAGNETIC_RANGE_ALERT, true);

		if (!show_range_alert) {
			return;
		}

		LayoutInflater layout_inflater = LayoutInflater.from(this);
		final View alert_view = layout_inflater.inflate(R.layout.layout_showonce_alert, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getTitle());
		builder.setPositiveButton(android.R.string.ok,
			new DontShowAgainOKListener(this, alert_view,
				SettingsActivity.KEY_PREF_SHOW_MAGNETIC_RANGE_ALERT));
		builder.setMessage(R.string.message_magnetic_not_in_range);
		builder.setView(alert_view);
		builder.setOnCancelListener(this);
		UI.alert_dialog = builder.show();
	}

	private void updateMagneticInRange(boolean in_range, boolean direction_magnetic) {
		// only show alert if direction is magnetic
		if (!in_range && direction_magnetic) {
			maybeShowCloseToMagnetAlert();
		}
	}

	private int selectAlertMessage(final Monitor.Entry entry) {
		int preferred_message = -1;

		// in order of priority
		if (entry.indistress) {
			preferred_message = R.string.alert_distress_call;
		}
		else if (entry.direction_is_magnetic && !entry.magnetic_in_range) {
			preferred_message = R.string.alert_close_to_magnet;
		}
		else if (entry.direction_is_magnetic && entry.magnetic_accuracy_low) {
			preferred_message = R.string.alert_need_calibration;
		}
		// for Stas
		else {
			SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
			boolean show_waiting_location_alert = shared_pref.getBoolean(SettingsActivity.KEY_PREF_SHOW_WAITING_ALERT, true); // for Max

			if (show_waiting_location_alert && entry.location == null) {
				preferred_message = R.string.alert_waiting_location;
			}
		}

		return preferred_message;
	}

	private void updateAlertMessage(Monitor.Entry entry) {
		int message = selectAlertMessage(entry);

		// no preferred message - set empty message
		if (message < 0) {
			UI.alert_text.setText(null);
			return;
		}

		UI.alert_text.setText(message);
	}

	private void updateTitle(boolean magnetic, boolean from_gps, boolean indistress) {
		if (indistress) {
			setTitle(R.string.title_distress_call);
		}
		else if (magnetic) {
			setTitle(R.string.title_magnetic_compass);
		}
		else if (from_gps) {
			setTitle(R.string.title_gps_compass);
		}
		else {
			setTitle(R.string.title_no_direction);
		}
	}

	private void updateVisibility(boolean indistress) {
		int visibility = (indistress ? View.INVISIBLE : View.VISIBLE);
		int indistress_visibility = (indistress ? View.VISIBLE : View.INVISIBLE);

		UI.target_text.setVisibility(visibility);
		UI.distance_text.setVisibility(visibility);
		UI.compass_bg.setVisibility(visibility);
		UI.compass_arrow.setVisibility(visibility);

		UI.distress_cancel_button.setVisibility(indistress_visibility);
	}

	private void updateGraphics(boolean magnetic, boolean from_gps, boolean indistress) {
		// this is bitmask representing changes need to be made on graphics
		// if it required: bg change on gps direction, visuals change on distress, etc
		int graphics_state = 0
			+ (indistress ? 1 : 0) * 4
			+ (magnetic ? 1 : 0) * 2
			+ (from_gps ? 1 : 0);

		if (this.graphics_state == graphics_state) {
			return;
		}

		updateTitle(magnetic, from_gps, indistress);

		this.graphics_state = graphics_state;

		updateVisibility(indistress);

		if (indistress) {
			UI.compass_arrow.clearAnimation();
			this.screen.reset(); // to complement clearAnimation()

			UI.distress_cancel_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent cancel_intent = new Intent(DistressCallService.ACTION_CANCEL);
					sendBroadcast(cancel_intent);
				}
			});

			return;
		}

		setupImaginery(from_gps);
	}

	public void updateStatus(Monitor.Entry entry) {
		boolean indistress = entry.indistress;

		updateAlertMessage(entry);
		updateGraphics(entry.direction_is_magnetic, entry.direction_from_gps, indistress);
		updateLocation(entry.location);

		if (indistress) { // don't update compass if in distress mode
			return;
		}

		updateDirection(entry.direction_is_magnetic, entry.direction_from_gps, entry.direction);
		updateDistanceAndSpeed(entry.distance, entry.speed, entry.direction_from_gps);

		updateMagneticAccuracy(entry.magnetic_accuracy_low, entry.direction_is_magnetic);
		updateMagneticInRange(entry.magnetic_in_range, entry.direction_is_magnetic);
	}

	private void setupImaginery(boolean from_gps) {
		UI.compass_arrow.setImageDrawable(from_gps
			? this.theme.getGPSCompassArrow(UI.compass_arrow.getWidth(),
				UI.compass_arrow.getHeight())
			: this.theme.getMagneticCompassArrow(UI.compass_arrow.getWidth(),
				UI.compass_arrow.getHeight()));

		UI.compass_bg.setImageDrawable(from_gps
			? this.theme.getGPSCompassBG(UI.compass_bg.getWidth(),
				UI.compass_bg.getHeight())
			: this.theme.getMagneticCompassBG(UI.compass_bg.getWidth(),
				UI.compass_bg.getHeight()));
	}

	public void onGlobalLayout() {
		setupImaginery(false);

		// reset compass arrow if only not in distress
		if (!DistressCallService.indistress) {
			rotateCompass(0, 0, 0, 0);
		}

		// unsubscribe from updates
		ViewTreeObserver observer = UI.compass_arrow.getViewTreeObserver();

		if (observer.isAlive()) {
			observer.removeGlobalOnLayoutListener(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		this.menu = menu;

		return true;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu) {
		this.menu = null;
	}

	private void updateSendLocationMenuItem(Menu menu) {
		if (menu != null) {
			menu.findItem(R.id.menu_send_location).setEnabled(this.location != null);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		updateSendLocationMenuItem(menu);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_bookmarks:
		{
			Intent intent = new Intent(MainActivity.this, BookmarksActivity.class);
			if (this.location != null) {
				intent.putExtra(BookmarksActivity.Extras.CURRENT_LOCATION, this.location);
			}

			startActivityForResult(intent, REQUEST_BOOKMARK_RESULT);
		}
			break;

		case R.id.menu_send_location:
			if (this.location != null) {
				if (!ActivityUtils.sendPOI(this, new POI(this.location, null))) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle(R.string.menu_send_location);
					builder.setMessage(R.string.message_cant_send_sms);
					builder.setPositiveButton(android.R.string.ok, null);
					builder.show();
				}
			}
			break;

		case R.id.menu_settings:
		{
			Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
			startActivity(intent);
		}
			break;
		}

		return false;
	}

	@Override
	public void onAnimationEnd(Animation animation) {

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		if (this.compass_wants > NO_DIRECTION) {
			animation.reset();
			rotateCompass(0, this.compass_wants,
				Config.COMPASS_ANIMATION_DURATION, 0);
			this.compass_wants = NO_DIRECTION;
		}
	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	public void onOK(DialogInterface dialog) {
		UI.alert_dialog = null;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		onOK(dialog);
	}
}
