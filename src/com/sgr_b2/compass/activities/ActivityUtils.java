package com.sgr_b2.compass.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;

import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.io.SMSText;


public class ActivityUtils {

	public static boolean sendPOI(Context context, POI poi) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.addCategory(Intent.CATEGORY_DEFAULT);
		i.setType("vnd.android-dir/mms-sms");

		i.putExtra("sms_body", SMSText.format(context, poi, null));

		try {
			context.startActivity(i);
		}
		catch (ActivityNotFoundException e) {
			return false;
		}

		return true;
	}

	public static void startDistressCallService(Context context) {
		Intent service_intent = new Intent(context, DistressCallService.class);
		context.startService(service_intent);
	}

	public static void stopDistressCallService(Context context) {
		Intent service_intent = new Intent(context, DistressCallService.class);
		context.stopService(service_intent);
	}
}
