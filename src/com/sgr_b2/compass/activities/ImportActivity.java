package com.sgr_b2.compass.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.sgr_b2.compass.R;
import com.sgr_b2.compass.db.BookmarksProvider;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.phone.SMSPOI;
import com.sgr_b2.compass.ui.ImportView;
import com.sgr_b2.compass.ui.ImportsAdapter;


public class ImportActivity
	extends TemporaryBaseActivity
	implements OnItemClickListener {

	private ImportsAdapter adapter = null;
	private BookmarksProvider pois_provider = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_import);
		setTitle(getResources().getString(R.string.title_import_bookmarks));

		this.pois_provider = new BookmarksProvider(this);
		this.adapter = new ImportsAdapter(this, this.pois_provider);

		ListView pois = (ListView)findViewById(R.id.imports_layout);
		pois.setAdapter(this.adapter);
		pois.setOnItemClickListener(this);
	}

	@Override
	public void onDestroy() {
		if (this.pois_provider != null) {
			this.pois_provider.dispose();
		}

		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();

		findViewById(R.id.import_nothing_here).setVisibility(
			this.adapter.getCount() > 0 ?
			View.INVISIBLE : View.VISIBLE);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		if (!view.isEnabled()) {
			return;
		}

		ImportView v = (ImportView)view;

		boolean do_import = !v.isChecked();

		final SMSPOI poi = (SMSPOI)view.getTag();
		final String title = ImportsAdapter.getSMSPOITitle(poi);

		if (do_import) {
			POI import_poi = new POI(poi.lat, poi.lon, title);
			POI existing = this.pois_provider.hasPOI(title);

			if (existing != null) {
				import_poi.id = existing.id;
				this.pois_provider.updatePOI(import_poi);
			}
			else {
				this.pois_provider.addPOI(import_poi);
			}
		}

		v.setChecked(!v.isChecked());
		v.setEnabled(!v.isChecked());

		this.adapter.notifyDataSetChanged();
	}
}
