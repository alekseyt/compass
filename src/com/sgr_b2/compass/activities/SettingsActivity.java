package com.sgr_b2.compass.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

import com.sgr_b2.compass.R;


public class SettingsActivity
	extends PreferenceActivity
	implements OnSharedPreferenceChangeListener {

	// display options
	public static final String KEY_PREF_UNITS = "pref_display_units";
	public static final String KEY_PREF_THEME = "pref_theme";
	public static final String KEY_PREF_SHOW_SPEED = "pref_show_speed";
	public static final String KEY_PREF_SHOW_WAITING_ALERT = "pref_show_waiting_alert";

	// sharing
	public static final String KEY_PREF_SHARING_URI = "pref_sharing_uri";

	// messages
	public static final String KEY_PREF_SHOW_SENSORS_ALERT = "pref_show_sensors_alert";
	public static final String KEY_PREF_SHOW_MAGNETIC_RANGE_ALERT = "pref_show_magnetic_range_alert";
	public static final String KEY_PREF_SHOW_CALIBRATION_ALERT = "pref_show_calibration_alert";

	// distress options
	public static final String KEY_PREF_DISTRESS_HELP = "pref_distress_help";
	public static final String KEY_PREF_DISTRESS_RUN_SERVICE = "pref_distress_run_service";
	public static final String KEY_PREF_DISTRESS_USE_NUMBER = "pref_distress_use_number";
	public static final String KEY_PREF_DISTRESS_NUMBER = "pref_distress_number";
	public static final String KEY_PREF_DISTRESS_USE_NUMBER_ONLY = "pref_distress_use_number_only";
	public static final String KEY_PREF_ABOUT_CC = "pref_about_cc";

	// internal stuff
	private static final int CONTACT_PICKER_RESULT = 1001;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		/* FIXME: this one cant extend TemporaryBaseActivity, therefore it
		 * manually reimplement orientation locking */

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	protected void onResume() {
		super.onResume();

		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);

		// configure sharing URI summary
		final String sharing_uri_backend = shared_pref.getString(KEY_PREF_SHARING_URI, "");

		ListPreference sharing_uri = (ListPreference)findPreference(KEY_PREF_SHARING_URI);
		sharing_uri.setSummary(sharingURISummary(sharing_uri_backend));


		// set distress help listener
		Preference distress_help = findPreference(KEY_PREF_DISTRESS_HELP);
		distress_help.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				displayDistressHelp();
				return false;
			}
		});


		// configure distress number entry and summary
		final String distress_number = shared_pref.getString(KEY_PREF_DISTRESS_NUMBER, "");

		CheckBoxPreference distress_service = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_RUN_SERVICE);
		distress_service.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				CheckBoxPreference distress_contact = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER);
				distress_contact.setEnabled(((CheckBoxPreference)preference).isChecked());

				updateUseNumberOnlyState(shared_pref.getString(KEY_PREF_DISTRESS_NUMBER, ""));

				return false;
			}
		});

		CheckBoxPreference distress_contact = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER);
		distress_contact.setEnabled(distress_service.isChecked());
		distress_contact.setSummary(distressCallNumberSummary(distress_contact.isChecked(), distress_number));
		distress_contact.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				CheckBoxPreference check = (CheckBoxPreference)preference;

				if (check.isChecked()) {
					Intent i = new Intent(Intent.ACTION_PICK);
					i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
					startActivityForResult(i, CONTACT_PICKER_RESULT);
				}
				else {
					setDistressNumber("");
					updateUseNumberOnlyState("");
				}

				return false;
			}
		});


		// configure preset distress number only option
		final CheckBoxPreference distress_contact_only = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER_ONLY);
		distress_contact_only.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				if (distress_contact_only.isChecked()) {
					displayUsePresetNumberOnlyWarning();
				}
				return false;
			}
		});

		updateUseNumberOnlyState(distress_number);

		shared_pref.registerOnSharedPreferenceChangeListener(this);


		// finally, configure about app listener
		findPreference(KEY_PREF_ABOUT_CC).setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				displayAboutCC();
				return false;
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();

		SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
		shared_pref.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(KEY_PREF_DISTRESS_USE_NUMBER)) {
			SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
			String distress_number = shared_pref.getString(KEY_PREF_DISTRESS_NUMBER, "");

			CheckBoxPreference pref = (CheckBoxPreference)findPreference(key);
			pref.setSummary(distressCallNumberSummary(pref.isChecked(), distress_number));
		}
		else if (key.equals(KEY_PREF_SHARING_URI)) {
			SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(this);
			String sharing_uri_backend = shared_pref.getString(KEY_PREF_SHARING_URI, "");

			ListPreference pref = (ListPreference)findPreference(key);
			pref.setSummary(sharingURISummary(sharing_uri_backend));
		}
	}

	@Override
	protected void onActivityResult(int request_code, int result_code, Intent data) {
		switch (request_code) {
		case CONTACT_PICKER_RESULT:
			if (result_code == RESULT_OK) {
				processPickerResult(data);
			}
			else {
				// uncheck distress number if nothing selected
				CheckBoxPreference pref = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER);;
				pref.setChecked(false);
			}
			break;
		}
	}

	private void displayDistressHelp() {
		final Resources res = getResources();

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.pref_distress_call_title);
		builder.setMessage(String.format(
			res.getString(R.string.message_distress_help),
			res.getString(R.string.title_activity_main)));
		builder.setPositiveButton(android.R.string.ok, null);

		builder.show();
	}

	private void displayAboutCC() {
		final SpannableString message = new SpannableString(
			getText(R.string.message_about_cc));
		Linkify.addLinks(message, Linkify.ALL);

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.app_name);
		builder.setMessage(message);
		builder.setPositiveButton(android.R.string.ok, null);

		final AlertDialog dialog = builder.create();
		dialog.show();

		((TextView)dialog.findViewById(android.R.id.message))
			.setMovementMethod(LinkMovementMethod.getInstance());
	}

	private void displayUsePresetNumberOnlyWarning() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.pref_distress_call_title);
		builder.setMessage(R.string.message_use_preset_only_warning);
		builder.setPositiveButton(android.R.string.ok, null);

		builder.show();
	}

	private String sharingURISummary(final String backend) {
		return (backend != null && backend.length() > 1)
		? backend
		: getResources().getString(R.string.pref_sharing_uri_summ);
	}

	private String distressCallNumberSummary(boolean checked, final String number) {
		return (checked && number != null && number.trim().length() > 0)
		? number
		: getResources().getString(R.string.pref_distress_number_summ);
	}

	private void setDistressNumber(final String number) {
		SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
		SharedPreferences.Editor editor = shared_pref.edit();

		editor.putString(KEY_PREF_DISTRESS_NUMBER, number);
		editor.commit();

		// for whatever reason onSharedPreferenceChanged() is not triggered on commit()
		// if called from AlertDialog: Android 2.3.5
		CheckBoxPreference pref = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER);
		pref.setSummary(distressCallNumberSummary(pref.isChecked(), number));
	}

	private void updateUseNumberOnlyState(final String number) {
		boolean enable = (number != null && number.length() > 0)
			&& ((CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_RUN_SERVICE)).isChecked();

		CheckBoxPreference use_number_only = (CheckBoxPreference)findPreference(KEY_PREF_DISTRESS_USE_NUMBER_ONLY);
		use_number_only.setEnabled(enable);
	}

	private void processPickerResult(Intent data) {
		Uri result = data.getData();
		String id = result.getLastPathSegment();

		Cursor cursor = getContentResolver().query(
			ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
			ContactsContract.CommonDataKinds.Phone._ID + "=?",
			new String[] {id}, null);

		String picked_number = null;
		try {
			if (cursor.moveToFirst()) {
				picked_number = cursor.getString(
					cursor.getColumnIndex(
						ContactsContract.CommonDataKinds.Phone.NUMBER));
			}
		}
		finally {
			cursor.close();
		}

		setDistressNumber(picked_number);
		updateUseNumberOnlyState(picked_number);
	}
}
