package com.sgr_b2.compass.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;


/** The whole purpose of this base class is to lock all CC activities
 * in portrait mode until landscape mode is finished, see branches/landscape
 *
 * It's better to lock everything in one mode, rather than let different
 * activities switch between portrait/landscape.
 */
public abstract class TemporaryBaseActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}
