package com.sgr_b2.compass.io;

import android.net.Uri;

import com.sgr_b2.compass.ui.UnitsFormatting;


public class OSM implements Alien {

	private static final String HOST = "www.openstreetmap.org";

	@Override
	public String probe(String u) {
		Uri uri = Uri.parse(u);

		if ((uri.getScheme().equals("http") || uri.getScheme().equals("https"))
		&& uri.getHost().equals(OSM.HOST)
		&& uri.getQueryParameter("mlat") != null
		&& uri.getQueryParameter("mlon") != null) {
			return u;
		}

		return null;
	}

	@Override
	public String marshall(float lat, float lon) {
		String slat = UnitsFormatting.formatCoord(lat);
		String slon = UnitsFormatting.formatCoord(lon);

		StringBuilder builder = new StringBuilder();
		builder.append("http://");
		builder.append(OSM.HOST);
		builder.append("/?mlat=");
		builder.append(slat);
		builder.append("&mlon=");
		builder.append(slon);

		return builder.toString();
	}

	@Override
	public Coords unmarshall(String u) {
		Uri uri = Uri.parse(u);
		String mlat = uri.getQueryParameter("mlat");
		String mlon = uri.getQueryParameter("mlon");

		if (mlat == null || mlat.length() < 1
		|| mlon == null || mlon.length() < 1) {
			return null;
		}

		float lat = 0;
		float lon = 0;

		try {
			lat = Float.parseFloat(mlat);
			lon = Float.parseFloat(mlon);
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}

		return new Coords(lat, lon);
	}
}
