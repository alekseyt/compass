package com.sgr_b2.compass.io;

import com.sgr_b2.compass.ui.UnitsFormatting;

import android.net.Uri;


public class GMaps implements Alien {

	private static final String HOST = "maps.google.com";

	@Override
	public String probe(final String u) {
		Uri uri = Uri.parse(u);

		if ((uri.getScheme().equals("http") || uri.getScheme().equals("https"))
		&& uri.getHost().equals(GMaps.HOST)
		&& uri.getLastPathSegment().equals("maps")
		&& uri.getQueryParameter("q") != null) {
			return u;
		}

		return null;
	}

	@Override
	public String marshall(float lat, float lon) {
		String slat = UnitsFormatting.formatCoord(lat);
		String slon = UnitsFormatting.formatCoord(lon);

		StringBuilder builder = new StringBuilder();
		builder.append("https://");
		builder.append(GMaps.HOST);
		builder.append("/maps?q=");
		builder.append(slat);
		builder.append("+"); // for potomok
		builder.append(slon);

		return builder.toString();
	}

	@Override
	public Coords unmarshall(final String u) {
		Uri uri = Uri.parse(u);
		String q = uri.getQueryParameter("q");

		q = q.replace("loc:", ""); // i wonder

		if (q == null || q.length() < 1) {
			return null;
		}

		String[] latlon = q.split(",");
		if (latlon.length < 2) {
			latlon = q.split(" ");

			if (latlon.length < 2) {
				return null;
			}
		}

		float lat = 0;
		float lon = 0;

		try {
			lat = Float.parseFloat(latlon[0]);
			lon = Float.parseFloat(latlon[1]);
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}

		return new Coords(lat, lon);
	}
}
