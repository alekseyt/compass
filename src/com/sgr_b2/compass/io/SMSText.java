package com.sgr_b2.compass.io;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sgr_b2.compass.activities.SettingsActivity;
import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.jni.HRTProtocol;


public class SMSText {

	public static String format(Context context, final POI poi, final String tail) {
		StringBuilder builder = new StringBuilder();

		builder.append(
			HRTProtocol.marshall(poi.title, (float)poi.lat, (float)poi.lon));

		if (tail != null) {
			builder.append(" ");
			builder.append(tail);
		}

		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(context);
		final String uri_backend = shared_pref.getString(SettingsActivity.KEY_PREF_SHARING_URI, "");
		Alien alien = AlienFactory.create(uri_backend);

		if (alien != null) {
			builder.append("\n\n");
			builder.append(alien.marshall(poi.lat, poi.lon));
		}

		return builder.toString();
	}
}
