package com.sgr_b2.compass.io;

import android.net.Uri;


public class AlienFactory {

	private static final String OSM_OPT = "www.openstreetmap.org";
	private static final String GMAPS_OPT = "maps.google.com";
	private static final String OSM_HOST = "openstreetmap.org";
	private static final String GMAPS_HOST = GMAPS_OPT;

	public static Alien create(final String option) {
		if (option.equals(OSM_OPT)) {
			return new OSM();
		}
		else if (option.equals(GMAPS_OPT)) {
			return new GMaps();
		}

		return null;
	}

	public static Alien createFromURI(final String u) {
		Uri uri = Uri.parse(u);

		if (uri.getHost().indexOf(OSM_HOST) >= 0) {
			return new OSM();
		}
		else if (uri.getHost().indexOf(GMAPS_HOST) >= 0) {
			return new GMaps();
		}

		return null;
	}
}
