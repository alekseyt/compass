package com.sgr_b2.compass.io;


public interface Alien {

	public class Coords {
		public float lat;
		public float lon;

		public Coords(float lat, float lon) {
			this.lat = lat;
			this.lon = lon;
		}
	}

	/**
	 * Expect probe() to return true and umarshall() to return
	 * null. probe() is not complete unmarshalling, hence
	 * loop need to continue with probing another aliens if
	 * unmarshall() return null
	 *
	 * @param uri
	 * @return will return same URL or new URL if modification
	 * required (original URL is shortened)
	 */
	public String probe(final String uri);

	/**
	 * Write location into URI
	 *
	 * @param lat
	 * @param lon
	 * @return URI string
	 */
	public String marshall(float lat, float lon);

	/**
	 * Expect this to return null even if probe() returned true.
	 * Note though that it is required to test string with probe()
	 * first before unmarshalling.
	 *
	 * @param uri
	 * @return coords or null on parsing error
	 */
	public Coords unmarshall(final String uri);
}
