package com.sgr_b2.compass.phone;

import com.sgr_b2.compass.activities.ActivityUtils;
import com.sgr_b2.compass.activities.SettingsActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;



public class BootBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		final SharedPreferences shared_pref = PreferenceManager.getDefaultSharedPreferences(context);
		boolean run_service = shared_pref.getBoolean(SettingsActivity.KEY_PREF_DISTRESS_RUN_SERVICE, true);

		if (!run_service) {
			return;
		}

		ActivityUtils.startDistressCallService(context);
	}
}
