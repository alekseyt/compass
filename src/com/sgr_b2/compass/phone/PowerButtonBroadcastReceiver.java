package com.sgr_b2.compass.phone;

import java.lang.ref.WeakReference;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sgr_b2.compass.activities.DistressCallService;
import com.sgr_b2.compass.jni.DistressDetector;


public class PowerButtonBroadcastReceiver extends BroadcastReceiver {

	private WeakReference<DistressCallService> distress_service = null;
	private DistressDetector detector = null;

	public PowerButtonBroadcastReceiver(DistressCallService distress_service, DistressDetector detector) {
		this.detector = detector;
		this.distress_service = new WeakReference<DistressCallService>(distress_service);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		assert(this.detector != null);

		long now = System.currentTimeMillis();

		if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			this.detector.updateAlertButton(true, now);
		}
		else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			this.detector.updateAlertButton(false, now);
		}

		// toggle armed status (if possible)
		if (this.detector.armed(now)) {
			this.distress_service.get().arm();
		}
		else {
			this.distress_service.get().disarm();
		}
	}
}
