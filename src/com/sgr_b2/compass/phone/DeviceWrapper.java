package com.sgr_b2.compass.phone;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationListener;
import android.location.LocationManager;

import com.sgr_b2.compass.Config;


public class DeviceWrapper {

	private LocationManager location_manager = null;
	private SensorManager sensors_manager = null;

	public DeviceWrapper(Context context) {
		this.location_manager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		this.sensors_manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	}

	/**
	 * check if GPS on this device is available/enabled
	 *
	 * @return true if GPS is available and enabled
	 */
	public boolean isGPSAvailable() {
		return (this.location_manager != null
			&& this.location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
	}

	/**
	 * enable location updates and setup listener
	 *
	 * @return true is setup was successful
	 */
	public boolean setupGPS(LocationListener listener) {
		if (!isGPSAvailable()) {
			return false;
		}

		this.location_manager.requestLocationUpdates(
			LocationManager.NETWORK_PROVIDER,
			Config.DEV_WRAPPER_MIN_TIME_BW_UPDATES,
			Config.DEV_WRAPPER_MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);

		this.location_manager.requestLocationUpdates(
			LocationManager.GPS_PROVIDER,
			Config.DEV_WRAPPER_MIN_TIME_BW_UPDATES,
			Config.DEV_WRAPPER_MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);

		return true;
	}

	/**
	 * turn off location updates
	 */
	public void releaseGPS(LocationListener listener) {
		if (this.location_manager != null && listener != null) {
			this.location_manager.removeUpdates(listener);
		}
	}

	public boolean isAccelerometerAvailable() {
		return (this.sensors_manager != null
				&& this.sensors_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null);
	}

	public boolean setupAccelerometer(SensorEventListener listener) {
		if (!isAccelerometerAvailable()) {
			return false;
		}

		Sensor accel = this.sensors_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		this.sensors_manager.registerListener(listener, accel, SensorManager.SENSOR_DELAY_NORMAL);

		return true;
	}

	/**
	 * check sensors needed for magnetic compass
	 *
	 * @return true if accelerometer and magnetic field sensors are available and enabled
	 */
	public boolean isMagneticSensorAvailable() {
		return (this.sensors_manager != null
			&& this.sensors_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null);
	}

	/**
	 * enable sensor updates and setup listener
	 *
	 * @return true on successful setup
	 */
	public boolean setupMagneticSensor(SensorEventListener listener) {
		if (!isMagneticSensorAvailable()) {
			return false;
		}

		Sensor mag = this.sensors_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		this.sensors_manager.registerListener(listener, mag, SensorManager.SENSOR_DELAY_NORMAL);

		return true;
	}

	public void releaseAccelerometer(SensorEventListener listener) {
		if (this.sensors_manager != null && listener != null) {
			Sensor accel = this.sensors_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			this.sensors_manager.unregisterListener(listener, accel);
		}
	}

	/**
	 * disable sensor updates
	 */
	public void releaseMagneticSensor(SensorEventListener listener) {
		if (this.sensors_manager != null && listener != null) {
			Sensor mag = this.sensors_manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			this.sensors_manager.unregisterListener(listener, mag);
		}
	}
}
