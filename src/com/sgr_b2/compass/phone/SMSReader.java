package com.sgr_b2.compass.phone;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.jni.HRTProtocol;


public class SMSReader {

	public final static int HISTORY_DEFAULT = 10;
	public final static int HISTORY_INFINITE = -1;

	private Context context = null;

	public SMSReader(Context context) {
		this.context = context;
	}

	/**
	 * go through last @HISTORY_DEPTH SMS and try to extract location
	 *
	 * @param after_id onlyy look after SMS with this id
	 * @return SMSPOI or null
	 */
	public SMSPOI findLocation(String after_id, int limit) {
		Cursor cursor = this.context.getContentResolver().query(
				Uri.parse("content://sms/inbox"),
				new String[] { "_id", "address", "body", "date" },
				null, null, "date DESC" + (limit > 0 ? " LIMIT " + (String.valueOf(limit)) : ""));

		try {
			// go through SMS in a backward direction due to "ORDER date DESC"
			if (cursor.moveToLast()) {
				boolean skipped = (after_id == null);

				do {
					String id = cursor.getString(0);
					String address = cursor.getString(1);
					String body = cursor.getString(2);
					long date = cursor.getLong(3);

					if (!skipped) {
						if (after_id != null
							&& Integer.valueOf(after_id) < Integer.valueOf(id)) {
							skipped = true;
						}
					}

					if (!skipped) {
						continue;
					}

					POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall(body));

					if (poi != null) {
						return new SMSPOI(address, date, id, poi);
					}
				}
				while (cursor.moveToPrevious());
			}
		}
		finally {
			cursor.close();
		}

		return null;
	}

	/**
	 * @return latest SMS id available in inbox
	 */
	public String getLastSMSId() {
		Cursor cursor = this.context.getContentResolver().query(
				Uri.parse("content://sms/inbox"),
				new String[] { "_id" },
				null, null, "date DESC LIMIT 1");

		try {
			if (!cursor.moveToFirst()) {
				return null;
			}

			String id = cursor.getString(0);
			return id;
		}
		finally {
			cursor.close();
		}
	}
}
