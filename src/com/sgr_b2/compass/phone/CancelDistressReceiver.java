package com.sgr_b2.compass.phone;

import java.lang.ref.WeakReference;

import com.sgr_b2.compass.activities.DistressCallService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class CancelDistressReceiver extends BroadcastReceiver {

	private WeakReference<DistressCallService> distress_service = null;

	public CancelDistressReceiver(DistressCallService distress_service) {
		this.distress_service = new WeakReference<DistressCallService>(distress_service);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		distress_service.get().hangup();
	}
}
