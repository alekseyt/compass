package com.sgr_b2.compass.phone;

import com.sgr_b2.compass.db.POI;


public class SMSPOI extends POI {

	private static final long serialVersionUID = 7978530767190304049L;

	public String contact;
	public String sms_id;
	public long date;

	public SMSPOI(String contact, long date, String sms_id, POI poi) {
		super(poi.lat, poi.lon, poi.title);
		this.contact = contact;
		this.sms_id = sms_id;
		this.date = date;
	}

	public SMSPOI(final SMSPOI other) {
		super(other.lat, other.lon, other.title);
		this.contact = other.contact;
		this.sms_id = other.sms_id;
		this.date = other.date;
	}
}
