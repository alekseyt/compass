package com.sgr_b2.compass.phone;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;


public class CallsReader {

	public static class Call {
		public String number;
		public String cached_name;
		public long date;
		public long duration;

		public Call(final String number, final String cached_name, long date, long duration) {
			this.number = number;
			this.cached_name = cached_name;
			this.date = date;
			this.duration = duration;
		}
	}

	private Context context = null;

	public CallsReader(Context context) {
		this.context = context;
	}

	public List<Call> getCalls() {
		assert(this.context != null);

		List<Call> ret = new LinkedList<Call>();

		Cursor cursor = this.context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
			null, null, null, null);

		try {
			int number_field = cursor.getColumnIndex(CallLog.Calls.NUMBER);
			int cached_name_field = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
			int date_field = cursor.getColumnIndex(CallLog.Calls.DATE);
			int duration_field = cursor.getColumnIndex(CallLog.Calls.DURATION);

			if (cursor.moveToFirst()) {
				do {
					String number = cursor.getString(number_field);
					String cached_name = cursor.getString(cached_name_field);
					long date = cursor.getLong(date_field);
					long duration = cursor.getLong(duration_field);

					ret.add(new Call(number, cached_name, date, duration));
				}
				while (cursor.moveToNext());
			}
		}
		finally {
			cursor.close();
		}

		return ret;
	}
}
