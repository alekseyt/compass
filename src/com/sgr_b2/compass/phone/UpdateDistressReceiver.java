package com.sgr_b2.compass.phone;

import java.lang.ref.WeakReference;

import com.sgr_b2.compass.activities.DistressCallService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateDistressReceiver extends BroadcastReceiver {

	private WeakReference<DistressCallService> distress_service = null;

	public UpdateDistressReceiver(DistressCallService distress_service) {
		this.distress_service = new WeakReference<DistressCallService>(distress_service);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		this.distress_service.get().update();
	}
}
