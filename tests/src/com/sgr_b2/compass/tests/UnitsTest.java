package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.navigation.Units;

public class UnitsTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDistanceConversion() {
		// m/s to km/h
		assertEquals(0f, Units.mps2kmph(0), 0.01f);
		assertEquals(60f, Units.mps2kmph(16.666f), 0.01f);
		assertEquals(-60f, Units.mps2kmph(-16.666f), 0.01f);

		// m to feet
		assertEquals(0f, Units.m2ft(0), 0.01f);
		assertEquals(36.0892f, Units.m2ft(11), 0.0001f);
		assertEquals(-36.0892f, Units.m2ft(-11), 0.0001f);

		// feet to miles
		assertEquals(0f, Units.ft2mi(0), 0.01f);
		assertEquals(0.189394f, Units.ft2mi(1000), 0.000001f);
		assertEquals(0.94697f, Units.ft2mi(5000), 0.00001f);
	}

	public void testSpeedConversion() {
		// m/s to km/h
		assertEquals(0f, Units.mps2kmph(0f), 0.1f);
		assertEquals(36f, Units.mps2kmph(10f), 0.1f);
		assertEquals(32.4f, Units.mps2kmph(9f), 0.1f);

		// m/s to mi/h
		assertEquals(0f, Units.mps2miph(0f), 0.1f);
		assertEquals(22.3694f, Units.mps2miph(10f), 0.0001f);
		assertEquals(20.1324f, Units.mps2miph(9f), 0.0001f);
	}
}
