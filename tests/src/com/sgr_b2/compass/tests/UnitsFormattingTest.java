package com.sgr_b2.compass.tests;

import java.text.ParseException;

import junit.framework.TestCase;

import com.sgr_b2.compass.ui.UnitsFormatting;

public class UnitsFormattingTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCoordsFormatting() {
		assertEquals("0.000000", UnitsFormatting.formatCoord(0));
		assertEquals("1.234568", UnitsFormatting.formatCoord(1.23456789f));
		assertEquals("11.234568", UnitsFormatting.formatCoord(11.23456789f));
		assertEquals("-11.234568", UnitsFormatting.formatCoord(-11.23456789f));
		assertEquals("-1.234568", UnitsFormatting.formatCoord(-1.23456789f));
	}

	public void testCoordsParsing() throws ParseException {
		assertEquals(0, UnitsFormatting.parseCoord("0.000000"), 0.000001);
		assertEquals(1.234567, UnitsFormatting.parseCoord("1.234567"), 0.000001);
		assertEquals(11.234567, UnitsFormatting.parseCoord("11.234567"), 0.000001);
		assertEquals(-11.234567, UnitsFormatting.parseCoord("-11.234567"), 0.000001);
		assertEquals(-1.234567, UnitsFormatting.parseCoord("-1.234567"), 0.000001);

		assertEquals(0, UnitsFormatting.parseCoord("0,000000"), 0.000001);
		assertEquals(1.234567, UnitsFormatting.parseCoord("1,234567"), 0.000001);
		assertEquals(11.234567, UnitsFormatting.parseCoord("11,234567"), 0.000001);
		assertEquals(-11.234567, UnitsFormatting.parseCoord("-11,234567"), 0.000001);
		assertEquals(-1.234567, UnitsFormatting.parseCoord("-1,234567"), 0.000001);
	}

	public void testRounding() throws ParseException {
		// 6 digital digit error is allowed
		assertEquals(60.966998, UnitsFormatting.parseCoord("60.966998"), 0.00001);
		assertEquals(69.040478, UnitsFormatting.parseCoord("69.040478"), 0.00001);
	}

	public void testDistanceAndSpeedJoin() {
		assertEquals("a / b", UnitsFormatting.joinDistanceAndSpeed("a", "b"));
	}
}
