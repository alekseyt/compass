package com.sgr_b2.compass.tests;

import junit.framework.TestCase;
import android.location.Location;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.navigation.AndroidRouter;
import com.sgr_b2.compass.navigation.Monitor;


public class RouterTest extends TestCase {

	AndroidRouter router = null;

	protected void setUp() throws Exception {
		super.setUp();

		this.router = new AndroidRouter();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testLocationAzimuthTTL() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setBearing(11.1f);

		router.updateLocation(location, System.currentTimeMillis());
		router.updateMagneticHeading(12.2f);

		float lowpass_factor = (1 - Config.TRACKER_ANGLE_ALPHA);

		Monitor.Entry entry = new Monitor.Entry();
		router.readState(entry, System.currentTimeMillis());
		assertFalse(entry.direction_is_magnetic);
		assertEquals((int)(11 * lowpass_factor), entry.azimuth, 0.1f);

		try {
			Thread.sleep(Config.ROUTER_LOCATION_TTL / 2, 0);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		// check heading is still there
		router.readState(entry, System.currentTimeMillis());
		assertFalse(entry.direction_is_magnetic);
		assertEquals((int)(11 * lowpass_factor), entry.azimuth, 0.1f);

		try {
			Thread.sleep(Config.ROUTER_LOCATION_TTL / 2 + 1000, 0);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		router.readState(entry, System.currentTimeMillis());
		assertTrue(entry.direction_is_magnetic);
		assertEquals((int)(12 * lowpass_factor), entry.azimuth, 0.1f);
	}

	public void testSpeedUpdates() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setSpeed(1.1f);

		router.updateLocation(location, System.currentTimeMillis());

		float lowpass_factor = (1 - Config.TRACKER_SPEED_ALPHA);

		Monitor.Entry entry = new Monitor.Entry();
		router.readState(entry, System.currentTimeMillis());
		assertEquals(1.1f * lowpass_factor, entry.speed, 0.1f);

		try {
			Thread.sleep(Config.ROUTER_SPEED_TTL / 2, 0);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		// check speed is still there
		router.readState(entry, System.currentTimeMillis());
		assertEquals(1.1f * lowpass_factor, entry.speed, 0.1f);

		try {
			Thread.sleep(Config.ROUTER_SPEED_TTL / 2 + 1000, 0);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		router.readState(entry, System.currentTimeMillis());
		assertEquals(0, entry.speed, 0.1f);
	}

	public void testWeakMagneticDetection() {
		{
			Monitor.Entry entry = new Monitor.Entry();

			router.updateMagneticHeading(12.2f);
			router.readState(entry, System.currentTimeMillis());
			assertTrue(entry.direction_is_magnetic);
			assertTrue(entry.azimuth > 0);
		}

		{
			Monitor.Entry entry = new Monitor.Entry();

			router.setExtra(AndroidRouter.EXTRA_MAG_IN_RANGE, 0);
			router.readState(entry, System.currentTimeMillis());

			// should get magnetic direction even if sensor fails
			assertTrue(entry.direction_is_magnetic);
			assertTrue(entry.azimuth > 0);
		}
	}
}
