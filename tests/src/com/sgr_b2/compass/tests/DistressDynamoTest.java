package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.jni.CalleeLookup;
import com.sgr_b2.compass.jni.DistressDynamo;


public class DistressDynamoTest extends TestCase {

	DistressDynamo dynamo = null;
	CalleeLookup lookup = null;

	protected void setUp() throws Exception {
		super.setUp();

		dynamo = new DistressDynamo();
		lookup = new CalleeLookup();

		lookup.pushCall("1111111", "name1", 0, 10);
		lookup.pushCall("1111111", "name1", 1, 10);
		lookup.pushCall("2222222", "name2", 0, 20);
		lookup.pushCall("2222222", "name2", 1, 20);
		lookup.pushCall("3333333", "name3", 0, 50);
		lookup.pushCall("3333333", "name3", 1, 50);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testRotationWOPreset() {
		// test lookup
		assertEquals("3333333", lookup.getBestNumber(0).getNumber());
		assertEquals("1111111", lookup.getBestNumber(2).getNumber());

		assertEquals("3333333", dynamo.getNumber(null, lookup));
		// test again to check that it didn't rotate yet
		assertEquals("3333333", dynamo.getNumber(null, lookup));
		// rotate
		dynamo.next();
		assertEquals("2222222", dynamo.getNumber(null, lookup));
		dynamo.next();
		assertEquals("1111111", dynamo.getNumber(null, lookup));
		dynamo.next();
		assertEquals("3333333", dynamo.getNumber(null, lookup));
	}

	public void testRotationWPreset() {
		final String preset = "sup"; // as is, constraints applied to phonebook numbers should be ignored

		// test lookup
		assertEquals("3333333", lookup.getBestNumber(0).getNumber());
		assertEquals("1111111", lookup.getBestNumber(2).getNumber());

		assertEquals(preset, dynamo.getNumber(preset, lookup));
		dynamo.next();
		assertEquals("3333333", dynamo.getNumber(preset, lookup));
		dynamo.next();
		assertEquals("2222222", dynamo.getNumber(preset, lookup));
		dynamo.next();
		assertEquals("1111111", dynamo.getNumber(preset, lookup));
		dynamo.next();
		assertEquals(preset, dynamo.getNumber(preset, lookup));
		dynamo.next();
		assertEquals("3333333", dynamo.getNumber(preset, lookup));
	}

	public void testReset() {
		// test lookup
		assertEquals("3333333", lookup.getBestNumber(0).getNumber());
		assertEquals("1111111", lookup.getBestNumber(2).getNumber());

		assertEquals("3333333", dynamo.getNumber(null, lookup));
		dynamo.next();
		assertEquals("2222222", dynamo.getNumber(null, lookup));
		dynamo.reset(); // restart dynamo
		assertEquals("3333333", dynamo.getNumber(null, lookup));
		dynamo.next();
		assertEquals("2222222", dynamo.getNumber(null, lookup));
		dynamo.next();
		assertEquals("1111111", dynamo.getNumber(null, lookup));
	}

	public void testUseNumberOnly() {
		final String preset = "1111111";

		assertEquals(preset, dynamo.getNumber(preset, null));
		dynamo.next();
		assertEquals(preset, dynamo.getNumber(preset, null));
		dynamo.next();
		assertEquals(preset, dynamo.getNumber(preset, null));
	}
}
