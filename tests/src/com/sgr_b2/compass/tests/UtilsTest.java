package com.sgr_b2.compass.tests;

import junit.framework.TestCase;
import android.location.Location;

import com.sgr_b2.compass.jni.Utils;
import com.sgr_b2.compass.jni.android.Adopt;

public class UtilsTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDirection() {
		Location here = new Location("Me");
		here.setLatitude(0);
		here.setLongitude(0);

		Location there = new Location("Me");
		there.setLatitude(0);
		there.setLongitude(1);

		assertEquals(90f, Utils.direction(
			Adopt.location(here), Adopt.location(there)), 0.01f);

		there.setLatitude(1);
		assertEquals(45f, Utils.direction(
			Adopt.location(here), Adopt.location(there)), 0.01f);

		there.setLatitude(-1);
		there.setLongitude(-1);
		assertEquals(-135f, Utils.direction(
			Adopt.location(here), Adopt.location(there)), 0.01f);

		there.setLatitude(0);
		there.setLongitude(-1);
		assertEquals(-90f, Utils.direction(
			Adopt.location(here), Adopt.location(there)), 0.01f);
	}

	public void testAzimuthNormalization() {
		assertEquals(0, Utils.normalizeAzimuth(0), 0.01);
		assertEquals(359, Utils.normalizeAzimuth(359), 0.01);
		assertEquals(0, Utils.normalizeAzimuth(360), 0.01);
		assertEquals(1, Utils.normalizeAzimuth(361), 0.01);

		assertEquals(359, Utils.normalizeAzimuth(-1), 0.01);
		assertEquals(359, Utils.normalizeAzimuth(-361), 0.01);
		assertEquals(0, Utils.normalizeAzimuth(-0), 0.01);
	}

	public void testSanitization() {
		assertEquals(0, Utils.sanitizeLat(0), 0.000001);
		assertEquals(0, Utils.sanitizeLon(0), 0.000001);

		assertEquals(-90, Utils.sanitizeLat(-90), 0.000001);
		assertEquals(90, Utils.sanitizeLat(90), 0.000001);
		assertEquals(-180, Utils.sanitizeLon(-180), 0.000001);
		assertEquals(180, Utils.sanitizeLon(180), 0.000001);

		assertEquals(-90, Utils.sanitizeLat(-91.5f), 0.000001);
		assertEquals(90, Utils.sanitizeLat(91.5f), 0.000001);
		assertEquals(-180, Utils.sanitizeLon(-181.5f), 0.000001);
		assertEquals(180, Utils.sanitizeLon(181.5f), 0.000001);
	}

	public void testMagneticFieldStrength() {
		float[] values = new float[3];
		values[0] = 1; values[1] = 2; values[2] = 3;

		// magneticFieldStrength() is returning sqr strength
		assertEquals(3.7416f * 3.7416f, Utils.magneticFieldSqrStrength(values), 0.001);
	}

	public void testMagneticFieldRange() {
		// magneticFieldValid() is accepting sqr strength
		assertFalse(Utils.sqrMagneticFieldValid(0));
		assertFalse(Utils.sqrMagneticFieldValid(1000 * 1000));
		assertFalse(Utils.sqrMagneticFieldValid(80.1f * 80.1f));
		assertFalse(Utils.sqrMagneticFieldValid(19.999f * 19.999f));
		assertTrue(Utils.sqrMagneticFieldValid(20 * 20));
		assertTrue(Utils.sqrMagneticFieldValid(70 * 70));
		assertTrue(Utils.sqrMagneticFieldValid(21 * 21));
		assertTrue(Utils.sqrMagneticFieldValid(69 * 69));
		assertTrue(Utils.sqrMagneticFieldValid(45 * 45));
		assertTrue(Utils.sqrMagneticFieldValid(45.5f * 45.5f));
	}
}
