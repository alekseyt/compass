package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.jni.CalleeLookup;


public class CalleeLookupTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testUpdate() {
		CalleeLookup lookup = new CalleeLookup();

		lookup.pushCall("1111111", "name1", 0, 1);
		lookup.pushCall("1111111", "name1", 1, 2);

		{
			CalleeLookup.Callee callee = lookup.getBestNumber(0);

			assertNotNull(callee);
			assertEquals("1111111", callee.getNumber());
			/* XXX: move to C tests
			assertEquals(2, callee.calls_number);
			assertEquals(1, callee.timestamp); // should be latest
			assertEquals(3, callee.duration); // should sum up
			*/
		}

		lookup.pushCall("2222222", "name2", 2, 1);
		lookup.pushCall("2222222", "name2", 3, 1);

		{
			CalleeLookup.Callee callee1 = lookup.getBestNumber(0);
			CalleeLookup.Callee callee2 = lookup.getBestNumber(1);

			assertNotNull(callee1);
			assertNotNull(callee2);
			assertFalse(callee1 == callee2);
			assertFalse(callee1.getNumber().equals(callee2.getNumber()));
		}
	}

	public void testSelection() {
		CalleeLookup lookup = new CalleeLookup();

		lookup.pushCall("1111111", "name1", 0, 1);
		lookup.pushCall("1111111", "name1", 1, 2);

		{
			CalleeLookup.Callee best = lookup.getBestNumber(0);

			assertNotNull(best);
			assertEquals("1111111", best.getNumber());
		}

		lookup.pushCall("2222222", "name2", 0, 1); // should be ignored at this point

		{
			CalleeLookup.Callee best = lookup.getBestNumber(0);

			assertNotNull(best);
			assertEquals("1111111", best.getNumber());
		}

		lookup.pushCall("2222222", "name2", 1, 1);
		lookup.pushCall("2222222", "name2", 2, 10); // now it's more calls and longer duration compared to 1111111

		{
			CalleeLookup.Callee best = lookup.getBestNumber(0);

			assertNotNull(best);
			assertEquals("2222222", best.getNumber());
		}
	}

	public void testSingleCallsAtTheEnd() {
		CalleeLookup lookup = new CalleeLookup();

		lookup.pushCall("1111111", "name11", 0, 1);
		lookup.pushCall("1111112", "name12", 0, 2);
		lookup.pushCall("1111113", "name13", 0, 3);
		lookup.pushCall("1111114", "name14", 0, 4);
		lookup.pushCall("1111115", "name15", 0, 5);
		assertNotNull(lookup.getBestNumber(0));

		lookup.pushCall("2222222", "name2", 1, 1);
		lookup.pushCall("2222222", "name2", 2, 1);
		{
			CalleeLookup.Callee best = lookup.getBestNumber(0);

			assertNotNull(best);
			assertEquals("2222222", best.getNumber());
		}

		lookup.pushCall("1111116", "name16", 0, 4);
		lookup.pushCall("1111117", "name17", 0, 5);

		{
			CalleeLookup.Callee best = lookup.getBestNumber(0);

			assertNotNull(best);
			assertEquals("2222222", best.getNumber());
		}
	}

	public void testShortNumbers() {
		CalleeLookup lookup = new CalleeLookup();

		lookup.pushCall("111", "name111", 0, 1);
		assertNull(lookup.getBestNumber(0));

		lookup.pushCall("1111", "name1111", 0, 1);
		assertNull(lookup.getBestNumber(0));

		lookup.pushCall("11111", "name11111", 0, 1);
		assertNotNull(lookup.getBestNumber(0));
	}

	public void testInsignificantBaseScore() {
		// records with significant score should always be in front of
		// insignificant calls
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("1111111", "name1", 1375669712, 1);
			lookup.pushCall("1111111", "name1", 1375669712, 1);
			lookup.pushCall("2222222", "name2", 1375669712, 1);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}

		// both call are insignificant
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("1111111", "name1", 1375669712, 1);
			lookup.pushCall("2222222", "name2", 1375669712, 1);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}

		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("2222222", "name2", 1375669712, 1);
			lookup.pushCall("1111111", "name1", 1375669712, 1);

			assertEquals("2222222", lookup.getBestNumber(0).getNumber());
		}
	}

	public void testDurationWeight() {
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("1111111", "name1", 1375669712, 10);
			lookup.pushCall("1111111", "name1", 1375669712, 10);
			lookup.pushCall("2222222", "name2", 1375669712, 5);
			lookup.pushCall("2222222", "name2", 1375669712, 5);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}

		// same duration
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("1111111", "name1", 1375669712, 5);
			lookup.pushCall("1111111", "name1", 1375669712, 5);
			lookup.pushCall("2222222", "name2", 1375669712, 5);
			lookup.pushCall("2222222", "name2", 1375669712, 5);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}

		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("2222222", "name2", 1375669712, 5);
			lookup.pushCall("2222222", "name2", 1375669712, 5);
			lookup.pushCall("1111111", "name1", 1375669712, 5);
			lookup.pushCall("1111111", "name1", 1375669712, 5);

			assertEquals("2222222", lookup.getBestNumber(0).getNumber());
		}
	}

	public void testCallsNumberWeight() {
		// same duration, different calls number
		// smaller average duration -> shorter calls, less significant
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("2222222", "name2", 1375669712, 10);
			lookup.pushCall("2222222", "name2", 1375669712, 10);
			lookup.pushCall("2222222", "name2", 1375669712, 10);
			lookup.pushCall("1111111", "name1", 1375669712, 15);
			lookup.pushCall("1111111", "name1", 1375669712, 15);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}
	}

	public void testNamedContactsPreferred() {
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("2222222", null, 1375669712, 10);
			lookup.pushCall("2222222", null, 1375669712, 10);
			lookup.pushCall("2222222", null, 1375669712, 10);
			lookup.pushCall("1111111", "name1", 1375669712, 1);
			lookup.pushCall("1111111", "name1", 1375669712, 1);

			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}
	}

	public void testShorterNamesPreferred() {
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("1111111", "name111", 1375669712, 10);
			lookup.pushCall("1111111", "name111", 1375669712, 10);
			lookup.pushCall("2222222", "name22", 1375669712, 10);
			lookup.pushCall("2222222", "name22", 1375669712, 10);
			lookup.pushCall("3333333", "name3333", 1375669712, 10);
			lookup.pushCall("3333333", "name3333", 1375669712, 10);

			assertEquals("2222222", lookup.getBestNumber(0).getNumber());
			assertEquals("1111111", lookup.getBestNumber(1).getNumber());
			assertEquals("3333333", lookup.getBestNumber(2).getNumber());
		}
	}

	public void testShorterNamesPrevail() {
		{
			CalleeLookup lookup = new CalleeLookup();
			lookup.pushCall("2222222", "name22", 1375669712, 10);
			lookup.pushCall("2222222", "name22", 1375669712, 10);
			lookup.pushCall("2222222", "name22", 1375669712, 10); // should be ignored
			lookup.pushCall("1111111", null, 1375669712, 5);
			lookup.pushCall("1111111", "name1", 1375669712, 5);
			lookup.pushCall("1111111", "nam", 1375669712, 5);

			// shorter name should be preffered even if duration of call
			// was less than duration with rival
			assertEquals("1111111", lookup.getBestNumber(0).getNumber());
		}
	}
}
