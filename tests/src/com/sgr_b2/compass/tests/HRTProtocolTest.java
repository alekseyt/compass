package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.db.POI;
import com.sgr_b2.compass.jni.HRTProtocol;


public class HRTProtocolTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testUnmarshalling() {
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("привет ромашки 1.234567,3.456789"));
			assertTrue(poi != null);
			assertEquals("привет ромашки", poi.title);
			assertEquals(1.234567f, poi.lat, 0.000001);
			assertEquals(3.456789f, poi.lon, 0.000001);
		}

		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall(" 1.200000,3.400000"));
			assertTrue(poi != null);
			assertTrue(poi.title == null);
			assertEquals(1.2, poi.lat, 0.01);
			assertEquals(3.4, poi.lon, 0.01);
		}
	}

	public void testUnmarshalling0A0D() {
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("привет ромашки\n1.234567,3.456789"));
			assertTrue(poi != null);
			assertTrue(poi.title == null);
			assertEquals(1.234567f, poi.lat, 0.000001);
			assertEquals(3.456789f, poi.lon, 0.000001);
		}

		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("привет ромашки\r1.234567,3.456789"));
			assertTrue(poi != null);
			assertTrue(poi.title == null);
			assertEquals(1.234567f, poi.lat, 0.000001);
			assertEquals(3.456789f, poi.lon, 0.000001);
		}
	}

	public void testUnmarshalling00A0() {
		POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("привет ромашки\u00A0+1.234567,3.456789"));
		assertTrue(poi != null);
		assertEquals("привет ромашки", poi.title);
		assertEquals(1.234567f, poi.lat, 0.000001);
		assertEquals(3.456789f, poi.lon, 0.000001);
	}

	public void testMarshalling() {
		// pois w/o title
		assertEquals("0.000000,0.000000", HRTProtocol.marshall(null, 0, 0));
		assertEquals("0.100000,-0.200000", HRTProtocol.marshall(null, 0.1f, -0.2f));

		// poi w/title
		assertEquals("привет 0.300000,0.400000", HRTProtocol.marshall("привет", 0.3f, 0.4f));
	}

	public void testMultistring() {
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("Hey,\n" +
				"what's up 1.200000,-3.400000"));
			assertEquals("what's up", poi.title);
			assertEquals(1.2f, poi.lat, 0.000001);
			assertEquals(-3.4f, poi.lon, 0.000001);
		}
	}

	public void testParsingPositives() {
		// no decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("1,2"));
			assertTrue(poi != null);
			assertEquals(1, poi.lat, 0.000001);
			assertEquals(2, poi.lon, 0.000001);
		}

		// no decimal part, but still valid
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("1.,2."));
			assertTrue(poi != null);
			assertEquals(1, poi.lat, 0.000001);
			assertEquals(2, poi.lon, 0.000001);
		}

		// shortest decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("1.0,2.0"));
			assertTrue(poi != null);
			assertEquals(1, poi.lat, 0.000001);
			assertEquals(2, poi.lon, 0.000001);
		}

		// longest decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("1.000000,2.000000"));
			assertTrue(poi != null);
			assertEquals(1, poi.lat, 0.000001);
			assertEquals(2, poi.lon, 0.000001);
		}
	}

	public void testParsingNegatives() {
		// no decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("-1,-2"));
			assertTrue(poi != null);
			assertEquals(-1, poi.lat, 0.000001);
			assertEquals(-2, poi.lon, 0.000001);
		}

		// no decimal part, but still valid
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("-1.,-2."));
			assertTrue(poi != null);
			assertEquals(-1, poi.lat, 0.000001);
			assertEquals(-2, poi.lon, 0.000001);
		}

		// shortest decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("-1.0,-2.0"));
			assertTrue(poi != null);
			assertEquals(-1, poi.lat, 0.000001);
			assertEquals(-2, poi.lon, 0.000001);
		}

		// longest decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("-1.000000,-2.000000"));
			assertTrue(poi != null);
			assertEquals(-1, poi.lat, 0.000001);
			assertEquals(-2, poi.lon, 0.000001);
		}
	}

	public void testParsingDoublePositives() {
		// no decimal part
		{
			POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall("+1.0,+2.0"));
			assertTrue(poi != null);
			assertEquals(1, poi.lat, 0.000001);
			assertEquals(2, poi.lon, 0.000001);
		}
	}

	public void testRounding() {
		// 6 decimal digit error is allowed
		String text = HRTProtocol.marshall(null, 60.966998f, 69.040478f);
		POI poi = POI.fromJNIBookmark(HRTProtocol.unmarshall(text));

		assertEquals(60.966998f, poi.lat, 0.00001f);
		assertEquals(69.040478f, poi.lon, 0.00001f);
	}
}
