package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.jni.DistressDetector;


public class DistressDetectorTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testArmingTTL() {
		DistressDetector detector = new DistressDetector();

		long start = 0;
		assertFalse(detector.armed(start));

		detector.updateAlertButton(true, start);
		detector.updateAlertButton(true, start + 1000);
		detector.updateAlertButton(true, start + 2000);
		detector.updateAlertButton(true, start + 3000);

		// should be disarmed - only 4 presses
		assertFalse(detector.armed(start + 3000));
		assertFalse(detector.armed(start + 4000));

		detector.updateAlertButton(true, start + 4000);

		// "on" doesn't really matter, so should be armed now
		assertTrue(detector.armed(start + 5000));

		// emulate sleep()
		start = 5000 + Config.D_DETECTOR_ARMED_TTL;
		assertFalse(detector.armed(start));

		detector.updateAlertButton(true, start);
		detector.updateAlertButton(true, start + 1000);
		detector.updateAlertButton(true, start + 2000);
		detector.updateAlertButton(true, start + 3000);

		// should be disarmed - only 4 presses
		assertFalse(detector.armed(start + 3000));
		assertFalse(detector.armed(start + 4000));

		detector.updateAlertButton(true, start + 4000);

		// armed again
		assertTrue(detector.armed(start + 5000));
	}

	public void testArmingAndReset() {
		DistressDetector detector = new DistressDetector();
		detector.updateAlertButton(true, 0);
		detector.updateAlertButton(true, 1000);
		detector.updateAlertButton(true, 2000);
		detector.updateAlertButton(true, 3000);
		detector.updateAlertButton(true, 4000);

		// "on" doesn't really matter, so should be armed now
		assertTrue(detector.armed(5000));

		detector.reset();

		assertFalse(detector.armed(3000));
		assertFalse(detector.armed(4000));
		assertFalse(detector.armed(5000));
	}
}
