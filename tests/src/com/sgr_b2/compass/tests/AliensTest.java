package com.sgr_b2.compass.tests;

import junit.framework.TestCase;

import com.sgr_b2.compass.io.Alien;
import com.sgr_b2.compass.io.GMaps;
import com.sgr_b2.compass.io.OSM;


public class AliensTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGMaps() {
		GMaps proto = new GMaps();

		assertNull(proto.probe("https://maps.google.com/maps"));
		assertNull(proto.probe("https://mads.google.com/maps?q=1,2"));
		assertNotNull(proto.probe("https://maps.google.com/maps?q=1,2"));
		assertNotNull(proto.probe("https://maps.google.com/maps?q=loc:1,2"));
		assertNotNull(proto.probe("https://maps.google.com/maps?q=1+2"));
		assertNotNull(proto.probe("https://maps.google.com/maps?q=loc:1+2"));

		assertEquals("https://maps.google.com/maps?q=1.000000+2.000000", proto.marshall(1, 2));
		assertEquals("https://maps.google.com/maps?q=-1.100000+-2.100000", proto.marshall(-1.1f, -2.1f));

		{
			Alien.Coords coords = proto.unmarshall("https://maps.google.com/maps?q=-1.1,-2.1");
			assertEquals(-1.1f, coords.lat, 0.000001);
			assertEquals(-2.1f, coords.lon, 0.000001);
		}

		{
			Alien.Coords coords = proto.unmarshall("https://maps.google.com/maps?q=loc:-1.01,2.2");
			assertEquals(-1.01f, coords.lat, 0.000001);
			assertEquals(2.2f, coords.lon, 0.000001);
		}

		{
			Alien.Coords coords = proto.unmarshall("https://maps.google.com/maps?q=-1.1+-2.1");
			assertEquals(-1.1f, coords.lat, 0.000001);
			assertEquals(-2.1f, coords.lon, 0.000001);
		}

		{
			Alien.Coords coords = proto.unmarshall("https://maps.google.com/maps?q=loc:-1.01+2.2");
			assertEquals(-1.01f, coords.lat, 0.000001);
			assertEquals(2.2f, coords.lon, 0.000001);
		}
	}

	public void testGMapsFail() {
		GMaps proto = new GMaps();

		{
			// dot instead of comma in lat,lon
			Alien.Coords coords = proto.unmarshall("https://maps.google.com/maps?q=-1.1.-2.1");
			assertNull(coords);
		}

		assertNotNull(proto.probe("https://maps.google.com/maps?q=1,2"));
		assertNull(proto.unmarshall("https://maps.google.com/maps?q=a,b"));

		assertNotNull(proto.probe("https://maps.google.com/maps?q=1+2"));
		assertNull(proto.unmarshall("https://maps.google.com/maps?q=a+b"));
	}

	public void testOSM() {
		OSM proto = new OSM();

		assertNull(proto.probe("http://www.openstreetmap.org/"));
		assertNull(proto.probe("http://www.openstreetmap.org/?mlat=1"));
		assertNull(proto.probe("http://www.openstreetmap.org/?mlon=2"));
		assertNotNull(proto.probe("http://www.openstreetmap.org/?mlat=1&mlon=2"));

		assertEquals("http://www.openstreetmap.org/?mlat=1.000000&mlon=2.000000", proto.marshall(1, 2));
		assertEquals("http://www.openstreetmap.org/?mlat=-1.100000&mlon=-2.100000", proto.marshall(-1.1f, -2.1f));

		{
			Alien.Coords coords = proto.unmarshall("http://www.openstreetmap.org/?mlat=-1.1&mlon=-2.1");
			assertEquals(-1.1f, coords.lat, 0.000001);
			assertEquals(-2.1f, coords.lon, 0.000001);
		}
	}

	public void testOSMFail() {
		OSM proto = new OSM();

		assertNotNull(proto.probe("http://www.openstreetmap.org/?mlat=1&mlon=2"));
		assertNull(proto.unmarshall("http://www.openstreetmap.org/?mlat=a&mlon=b"));
	}
}
