package com.sgr_b2.compass.tests;

import junit.framework.TestCase;
import android.location.Location;

import com.sgr_b2.compass.Config;
import com.sgr_b2.compass.jni.Tracker;
import com.sgr_b2.compass.jni.Utils;

public class TrackerTest extends TestCase {
	Tracker tracker = null;

	protected void setUp() throws Exception {
		super.setUp();

		this.tracker = new Tracker();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testMagneticAzimuthLowPass() {
		assertEquals(0f, tracker.getMagneticHeading(), 0.00001f);

		tracker.updateMagneticHeading(100);

		float lowpass_factor = (1 - Config.TRACKER_ANGLE_ALPHA);
		assertEquals(100 * lowpass_factor, tracker.getMagneticHeading(), 1); // it should be on its way to 100

		for (int i = 0; i < 10; ++i) {
			tracker.updateMagneticHeading(100);
		}

		assertEquals(100f, tracker.getMagneticHeading(), 0.00001f); // now it should be ok
	}

	public void testLocationHeading() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setBearing(11.1f);

		assertFalse(tracker.hasLocationHeading());
		tracker.updateLocation(location, System.currentTimeMillis());
		assertTrue(tracker.hasLocationHeading());

		location.removeBearing();
		tracker.updateLocation(location, System.currentTimeMillis());
		assertTrue(tracker.hasLocationHeading()); // still valid, i.e. not updated
	}

	public void testLocationSpeed() {
		Location location = new Location("Me");
		location.setLatitude(0);
		location.setLongitude(0);
		location.setSpeed(1.1f);

		assertFalse(tracker.hasSpeed());
		tracker.updateLocation(location, System.currentTimeMillis());
		assertTrue(tracker.hasSpeed());

		location.removeSpeed();
		tracker.updateLocation(location, System.currentTimeMillis());
		assertFalse(tracker.hasSpeed());
	}

	public void testLocationAzimuthLowPass() {
		Location location = new Location("Me");
		location.setLatitude(1);
		location.setLongitude(1);
		location.setBearing(45);

		assertEquals(0, tracker.getLocationHeading(), 0.0001);

		float lowpass_factor = (1 - Config.TRACKER_ANGLE_ALPHA);

		tracker.updateLocation(location, System.currentTimeMillis());
		assertEquals(Utils.normalizeAzimuth(45) * lowpass_factor,
			tracker.getLocationHeading(), 1); // it should be on its way to 45 deg

		for (int i = 0; i < 10; ++i) {
			try {
				Thread.sleep(100, 0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			location.setLatitude(i + 2);
			location.setLongitude(i + 2);
			tracker.updateLocation(location, System.currentTimeMillis());
		}

		assertEquals(Utils.normalizeAzimuth(45),
			tracker.getLocationHeading(), 1); // now it should be ok
	}

	public void testDistance() {
		Location current = new Location("Me");
		current.setLatitude(0);
		current.setLongitude(0);

		Location target = new Location("Me");
		target.setLatitude(1);
		target.setLongitude(1);

		tracker.updateLocation(current, System.currentTimeMillis());
		assertEquals(157249.37f, tracker.getDistance(target), 0.01f);

		target.setLatitude(10);
		target.setLongitude(10);

		tracker.updateLocation(current, System.currentTimeMillis());
		assertEquals(1568520.5f, tracker.getDistance(target), 0.01f);
	}

	public void testDirection() {
		Location current = new Location("Me");
		current.setLatitude(1);
		current.setLongitude(1);

		Location target_cw = new Location("Me");
		target_cw.setLatitude(2);
		target_cw.setLongitude(2);

		Location target_ccw = new Location("Me");
		target_ccw.setLatitude(0);
		target_ccw.setLongitude(0);

		tracker.updateLocation(current, System.currentTimeMillis());

		// cw
		assertEquals(0, tracker.getDirection(45, target_cw), 0.01f);
		assertEquals(45, tracker.getDirection(0, target_cw), 0.01f);
		assertEquals(90, tracker.getDirection(-45f, target_cw), 0.01f);
		assertEquals(90, tracker.getDirection(Utils.normalizeAzimuth(-45f), target_cw), 0.01f);
		assertEquals(315, tracker.getDirection(90, target_cw), 0.01f);

		// ccw
		assertEquals(0, tracker.getDirection(225, target_ccw), 0.01f);
		assertEquals(0, tracker.getDirection(-135, target_ccw), 0.01f);
		assertEquals(0, tracker.getDirection(Utils.normalizeAzimuth(-135), target_ccw), 0.01f);
		assertEquals(180, tracker.getDirection(45, target_ccw), 0.01f);
		assertEquals(90, tracker.getDirection(135, target_ccw), 0.01f);
	}
}
